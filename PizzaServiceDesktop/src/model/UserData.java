package model;

import java.time.LocalDate;
import java.util.ArrayList;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

public class UserData {

    private SimpleIntegerProperty iD;
    private SimpleStringProperty name;
    private SimpleStringProperty surname;
    private SimpleObjectProperty<LocalDate> birthday;
    private SimpleStringProperty login;
    private SimpleStringProperty password;

    public UserData() {
        iD = new SimpleIntegerProperty();
        name = new SimpleStringProperty();
        surname = new SimpleStringProperty();
        birthday = new SimpleObjectProperty<>();
        login = new SimpleStringProperty();
        password = new SimpleStringProperty();
    }

    public SimpleIntegerProperty getiDProperty() {
        return iD;
    }

    public void setiDProperty(SimpleIntegerProperty iD) {
        this.iD = iD;
    }

    public SimpleStringProperty getNameProperty() {
        return name;
    }

    public void setNameProperty(SimpleStringProperty name) {
        this.name = name;
    }

    public SimpleStringProperty getSurnameProperty() {
        return surname;
    }

    public void setSurnameProperty(SimpleStringProperty surname) {
        this.surname = surname;
    }

    public SimpleObjectProperty<LocalDate> getBirthdayProperty() {
        return birthday;
    }

    public void setBirthdayProperty(SimpleObjectProperty<LocalDate> birthday) {
        this.birthday = birthday;
    }

    public SimpleStringProperty getLoginProperty() {
        return login;
    }

    public void setLoginProperty(SimpleStringProperty login) {
        this.login = login;
    }

    public SimpleStringProperty getPasswordProperty() {
        return password;
    }

    public void setPasswordProperty(SimpleStringProperty password) {
        this.password = password;
    }
    //-------------------------------------------------------

    public Integer getiD() {
        return iD.get();
    }

    public void setiD(Integer iD) {
        this.iD.set(iD);;
    }

    public String getName() {
        return name.get();
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public String getSurname() {
        return surname.get();
    }

    public void setSurname(String surname) {
        this.surname.set(surname);
    }

    public LocalDate getBirthday() {
        return birthday.get();
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday.set(birthday);
    }

    public String getLogin() {
        return login.get();
    }

    public void setLogin(String login) {
        this.login.set(login);
    }

    public String getPassword() {
        return password.get();
    }

    public void setPassword(String password) {
        this.password.set(password);
    }

    public static ArrayList<TableColumn<UserData, ?>> getColumns(TableView<UserData> table) {
        ArrayList<TableColumn<UserData, ?>> columns = new ArrayList<>();
        int[] columnWidth = {7, 14, 19, 12, 17, 21}; //percentage values

        TableColumn<UserData, Number> id = new TableColumn<>("ID");
        TableColumn<UserData, String> name = new TableColumn<>("Name");
        TableColumn<UserData, String> surname = new TableColumn<>("Surname");
        TableColumn<UserData, LocalDate> birthday = new TableColumn<>("Birthday");
        TableColumn<UserData, String> login = new TableColumn<>("Login");
        TableColumn<UserData, String> password = new TableColumn<>("Password");

        int i = 0;
        id.prefWidthProperty().bind(table.widthProperty().divide(100 / columnWidth[i++]));
        name.prefWidthProperty().bind(table.widthProperty().divide(100 / columnWidth[i++]));
        surname.prefWidthProperty().bind(table.widthProperty().divide(100 / columnWidth[i++]));
        birthday.prefWidthProperty().bind(table.widthProperty().divide(100 / columnWidth[i++]));
        login.prefWidthProperty().bind(table.widthProperty().divide(100 / columnWidth[i++]));
        password.prefWidthProperty().bind(table.widthProperty().divide(100 / columnWidth[i++]));

        id.setCellValueFactory(cellData -> cellData.getValue().getiDProperty());
        name.setCellValueFactory(cellData -> cellData.getValue().getNameProperty());
        surname.setCellValueFactory(cellData -> cellData.getValue().getSurnameProperty());
        birthday.setCellValueFactory(cellData -> cellData.getValue().getBirthdayProperty());
        login.setCellValueFactory(cellData -> cellData.getValue().getLoginProperty());
        password.setCellValueFactory(cellData -> cellData.getValue().getPasswordProperty());

        columns.add(id);
        columns.add(name);
        columns.add(surname);
        columns.add(birthday);
        columns.add(login);
        columns.add(password);
        return columns;
    }
}
