package model;

import classes.ExceptionDialog;
import classes.ModalWindow;
import controller.NewPizzaSizeController;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Optional;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableView;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;

public class PizzaSizeTable extends TableView<PizzaSizeData> {

    private final ObservableList<PizzaSizeData> pizzaSizeList;

    public PizzaSizeTable() {
        super();
        pizzaSizeList = FXCollections.observableArrayList();
        getColumns().addAll(PizzaSizeData.getColumns(this));
        setItems(pizzaSizeList);
    }

    public ObservableList<PizzaSizeData> getPizzaSizeList() {
        return pizzaSizeList;
    }

    public boolean loadPizzaSizeTable() {
        boolean isOK = false;
        String query = "SELECT * FROM pizza_size";
        try (Connection connection = Main.dataSource.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    while (resultSet.next()) {
                        PizzaSizeData pizzaSizeData = new PizzaSizeData();
                        pizzaSizeData.setSize(resultSet.getInt(1));
                        pizzaSizeData.setPrice(resultSet.getDouble(2));

                        pizzaSizeList.add(pizzaSizeData);
                    }
                    isOK = true;
                }
            }
        } catch (Exception exception) {
            ExceptionDialog exceptionDialog = new ExceptionDialog("Error while loading data from database", exception);
            exceptionDialog.showAndWait();
        };

        return isOK;
    }

    public void newSize() {
        FXMLLoader loader = new FXMLLoader(getClass().getResource(CONSTANTS.ROOT_NEW_PIZZA_SIZE.string));
        BorderPane editPane;
        try {
            editPane = new BorderPane(loader.load());
            ModalWindow newSizeWindow = new ModalWindow(Main.mainStage, editPane, "New pizza size");
            newSizeWindow.setResizable(false);
            newSizeWindow.getScene().getStylesheets().add(getClass().getResource(CONSTANTS.CSS_PATH.string).toExternalForm());

            NewPizzaSizeController pizzaSizeController = loader.getController();
            pizzaSizeController.setPizzaSizeTable(this);

            newSizeWindow.initModality(Modality.APPLICATION_MODAL);
            newSizeWindow.showAndWait();
        } catch (IOException exception) {
            ExceptionDialog exceptionDialog = new ExceptionDialog("Couldn't load newPizzaSize", exception);
            exceptionDialog.showAndWait();
        }
    }

    public void delete() {
        int indexOfItem = getSelectionModel().getSelectedIndex();
        if (indexOfItem >= 0) { // we check if something is selected
            PizzaSizeData pizzaSizeData = (PizzaSizeData) getSelectionModel().getSelectedItem();

            Alert alert = new Alert(AlertType.CONFIRMATION);
            alert.setTitle("Confirmation Dialog");
            alert.setHeaderText("Pizza size: " + pizzaSizeData.getSize() + ", price: " + pizzaSizeData.getPrice());
            alert.setContentText("Delete this pizza size?");

            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                String query = "DELETE FROM pizza_size WHERE size = ?";
                try (Connection connection = Main.dataSource.getConnection()) {
                    try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                        preparedStatement.setInt(1, pizzaSizeData.getSize());
                        preparedStatement.executeUpdate();
                        pizzaSizeList.remove(indexOfItem);
                    }
                } catch (Exception exception) {
                    ExceptionDialog exceptionDialog = new ExceptionDialog("Error while deleting pizza size", exception);
                    exceptionDialog.showAndWait();
                }
            }
        }
    }
}
