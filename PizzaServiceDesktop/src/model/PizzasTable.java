package model;

import classes.ExceptionDialog;
import classes.InfoTip;
import classes.ModalWindow;
import classes.SearchPane;
import controller.NewPizzaController;
import controller.PizzaEditController;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Optional;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TableView;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import model.PizzaData.AVAILABLE;

public class PizzasTable extends TableView<PizzaData> {

    private ObservableList<PizzaData> pizzasList;

    public PizzasTable() {
        super();
        pizzasList = FXCollections.observableArrayList();
        getColumns().addAll(PizzaData.getColumns(this));
        setItems(pizzasList);
    }

    public ObservableList<PizzaData> getPizzasList() {
        return pizzasList;
    }

    public boolean loadPizzasTable() {
        boolean isOK = false;
        String query = "SELECT * FROM pizzas";
        try (Connection connection = Main.dataSource.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    while (resultSet.next()) {
                        PizzaData pizzaData = new PizzaData();
                        pizzaData.setID(resultSet.getInt(1));
                        pizzaData.setName(resultSet.getString(2));
                        pizzaData.setComposition(resultSet.getString(3));
                        pizzaData.setPrice(resultSet.getDouble(4));
                        pizzaData.setAvailable(AVAILABLE.valueOf(resultSet.getString(5)));

                        pizzasList.add(pizzaData);
                    }
                }
            }
        } catch (Exception exception) {
            ExceptionDialog exceptionDialog = new ExceptionDialog("Error while loading data from database", exception);
            exceptionDialog.showAndWait();
        }

        return isOK;
    }

    public boolean addNew() {
        boolean isOK = false;
        FXMLLoader loader = new FXMLLoader(getClass().getResource(CONSTANTS.ROOT_NEW_PIZZA.string));
        BorderPane editPane;
        try {
            editPane = new BorderPane(loader.load());
            ModalWindow editWindow = new ModalWindow(Main.mainStage, editPane, "New pizza");
            editWindow.setResizable(false);
            editWindow.getScene().getStylesheets().add(getClass().getResource(CONSTANTS.CSS_PATH.string).toExternalForm());
            NewPizzaController newPizzaController = new NewPizzaController();
            newPizzaController = loader.getController();
            newPizzaController.setPizzasTable(this);

            editWindow.initModality(Modality.APPLICATION_MODAL);
            editWindow.showAndWait();
            isOK = true;
        } catch (IOException exception) {
            ExceptionDialog exceptionDialog = new ExceptionDialog("Couldn't load NewPizza", exception);
            exceptionDialog.showAndWait();
        }
        return isOK;
    }

    public void changeStatus(boolean available) { //true = visible
        int indexOfItem = getSelectionModel().getSelectedIndex();
        if (indexOfItem >= 0) { // we check if something is selected
            PizzaData pizzaData = (PizzaData) getSelectionModel().getSelectedItem();

            //we check first if user tries to change yes to yes or no to no
            boolean condition1 = (pizzaData.getAvailable() != AVAILABLE.Yes) || !available;
            boolean condition2 = (pizzaData.getAvailable() != AVAILABLE.No) || available;
            if (condition1 && condition2) {
                Alert alert = new Alert(AlertType.CONFIRMATION);
                alert.setTitle("Confirmation Dialog");
                alert.setHeaderText("Pizza: " + pizzaData.getID() + ". " + pizzaData.getName() + " Price: " + pizzaData.getPrice());

                if (available) {
                    alert.setContentText("Show this pizza in menu?");
                } else {
                    alert.setContentText("Hide this pizza in menu?");
                }

                Optional<ButtonType> result = alert.showAndWait();
                if (result.get() == ButtonType.OK) {
                    String query = "UPDATE pizzas SET visible = ? WHERE id = ?;";
                    try (Connection connection = Main.dataSource.getConnection()) {
                        try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                            if (available) {
                                preparedStatement.setString(1, "Yes");
                                pizzasList.get(indexOfItem).setAvailable(AVAILABLE.Yes);
                            } else {
                                preparedStatement.setString(1, "No");
                                pizzasList.get(indexOfItem).setAvailable(AVAILABLE.No);
                            }
                            preparedStatement.setInt(2, pizzaData.getID());
                            preparedStatement.executeUpdate();

                        }
                    } catch (Exception exception) {
                        ExceptionDialog exceptionDialog = new ExceptionDialog("Error while updating pizza status", exception);
                        exceptionDialog.showAndWait();
                    }
                }
            }
        }
    }

    public void edit() {
        if (getSelectionModel().getSelectedIndex() >= 0) { // we check if something is selected
            FXMLLoader loader = new FXMLLoader(UsersTable.class.getResource(CONSTANTS.ROOT_PIZZA_EDIT.string));
            BorderPane editPane;
            try {
                editPane = new BorderPane(loader.load());
                ModalWindow editWindow = new ModalWindow(Main.mainStage, editPane, "Edit user");
                editWindow.setResizable(false);
                editWindow.getScene().getStylesheets().add(UsersTable.class.getResource(CONSTANTS.CSS_PATH.string).toExternalForm()); //its new stage with new scene, so we need to load this file again	

                PizzaData pizzaData = getSelectionModel().getSelectedItem();
                PizzaEditController pizzaEditWindowController = loader.getController();
                pizzaEditWindowController.fillWithData(pizzaData);

                editWindow.initModality(Modality.APPLICATION_MODAL);
                editWindow.showAndWait();
            } catch (IOException exception) {
                ExceptionDialog exceptionDialog = new ExceptionDialog("Couldn't load PizzaEdit", exception);
                exceptionDialog.showAndWait();
            }
        }
    }

    public ModalWindow pizzasSearch() {
        SearchPane searchPane = new SearchPane(5);
        searchPane.setNames("ID", "Name", "Composition", "Price", "Available");
        ModalWindow searchWindow = new ModalWindow(Main.mainStage, searchPane, "Search pizzas table");

        searchPane.setOnSearch(event -> {
            ArrayList<CheckBox> list = searchPane.getCheckBoxList();
            boolean iD = list.get(0).isSelected();
            boolean name = list.get(1).isSelected();
            boolean composition = list.get(2).isSelected();
            boolean price = list.get(3).isSelected();
            boolean available = list.get(4).isSelected();

            InfoTip infoTip = new InfoTip();
            if (searchPane.isSomethingSelected()) {
                if (!searchPane.getSearchPhrase().isEmpty()) {
                    findInDatabase(iD, name, composition, price, available, searchPane.getSearchPhrase());
                    searchWindow.close();
                } else {
                    infoTip.showTip(searchPane.getTextField(), "You need to type phrase to search");
                }
            } else {
                infoTip.showTip(searchPane.getCheckBoxList().get(0), "You need to select columns first");
            }
        });

        searchWindow.setResizable(false);
        searchWindow.getScene().getStylesheets().add(getClass().getResource(CONSTANTS.CSS_PATH.string).toExternalForm());
        searchWindow.initModality(Modality.APPLICATION_MODAL);
        searchWindow.show();

        return searchWindow;
    }

    private void findInDatabase(boolean iD, boolean name, boolean composition, boolean price, boolean available, String text) {
        StringBuilder query = createQuery(text, iD, name, composition, price, available);

        try (Connection connection = Main.dataSource.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(query.toString())) {
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    pizzasList.clear();
                    while (resultSet.next()) {
                        PizzaData pizzaData = new PizzaData();
                        pizzaData.setID(resultSet.getInt(1));
                        pizzaData.setName(resultSet.getString(2));
                        pizzaData.setComposition(resultSet.getString(3));
                        pizzaData.setPrice(resultSet.getDouble(4));
                        pizzaData.setAvailable(AVAILABLE.valueOf(resultSet.getString(5)));
                        pizzasList.add(pizzaData);
                    }
                }
            }
        } catch (Exception ex) {
            ExceptionDialog exceptionDialog = new ExceptionDialog("Couldn't search pizzas table", ex);
            exceptionDialog.showAndWait();
        }
    }

    private StringBuilder createQuery(String text, boolean iD, boolean name, boolean composition, boolean price, boolean available) {
        final String phrase = "LIKE '%" + text + "%')";
        StringBuilder query = new StringBuilder("SELECT * FROM pizzas WHERE ");
        query.append(iD ? "(id " + phrase : "");
        if (name && iD) {
            query.append(" OR ");
        }
        query.append(name ? "(name " + phrase : "");
        if (composition && (iD || name)) {
            query.append(" OR ");
        }
        query.append(composition ? "(composition " + phrase : "");
        if (price && (iD || name || composition)) {
            query.append(" OR ");
        }
        query.append(price ? "(price " + phrase : "");
        if (available && (iD || name || composition || price)) {
            query.append(" OR ");
        }
        query.append(available ? "(visible " + phrase : "");
        return query;
    }
}
