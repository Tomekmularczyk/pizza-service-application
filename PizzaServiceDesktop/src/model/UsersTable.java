package model;

import classes.ExceptionDialog;
import classes.InfoTip;
import classes.ModalWindow;
import classes.SearchPane;
import controller.UserEditController;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Optional;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TableView;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;

public class UsersTable extends TableView<UserData> {

    private final ObservableList<UserData> usersList;

    public UsersTable() {
        super();
        usersList = FXCollections.observableArrayList();
        getColumns().addAll(UserData.getColumns(this));
        setItems(usersList);
    }

    public ObservableList<UserData> getUsersList() {
        return usersList;
    }

    public boolean loadUserTable() {
        boolean isOK = false;
        String query = "SELECT e.id, e.name, e.surname, e.birthday, u.login, u.password FROM employees AS e "
                + "RIGHT JOIN users AS u ON e.id = u.employee_id ORDER BY e.id;";
        try (Connection connection = Main.dataSource.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    while (resultSet.next()) {
                        UserData userData = new UserData();
                        userData.setiD(resultSet.getInt(1));
                        userData.setName(resultSet.getString(2));
                        userData.setSurname(resultSet.getString(3));
                        userData.setBirthday(resultSet.getDate(4).toLocalDate());
                        userData.setLogin(resultSet.getString(5));
                        userData.setPassword(resultSet.getString(6));
                        usersList.add(userData);
                    }
                    isOK = true;
                }
            }
        } catch (Exception exception) {
            ExceptionDialog exceptionDialog = new ExceptionDialog("Error while loading data from database", exception);
            exceptionDialog.showAndWait();
        }

        return isOK;
    }

    public void userEdit() {
        if (getSelectionModel().getSelectedIndex() >= 0) { // we check if something is selected
            UserData userData = (UserData) getSelectionModel().getSelectedItem();
            loadUserEdit(userData);
        }
    }

    public void userDelete() {
        int indexOfItem = getSelectionModel().getSelectedIndex();
        if (indexOfItem >= 0) { // we check if something is selected
            UserData userData = (UserData) getSelectionModel().getSelectedItem();

            Alert alert = new Alert(AlertType.CONFIRMATION);
            alert.setTitle("Confirmation Dialog");
            alert.setHeaderText("Employee ID: " + userData.getiD() + ", user: " + userData.getLogin());
            alert.setContentText("Delete this user account?");

            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                String query = "DELETE FROM users WHERE employee_id = ?";
                try (Connection connection = Main.dataSource.getConnection()) {
                    try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                        preparedStatement.setInt(1, userData.getiD());
                        preparedStatement.executeUpdate();
                        usersList.remove(indexOfItem);
                    }
                } catch (Exception exception) {
                    ExceptionDialog exceptionDialog = new ExceptionDialog("Error while deleting user account", exception);
                    exceptionDialog.showAndWait();
                };
            }
        }
    }

    public static void loadUserEdit(UserData userData) {
        FXMLLoader loader = new FXMLLoader(UsersTable.class.getResource(CONSTANTS.ROOT_USER_EDIT.string));
        BorderPane editPane;
        try {
            editPane = new BorderPane(loader.load());
            ModalWindow editWindow = new ModalWindow(Main.mainStage, editPane, "Edit user");
            editWindow.setResizable(false);
            editWindow.getScene().getStylesheets().add(UsersTable.class.getResource(CONSTANTS.CSS_PATH.string).toExternalForm()); //its new stage with new scene, so we need to load this file again	

            UserEditController userEditWindowController = loader.getController();
            userEditWindowController.fillWithData(userData);

            editWindow.initModality(Modality.APPLICATION_MODAL);
            editWindow.showAndWait();
        } catch (IOException exception) {
            ExceptionDialog exceptionDialog = new ExceptionDialog("Couldn't load UserEdit", exception);
            exceptionDialog.showAndWait();
        }
    }

    public ModalWindow userSearch() {
        SearchPane searchPane = new SearchPane(3);
        searchPane.setNames("Login", "Password", "Employee ID");
        ModalWindow searchWindow = new ModalWindow(Main.mainStage, searchPane, "Search users table");

        searchPane.setOnSearch(event -> {
            ArrayList<CheckBox> list = searchPane.getCheckBoxList();
            boolean login = list.get(0).isSelected();
            boolean password = list.get(1).isSelected();
            boolean employeeID = list.get(2).isSelected();

            InfoTip infoTip = new InfoTip();
            if (searchPane.isSomethingSelected()) {
                if (!searchPane.getSearchPhrase().isEmpty()) {
                    findInDatabase(login, password, employeeID, searchPane.getSearchPhrase());
                    searchWindow.close();
                } else {
                    infoTip.showTip(searchPane.getTextField(), "You need to type phrase to search");
                }
            } else {
                infoTip.showTip(searchPane.getCheckBoxList().get(0), "You need to select columns first");
            }
        });

        searchWindow.setResizable(false);
        searchWindow.getScene().getStylesheets().add(getClass().getResource(CONSTANTS.CSS_PATH.string).toExternalForm());
        searchWindow.initModality(Modality.APPLICATION_MODAL);
        searchWindow.show();

        return searchWindow;
    }

    private void findInDatabase(boolean login, boolean password, boolean employeeID, String text) {
        StringBuilder query = createQuery(text, login, password, employeeID);

        try (Connection connection = Main.dataSource.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(query.toString())) {
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    usersList.clear();
                    while (resultSet.next()) {
                        UserData userData = new UserData();
                        userData.setiD(resultSet.getInt(1));
                        userData.setName(resultSet.getString(2));
                        userData.setSurname(resultSet.getString(3));
                        userData.setBirthday(resultSet.getDate(4).toLocalDate());
                        userData.setLogin(resultSet.getString(5));
                        userData.setPassword(resultSet.getString(6));
                        usersList.add(userData);
                    }
                }
            }
        } catch (Exception ex) {
            ExceptionDialog exceptionDialog = new ExceptionDialog("Couldn't search users table", ex);
            exceptionDialog.showAndWait();
        }
    }

    private StringBuilder createQuery(String text, boolean login, boolean password, boolean employeeID) {
        final String phrase = "LIKE '%" + text + "%')";
        StringBuilder query = new StringBuilder("SELECT e.id, e.name, e.surname, e.birthday, u.login, u.password FROM employees AS e RIGHT JOIN users AS u ON e.id = u.employee_id WHERE ");
        query.append((login) ? "(u.login " + phrase : "");
        if ((password) && (login)) {
            query.append(" OR ");
        }
        query.append((password) ? "(u.password " + phrase : "");
        if ((employeeID) && (login || password)) {
            query.append(" OR ");
        }
        query.append((employeeID) ? "(u.employee_id " + phrase : "");
        query.append(" ORDER BY e.id;");
        return query;
    }
}
