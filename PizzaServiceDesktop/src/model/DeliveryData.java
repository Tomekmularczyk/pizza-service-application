package model;

import java.sql.Timestamp;
import java.util.ArrayList;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

public class DeliveryData {

    private SimpleIntegerProperty orderID;
    private SimpleObjectProperty<Timestamp> orderDate;
    private SimpleStringProperty address;
    private SimpleStringProperty clientName;
    private SimpleStringProperty telephone;
    private SimpleDoubleProperty cost;

    public DeliveryData() {
        orderID = new SimpleIntegerProperty();
        address = new SimpleStringProperty();
        orderDate = new SimpleObjectProperty<>();
        clientName = new SimpleStringProperty();
        telephone = new SimpleStringProperty();
        cost = new SimpleDoubleProperty();
    }

    public static ArrayList<TableColumn<DeliveryData, ?>> getColumns(TableView<DeliveryData> table) {
        ArrayList<TableColumn<DeliveryData, ?>> columns = new ArrayList<>();
        double[] columnWidth = {9, 21, 30, 16, 14, 7}; //percentage values

        TableColumn<DeliveryData, Number> orderID = new TableColumn<>("Order ID");
        TableColumn<DeliveryData, Timestamp> orderDate = new TableColumn<>("Order date");
        TableColumn<DeliveryData, String> adress = new TableColumn<>("Address");
        TableColumn<DeliveryData, String> clientName = new TableColumn<>("Client name");
        TableColumn<DeliveryData, String> telephone = new TableColumn<>("Telephone");
        TableColumn<DeliveryData, Number> cost = new TableColumn<>("Cost");

        int i = 0;
        orderID.prefWidthProperty().bind(table.widthProperty().divide(100 / columnWidth[i++]));
        orderDate.prefWidthProperty().bind(table.widthProperty().divide(100 / columnWidth[i++]));
        adress.prefWidthProperty().bind(table.widthProperty().divide(100 / columnWidth[i++]));
        clientName.prefWidthProperty().bind(table.widthProperty().divide(100 / columnWidth[i++]));
        telephone.prefWidthProperty().bind(table.widthProperty().divide(100 / columnWidth[i++]));
        cost.prefWidthProperty().bind(table.widthProperty().divide(100 / columnWidth[i++]));

        orderID.setCellValueFactory(cellData -> cellData.getValue().getOrderIDProperty());
        orderDate.setCellValueFactory(cellData -> cellData.getValue().getOrderDateProperty());
        adress.setCellValueFactory(cellData -> cellData.getValue().getAdressProperty());
        clientName.setCellValueFactory(cellData -> cellData.getValue().getClientNameProperty());
        telephone.setCellValueFactory(cellData -> cellData.getValue().getTelephoneProperty());
        cost.setCellValueFactory(cellData -> cellData.getValue().getCostProperty());

        columns.add(orderID);
        columns.add(orderDate);
        columns.add(adress);
        columns.add(clientName);
        columns.add(telephone);
        columns.add(cost);
        return columns;
    }

    public SimpleIntegerProperty getOrderIDProperty() {
        return orderID;
    }

    public void setOrderIDProperty(SimpleIntegerProperty orderID) {
        this.orderID = orderID;
    }

    public SimpleObjectProperty<Timestamp> getOrderDateProperty() {
        return orderDate;
    }

    public void setOrderDateProperty(SimpleObjectProperty<Timestamp> orderDate) {
        this.orderDate = orderDate;
    }

    public SimpleStringProperty getAdressProperty() {
        return address;
    }

    public void setAdressProperty(SimpleStringProperty adress) {
        this.address = adress;
    }

    public SimpleStringProperty getClientNameProperty() {
        return clientName;
    }

    public void setClientNameProperty(SimpleStringProperty clientName) {
        this.clientName = clientName;
    }

    public SimpleStringProperty getTelephoneProperty() {
        return telephone;
    }

    public void setTelephoneProperty(SimpleStringProperty telephone) {
        this.telephone = telephone;
    }

    public SimpleDoubleProperty getCostProperty() {
        return cost;
    }

    public void setCostProperty(SimpleDoubleProperty cost) {
        this.cost = cost;
    }

//--------------------------------------------
    public Integer getOrderID() {
        return orderID.get();
    }

    public void setOrderID(Integer orderID) {
        this.orderID.set(orderID);
    }

    public Timestamp getOrderDate() {
        return orderDate.get();
    }

    public void setOrderDate(Timestamp orderDate) {
        this.orderDate.set(orderDate);
    }

    public String getAdress() {
        return address.get();
    }

    public void setAdress(String adress) {
        this.address.set(adress);
    }

    public String getClientName() {
        return clientName.get();
    }

    public void setClientName(String clientName) {
        this.clientName.set(clientName);
    }

    public String getTelephone() {
        return telephone.get();
    }

    public void setTelephone(String telephone) {
        this.telephone.set(telephone);
    }

    public Double getCost() {
        return cost.get();
    }

    public void setCost(Double cost) {
        this.cost.set(cost);
    }
}
