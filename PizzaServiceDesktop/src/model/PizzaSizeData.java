package model;

import java.util.ArrayList;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

public class PizzaSizeData {

    private SimpleIntegerProperty size;
    private SimpleDoubleProperty price;

    public PizzaSizeData() {
        size = new SimpleIntegerProperty();
        price = new SimpleDoubleProperty();
    }

    public SimpleIntegerProperty getSizeProperty() {
        return size;
    }

    public void setSizeProperty(SimpleIntegerProperty size) {
        this.size = size;
    }

    public SimpleDoubleProperty getPriceProperty() {
        return price;
    }

    public void setPriceProperty(SimpleDoubleProperty price) {
        this.price = price;
    }

    public Integer getSize() {
        return size.get();
    }

    public void setSize(int size) {
        this.size.set(size);
    }

    public Double getPrice() {
        return price.get();
    }

    public void setPrice(double price) {
        this.price.set(price);
    }

    public static ArrayList<TableColumn<PizzaSizeData, ?>> getColumns(TableView<PizzaSizeData> table) {
        ArrayList<TableColumn<PizzaSizeData, ?>> columns = new ArrayList<>();
        int[] columnWidth = {15, 15}; //percentage values

        TableColumn<PizzaSizeData, Number> id = new TableColumn<>("Size(cm)");
        TableColumn<PizzaSizeData, Number> name = new TableColumn<>("Price");

        int i = 0;
        id.prefWidthProperty().bind(table.widthProperty().divide(100 / columnWidth[i++]));
        name.prefWidthProperty().bind(table.widthProperty().divide(100 / columnWidth[i++]));

        id.setCellValueFactory(cellData -> cellData.getValue().getSizeProperty());
        name.setCellValueFactory(cellData -> cellData.getValue().getPriceProperty());

        columns.add(id);
        columns.add(name);
        return columns;
    }
}
