package model;

import java.time.LocalDate;
import java.util.ArrayList;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

public class EmployeeData {

    private SimpleIntegerProperty iD;
    private SimpleStringProperty name;
    private SimpleStringProperty surname;
    private SimpleObjectProperty<LocalDate> birthday;
    private SimpleStringProperty address;
    private SimpleStringProperty telephone;

    public enum EXISTS {
        YES, NO
    }
    private SimpleObjectProperty<EXISTS> account;

    public EmployeeData() {
        iD = new SimpleIntegerProperty();
        name = new SimpleStringProperty();
        surname = new SimpleStringProperty();
        birthday = new SimpleObjectProperty<>();
        address = new SimpleStringProperty();
        telephone = new SimpleStringProperty();
        account = new SimpleObjectProperty<>();
    }

    public SimpleIntegerProperty getiDProperty() {
        return iD;
    }

    public void setiDProperty(SimpleIntegerProperty iD) {
        this.iD = iD;
    }

    public SimpleStringProperty getNameProperty() {
        return name;
    }

    public void setNameProperty(SimpleStringProperty name) {
        this.name = name;
    }

    public SimpleStringProperty getSurnameProperty() {
        return surname;
    }

    public void setSurnameProperty(SimpleStringProperty surname) {
        this.surname = surname;
    }

    public SimpleObjectProperty<LocalDate> getBirthdayProperty() {
        return birthday;
    }

    public void setBirthdayProperty(SimpleObjectProperty<LocalDate> birthday) {
        this.birthday = birthday;
    }

    public SimpleStringProperty getAdressProperty() {
        return address;
    }

    public void setAdressProperty(SimpleStringProperty adress) {
        this.address = adress;
    }

    public SimpleStringProperty getTelephoneProperty() {
        return telephone;
    }

    public void setTelephoneProperty(SimpleStringProperty telephone) {
        this.telephone = telephone;
    }

    public SimpleObjectProperty<EXISTS> getAccountProperty() {
        return account;
    }

    public void setAccountProperty(SimpleObjectProperty<EXISTS> account) {
        this.account = account;
    }
    //--------------------------------------------------------------------

    public Integer getiD() {
        return iD.get();
    }

    public void setiD(Integer iD) {
        this.iD.set(iD);;
    }

    public String getName() {
        return name.get();
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public String getSurname() {
        return surname.get();
    }

    public void setSurname(String surname) {
        this.surname.set(surname);
    }

    public LocalDate getBirthday() {
        return birthday.get();
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday.set(birthday);
    }

    public String getAdress() {
        return address.get();
    }

    public void setAdress(String adress) {
        this.address.set(adress);
    }

    public String getTelephone() {
        return telephone.get();
    }

    public void setTelephone(String telephone) {
        this.telephone.set(telephone);
    }

    public EXISTS getAccount() {
        return account.get();
    }

    public void setAccount(EXISTS account) {
        this.account.set(account);
    }

    //========================================================================
    public static ArrayList<TableColumn<EmployeeData, ?>> getColumns(TableView<EmployeeData> table) {
        ArrayList<TableColumn<EmployeeData, ?>> columns = new ArrayList<>();
        int[] columnWidth = {5, 12, 15, 12, 25, 15, 10}; //percentage values

        TableColumn<EmployeeData, Number> id = new TableColumn<>("ID");
        TableColumn<EmployeeData, String> name = new TableColumn<>("Name");
        TableColumn<EmployeeData, String> surname = new TableColumn<>("Surname");
        TableColumn<EmployeeData, LocalDate> birthday = new TableColumn<>("Birthday");
        TableColumn<EmployeeData, String> adress = new TableColumn<>("Adress");
        TableColumn<EmployeeData, String> telephone = new TableColumn<>("Telephone");
        TableColumn<EmployeeData, EXISTS> account = new TableColumn<>("Account");

        int i = 0;
        id.prefWidthProperty().bind(table.widthProperty().divide(100 / columnWidth[i++]));
        name.prefWidthProperty().bind(table.widthProperty().divide(100 / columnWidth[i++]));
        surname.prefWidthProperty().bind(table.widthProperty().divide(100 / columnWidth[i++]));
        birthday.prefWidthProperty().bind(table.widthProperty().divide(100 / columnWidth[i++]));
        adress.prefWidthProperty().bind(table.widthProperty().divide(100 / columnWidth[i++]));
        telephone.prefWidthProperty().bind(table.widthProperty().divide(100 / columnWidth[i++]));
        account.prefWidthProperty().bind(table.widthProperty().divide(100 / columnWidth[i++]));

        id.setCellValueFactory(cellData -> cellData.getValue().getiDProperty());
        name.setCellValueFactory(cellData -> cellData.getValue().getNameProperty());
        surname.setCellValueFactory(cellData -> cellData.getValue().getSurnameProperty());
        birthday.setCellValueFactory(cellData -> cellData.getValue().getBirthdayProperty());
        adress.setCellValueFactory(cellData -> cellData.getValue().getAdressProperty());
        telephone.setCellValueFactory(cellData -> cellData.getValue().getTelephoneProperty());
        account.setCellValueFactory(cellData -> cellData.getValue().getAccountProperty());

        columns.add(id);
        columns.add(name);
        columns.add(surname);
        columns.add(birthday);
        columns.add(adress);
        columns.add(telephone);
        columns.add(account);
        return columns;
    }
}
