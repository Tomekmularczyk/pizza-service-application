package model;

import java.sql.Timestamp;
import java.util.ArrayList;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import model.EmployeeData.EXISTS;

public class SaleData {

    private SimpleIntegerProperty iD;
    private SimpleObjectProperty<Timestamp> orderDate;
    private SimpleObjectProperty<ORDER_STATUS> orderStatus;
    private SimpleStringProperty user;
    private SimpleIntegerProperty pizzaNumber;
    private SimpleIntegerProperty pizzaSize;
    private SimpleObjectProperty<EXISTS> delivery;
    private SimpleDoubleProperty income;

    public enum ORDER_STATUS {
        Waiting, Accepted, Production, Transport, Finalised, Canceled
    }

    public SaleData() {
        iD = new SimpleIntegerProperty();
        orderDate = new SimpleObjectProperty<>();
        orderStatus = new SimpleObjectProperty<>();
        user = new SimpleStringProperty();
        pizzaNumber = new SimpleIntegerProperty();
        pizzaSize = new SimpleIntegerProperty();
        delivery = new SimpleObjectProperty<>();
        income = new SimpleDoubleProperty();
    }

    public SimpleIntegerProperty getIDProperty() {
        return iD;
    }

    public void setiDProperty(SimpleIntegerProperty iD) {
        this.iD = iD;
    }

    public SimpleObjectProperty<Timestamp> getOrderDateProperty() {
        return orderDate;
    }

    public void setOrderDateProperty(SimpleObjectProperty<Timestamp> orderDate) {
        this.orderDate = orderDate;
    }

    public SimpleObjectProperty<ORDER_STATUS> getOrderStatusProperty() {
        return orderStatus;
    }

    public void setOrderStatusProperty(SimpleObjectProperty<ORDER_STATUS> orderStatus) {
        this.orderStatus = orderStatus;
    }

    public SimpleStringProperty getUserProperty() {
        return user;
    }

    public void setUserProperty(SimpleStringProperty user) {
        this.user = user;
    }

    public SimpleIntegerProperty getPizzaNumberProperty() {
        return pizzaNumber;
    }

    public void setPizzaNumberProperty(SimpleIntegerProperty pizzaNumber) {
        this.pizzaNumber = pizzaNumber;
    }

    public SimpleIntegerProperty getPizzaSizeProperty() {
        return pizzaSize;
    }

    public void setPizzaSizeProperty(SimpleIntegerProperty pizzaSize) {
        this.pizzaSize = pizzaSize;
    }

    public SimpleObjectProperty<EXISTS> getDeliveryProperty() {
        return delivery;
    }

    public void setDeliveryProperty(SimpleObjectProperty<EXISTS> delivery) {
        this.delivery = delivery;
    }

    public SimpleDoubleProperty getIncomeProperty() {
        return income;
    }

    public void setIncomeProperty(SimpleDoubleProperty income) {
        this.income = income;
    }
    //----------------------------------------------------------------------------------------------

    public Integer getID() {
        return iD.get();
    }

    public void setiD(Integer iD) {
        this.iD.set(iD);
    }

    public Timestamp getOrderDate() {
        return orderDate.get();
    }

    public void setOrderDate(Timestamp orderDate) {
        this.orderDate.set(orderDate);
    }

    public ORDER_STATUS getOrderStatus() {
        return orderStatus.get();
    }

    public void setOrderStatus(ORDER_STATUS orderStatus) {
        this.orderStatus.set(orderStatus);
    }

    public String getUser() {
        return user.get();
    }

    public void setUser(String user) {
        this.user.set(user);
    }

    public Integer getPizzaNumber() {
        return pizzaNumber.get();
    }

    public void setPizzaNumber(Integer pizzaNumber) {
        this.pizzaNumber.set(pizzaNumber);
    }

    public Integer getPizzaSize() {
        return pizzaSize.get();
    }

    public void setPizzaSize(Integer pizzaSize) {
        this.pizzaSize.set(pizzaSize);
    }

    public EXISTS getDelivery() {
        return delivery.get();
    }

    public void setDelivery(EXISTS delivery) {
        this.delivery.set(delivery);
    }

    public Double getIncome() {
        return income.get();
    }

    public void setIncome(Double income) {
        this.income.set(income);
    }

    //===============================================================================================
    public static ArrayList<TableColumn<SaleData, ?>> getColumns(TableView<SaleData> table) {
        ArrayList<TableColumn<SaleData, ?>> columns = new ArrayList<>();
        double[] columnWidth = {7, 23, 12, 20, 10, 10, 8, 8}; //percentage values

        TableColumn<SaleData, Number> iD = new TableColumn<>("ID");
        TableColumn<SaleData, Timestamp> orderDate = new TableColumn<>("Order date");
        TableColumn<SaleData, ORDER_STATUS> orderStatus = new TableColumn<>("Status");
        TableColumn<SaleData, String> user = new TableColumn<>("User");
        TableColumn<SaleData, Number> pizzaNumber = new TableColumn<>("Pizza no.");
        TableColumn<SaleData, Number> pizzaSize = new TableColumn<>("Size(cm)");
        TableColumn<SaleData, EXISTS> delivery = new TableColumn<>("Delivery");
        TableColumn<SaleData, Number> income = new TableColumn<>("Income");

        int i = 0;
        iD.prefWidthProperty().bind(table.widthProperty().divide(100 / columnWidth[i++]));
        orderDate.prefWidthProperty().bind(table.widthProperty().divide(100 / columnWidth[i++]));
        orderStatus.prefWidthProperty().bind(table.widthProperty().divide(100 / columnWidth[i++]));
        user.prefWidthProperty().bind(table.widthProperty().divide(100 / columnWidth[i++]));
        pizzaNumber.prefWidthProperty().bind(table.widthProperty().divide(100 / columnWidth[i++]));
        pizzaSize.prefWidthProperty().bind(table.widthProperty().divide(100 / columnWidth[i++]));
        delivery.prefWidthProperty().bind(table.widthProperty().divide(100 / columnWidth[i++]));
        income.prefWidthProperty().bind(table.widthProperty().divide(100 / columnWidth[i++]));

        iD.setCellValueFactory(cellData -> cellData.getValue().getIDProperty());
        orderDate.setCellValueFactory(cellData -> cellData.getValue().getOrderDateProperty());
        orderStatus.setCellValueFactory(cellData -> cellData.getValue().getOrderStatusProperty());
        user.setCellValueFactory(cellData -> cellData.getValue().getUserProperty());
        pizzaNumber.setCellValueFactory(cellData -> cellData.getValue().getPizzaNumberProperty());
        pizzaSize.setCellValueFactory(cellData -> cellData.getValue().getPizzaSizeProperty());
        delivery.setCellValueFactory(cellData -> cellData.getValue().getDeliveryProperty());
        income.setCellValueFactory(cellData -> cellData.getValue().getIncomeProperty());

        columns.add(iD);
        columns.add(orderDate);
        columns.add(orderStatus);
        columns.add(user);
        columns.add(pizzaNumber);
        columns.add(pizzaSize);
        columns.add(delivery);
        columns.add(income);

        return columns;
    }
}
