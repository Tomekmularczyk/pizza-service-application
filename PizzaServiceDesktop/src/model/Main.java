package model;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class Main extends Application {

    public static Stage mainStage;
    public static MysqlDataSource dataSource = new MysqlDataSource();

    @Override
    public void start(Stage primaryStage) {
//		ustawDane();
        primaryStage.setResizable(false);

        mainStage = primaryStage;
        primaryStage.setTitle("Pizza Service");

        try {
            BorderPane root = (BorderPane) FXMLLoader.load(getClass().getResource(CONSTANTS.ROOT_MAIN_WINDOW.string));
            Scene scene = new Scene(root);
            scene.getStylesheets().add(getClass().getResource(CONSTANTS.CSS_PATH.string).toExternalForm());
            primaryStage.getIcons().add(new Image(CONSTANTS.APPLICATION_ICON.string));
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }

//	private void ustawDane(){
//		Main.dataSource.setServerName(UserPreferences.getServerName());
//		Main.dataSource.setDatabaseName(UserPreferences.getDatabaseName());
//		Main.dataSource.setUser(UserPreferences.getUserName());
//		Main.dataSource.setPassword(UserPreferences.getUserPassword());
//	}
}
