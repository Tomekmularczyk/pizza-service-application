package model;

import classes.ExceptionDialog;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

public class TableFunctions {

    public static void thrashAndLoadDefaultData() {
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle("Information");
        alert.setHeaderText(null);
        alert.setContentText("Operation may take few seconds. If you have open table you will need to open it again.");
        alert.showAndWait();
        try (Connection connection = Main.dataSource.getConnection()) {
            try {
                disableForeignKeys(connection);
                truncateTables(connection);
                insertEmployees(connection);
                insertUsers(connection);
                insertPizzas(connection);
                insertPizzaSizes(connection);
                insertSales(connection);
                insertDeliveries(connection);
                enableForeignKeys(connection);

                Alert alert2 = new Alert(AlertType.INFORMATION);
                alert2.setTitle("Information");
                alert2.setHeaderText(null);
                alert2.setContentText("Succesfully loaded default data!");
                alert2.showAndWait();
            } catch (SQLException excep) {
                ExceptionDialog exceptionDialog = new ExceptionDialog("Operations on database failed!", excep);
                exceptionDialog.showAndWait();
            }
        } catch (Exception exception) {
            ExceptionDialog exceptionDialog = new ExceptionDialog("Couldn't connect to database", exception);
            exceptionDialog.showAndWait();
        }
    }

    private static void enableForeignKeys(final Connection connection) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement("SET FOREIGN_KEY_CHECKS = 1");
        preparedStatement.executeUpdate();
    }

    private static void disableForeignKeys(Connection connection) throws SQLException {
        PreparedStatement preparedStatement;
        //we need to turn OFF foreign key constraint
        preparedStatement = connection.prepareStatement("SET FOREIGN_KEY_CHECKS = 0");
        preparedStatement.executeUpdate();
    }

    private static void truncateTables(Connection connection) throws SQLException {
        PreparedStatement preparedStatement;
        preparedStatement = connection.prepareStatement("TRUNCATE TABLE employees;");
        preparedStatement.executeUpdate();
        preparedStatement = connection.prepareStatement("TRUNCATE TABLE users;");
        preparedStatement.executeUpdate();
        preparedStatement = connection.prepareStatement("TRUNCATE TABLE pizzas;");
        preparedStatement.executeUpdate();
        preparedStatement = connection.prepareStatement("TRUNCATE TABLE pizza_size;");
        preparedStatement.executeUpdate();
        preparedStatement = connection.prepareStatement("TRUNCATE TABLE sales;");
        preparedStatement.executeUpdate();
        preparedStatement = connection.prepareStatement("TRUNCATE TABLE deliveries;");
        preparedStatement.executeUpdate();
    }

    private static void insertEmployees(final Connection connection) throws SQLException {
        String employeeStatement = "INSERT INTO employees VALUES(";
        InputStream inputFile;
        inputFile = TableFunctions.class.getResourceAsStream(CONSTANTS.DATA_EMPLOYEES.string);
        try (Scanner scanner = new Scanner(inputFile)) {
            while (scanner.hasNext()) {
                PreparedStatement preparedStatement = connection.prepareStatement(employeeStatement + scanner.nextLine() + ");");
                preparedStatement.executeUpdate();
            }
        }
    }

    private static void insertUsers(final Connection connection) throws SQLException {
        String userStatement = "INSERT INTO users VALUES(";
        InputStream inputFile = TableFunctions.class.getResourceAsStream(CONSTANTS.DATA_USERS.string);
        try (Scanner scanner = new Scanner(inputFile)) {
            while (scanner.hasNext()) {
                PreparedStatement preparedStatement = connection.prepareStatement(userStatement + scanner.nextLine() + ");");
                preparedStatement.executeUpdate();
            }
        }
    }

    private static void insertPizzas(final Connection connection) throws SQLException {
        String pizzaStatement = "INSERT INTO pizzas VALUES(";
        InputStream inputFile = TableFunctions.class.getResourceAsStream(CONSTANTS.DATA_PIZZA.string);
        try (Scanner scanner = new Scanner(inputFile)) {
            while (scanner.hasNext()) {
                PreparedStatement preparedStatement = connection.prepareStatement(pizzaStatement + scanner.nextLine() + ");");
                preparedStatement.executeUpdate();
            }
        }
    }

    private static void insertPizzaSizes(final Connection connection) throws SQLException {
        String pizza_sizeStatement = "INSERT INTO pizza_size VALUES(";
        InputStream inputFile = TableFunctions.class.getResourceAsStream(CONSTANTS.DATA_PIZZA_SIZE.string);
        try (Scanner scanner = new Scanner(inputFile)) {
            while (scanner.hasNext()) {
                PreparedStatement preparedStatement = connection.prepareStatement(pizza_sizeStatement + scanner.nextLine() + ");");
                preparedStatement.executeUpdate();
            }
        }
    }

    private static void insertSales(final Connection connection) throws SQLException {
        String salesStatement = "INSERT INTO sales VALUES(";
        InputStream inputFile = TableFunctions.class.getResourceAsStream(CONSTANTS.DATA_SALES.string);
        try (Scanner scanner = new Scanner(inputFile)) {
            while (scanner.hasNext()) {
                PreparedStatement preparedStatement = connection.prepareStatement(salesStatement + scanner.nextLine() + ");");
                preparedStatement.executeUpdate();
            }
        }
    }

    private static void insertDeliveries(final Connection connection) throws SQLException {
        String deliveryStatement = "INSERT INTO deliveries VALUES(";
        InputStream inputFile = TableFunctions.class.getResourceAsStream(CONSTANTS.DATA_DELIVERIES.string);
        try (Scanner scanner = new Scanner(inputFile)) {
            while (scanner.hasNext()) {
                PreparedStatement preparedStatement = connection.prepareStatement(deliveryStatement + scanner.nextLine() + ");");
                preparedStatement.executeUpdate();
            }
        }
    }
}
