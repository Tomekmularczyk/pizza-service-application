package model;

public enum CONSTANTS {
    CSS_PATH("/view/application.css", 0),
    APPLICATION_ICON("/PizzaService.png", 0),
    ROOT_MAIN_WINDOW("/view/MainWindow.fxml", 0),
    ROOT_ADMINISTRATOR_WINDOW("/view/AdministratorWindow.fxml", 0),
    ROOT_EMPLOYEE_WINDOW("/view/EmployeeWindow.fxml", 0),
    ROOT_PREFERENCES("/view/PreferencesWindow.fxml", 0),
    ROOT_USER_EDIT("/view/UserEdit.fxml", 0),
    ROOT_EMPLOYEE_EDIT("/view/EmployeeEdit.fxml", 0),
    ROOT_EMPLOYEE_NEW("/view/EmployeeNew.fxml", 0),
    ROOT_SALE_WINDOW("/view/SaleWindow.fxml", 0),
    ROOT_ORDER_INFO("/view/OrderInfo.fxml", 0),
    ROOT_NEW_PIZZA("/view/NewPizza.fxml", 0),
    ROOT_PIZZA_EDIT("/view/PizzaEdit.fxml", 0),
    ROOT_NEW_PIZZA_SIZE("/view/NewSize.fxml", 0),
    ROOT_ADD_EMPLOYEE_ACCOUNT("/view/AddEmployeeAccount.fxml", 0),
    PREFERENCES_ID_SERVER("ID_SERVER", 0),
    PREFERENCES_ID_DATABASE("ID_DATABASE", 0),
    PREFERENCES_ID_USER("ID_USER", 0),
    PREFERENCES_ID_PASSWORD("ID_PASSWORD", 0),
    PREFERENCES_NODE_NAME("/model", 0),
    DEFAULT_SERVER_NAME("www.db4free.net", 0),
    DEFAULT_DATABASE_NAME("pizzaservice3", 0),
    DEFAULT_USER_NAME("pizzaadmin3", 0),
    DEFAULT_USER_PASSWORD("administrator", 0),
    ADMINISTRATOR_NAME("Admin", 0),
    ADMINISTRATOR_PASSWORD("admin", 0),
    DATA_EMPLOYEES("/DataEmployees.txt", 0),
    DATA_USERS("/DataUsers.txt", 0),
    DATA_PIZZA("/DataPizza.txt", 0),
    DATA_PIZZA_SIZE("/DataPizzaSize.txt", 0),
    DATA_SALES("/DataSales.txt", 0),
    DATA_DELIVERIES("/DataDelivery.txt", 0),
    FADE_OUT("", 1000),
    FADE_IN("", 1500);

    public String string;
    public Integer value;

    private CONSTANTS(String string, int value) {
        this.string = string;
        this.value = value;
    }
}
