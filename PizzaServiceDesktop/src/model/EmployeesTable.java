package model;

import classes.ExceptionDialog;
import classes.InfoTip;
import classes.ModalWindow;
import classes.SearchPane;
import controller.AddEmployeeAccountController;
import controller.EmployeeEditController;
import controller.EmployeeNewController;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Optional;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TableView;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import model.EmployeeData.EXISTS;

public class EmployeesTable extends TableView<EmployeeData> {

    private final ObservableList<EmployeeData> employeesList;

    public EmployeesTable() {
        super();
        employeesList = FXCollections.observableArrayList();
        getColumns().addAll(EmployeeData.getColumns(this));
        setItems(employeesList);
    }

    public ObservableList<EmployeeData> getEmployeesList() {
        return employeesList;
    }

    public boolean loadEmployeesTable() {
        boolean isOK = false;
        String query = "SELECT e.ID, e.name, e.surname, e.birthday, e.adres, e.telephone, u.login FROM employees AS e "
                + "LEFT JOIN users AS u on e.id = u.employee_id;";
        try (Connection connection = Main.dataSource.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    while (resultSet.next()) {
                        EmployeeData employee = new EmployeeData();
                        employee.setiD(resultSet.getInt(1));
                        employee.setName(resultSet.getString(2));
                        employee.setSurname(resultSet.getString(3));
                        employee.setBirthday(resultSet.getDate(4).toLocalDate());
                        employee.setAdress(resultSet.getString(5));
                        employee.setTelephone(resultSet.getString(6));
                        if (resultSet.getString(7) != null) {
                            employee.setAccount(EXISTS.YES);
                        }
                        employeesList.add(employee);
                    }
                    isOK = true;
                }
            }
        } catch (Exception exception) {
            ExceptionDialog exceptionDialog = new ExceptionDialog("Error while loading data from database", exception);
            exceptionDialog.showAndWait();
        }

        return isOK;
    }

    public void employeeEdit() {
        if (getSelectionModel().getSelectedIndex() >= 0) { // we check if something is selected
            FXMLLoader loader = new FXMLLoader(getClass().getResource(CONSTANTS.ROOT_EMPLOYEE_EDIT.string));
            BorderPane editPane;
            try {
                editPane = new BorderPane(loader.load());
                ModalWindow editWindow = new ModalWindow(Main.mainStage, editPane, "Edit employee");
                editWindow.setResizable(false);
                editWindow.getScene().getStylesheets().add(getClass().getResource(CONSTANTS.CSS_PATH.string).toExternalForm()); //its new stage with new scene, so we need to load this file again

                EmployeeData employeeData = (EmployeeData) getSelectionModel().getSelectedItem();
                EmployeeEditController employeeEditWindowController = loader.getController();
                employeeEditWindowController.fillWithData(employeeData);

                editWindow.initModality(Modality.APPLICATION_MODAL);
                editWindow.showAndWait();
            } catch (IOException exception) {
                ExceptionDialog exceptionDialog = new ExceptionDialog("Couldn't load EmployeeEdit", exception);
                exceptionDialog.showAndWait();
            }
        }
    }

    public void addAccount() {
        if (getSelectionModel().getSelectedIndex() >= 0) { // we check if something is selected
            FXMLLoader loader = new FXMLLoader(getClass().getResource(CONSTANTS.ROOT_ADD_EMPLOYEE_ACCOUNT.string));
            BorderPane editPane;
            try {
                editPane = new BorderPane(loader.load());
                ModalWindow editWindow = new ModalWindow(Main.mainStage, editPane, "Add employee account");
                editWindow.setResizable(false);
                editWindow.getScene().getStylesheets().add(getClass().getResource(CONSTANTS.CSS_PATH.string).toExternalForm()); //its new stage with new scene, so we need to load this file again

                EmployeeData employeeData = (EmployeeData) getSelectionModel().getSelectedItem();
                AddEmployeeAccountController addEmployeeAccountController = loader.getController();
                addEmployeeAccountController.fillWithData(employeeData);

                editWindow.initModality(Modality.APPLICATION_MODAL);
                editWindow.showAndWait();
            } catch (IOException exception) {
                ExceptionDialog exceptionDialog = new ExceptionDialog("Couldn't load EmployeeEdit", exception);
                exceptionDialog.showAndWait();
            }
        }
    }

    public void employeeDelete() {
        int indexOfEmployee = getSelectionModel().getSelectedIndex();
        if (indexOfEmployee >= 0) { // we check if something is selected
            EmployeeData employeeData = (EmployeeData) getSelectionModel().getSelectedItem();

            Alert alert = new Alert(AlertType.CONFIRMATION);
            alert.setTitle("Confirmation Dialog");
            alert.setHeaderText("ID: " + employeeData.getiD() + "\nName: " + employeeData.getName() + " " + employeeData.getSurname()
                    + "\nBorn: " + employeeData.getBirthday());
            alert.setContentText("Delete this employee?");

            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                String query = "DELETE FROM employees WHERE id = ?";
                try (Connection connection = Main.dataSource.getConnection()) {
                    try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                        preparedStatement.setInt(1, employeeData.getiD());
                        preparedStatement.executeUpdate();
                        getEmployeesList().remove(indexOfEmployee);
                    }
                } catch (Exception exception) {
                    ExceptionDialog exceptionDialog = new ExceptionDialog("Error while deleting employee from database", exception);
                    exceptionDialog.showAndWait();
                };
            }
        }
    }

    public void employeeNew(EmployeesTable employeesTable) {
        FXMLLoader loader = new FXMLLoader(getClass().getResource(CONSTANTS.ROOT_EMPLOYEE_NEW.string));
        BorderPane editPane;
        try {
            editPane = new BorderPane(loader.load());
            ModalWindow editWindow = new ModalWindow(Main.mainStage, editPane, "New employee");
            editWindow.setResizable(false);
            editWindow.getScene().getStylesheets().add(getClass().getResource(CONSTANTS.CSS_PATH.string).toExternalForm()); //its new stage with new scene, so we need to load this file again

            EmployeeNewController employeeNewController = loader.getController();
            employeeNewController.setEmployeesTable(employeesTable);

            editWindow.initModality(Modality.APPLICATION_MODAL);
            editWindow.showAndWait();
        } catch (IOException exception) {
            ExceptionDialog exceptionDialog = new ExceptionDialog("Couldn't load EmployeeNew", exception);
            exceptionDialog.showAndWait();
        }
    }

    public ModalWindow employeeSearch() {
        SearchPane searchPane = new SearchPane(6);
        searchPane.setNames("ID", "Name", "Surname", "Birthday", "Address", "Telephone");
        ModalWindow searchWindow = new ModalWindow(Main.mainStage, searchPane, "Search employees table");

        searchPane.setOnSearch(event -> {
            ArrayList<CheckBox> list = searchPane.getCheckBoxList();
            boolean iD = list.get(0).isSelected();
            boolean name = list.get(1).isSelected();
            boolean surname = list.get(2).isSelected();
            boolean birthday = list.get(3).isSelected();
            boolean address = list.get(4).isSelected();
            boolean telephone = list.get(5).isSelected();

            InfoTip infoTip = new InfoTip();
            if (searchPane.isSomethingSelected()) {
                if (!searchPane.getSearchPhrase().isEmpty()) {
                    findInDatabase(iD, name, surname, birthday, address, telephone, searchPane.getSearchPhrase());
                    searchWindow.close();
                } else {
                    infoTip.showTip(searchPane.getTextField(), "You need to type phrase to search");
                }
            } else {
                infoTip.showTip(searchPane.getCheckBoxList().get(0), "You need to select columns first");
            }
        });

        searchWindow.setResizable(false);
        searchWindow.getScene().getStylesheets().add(getClass().getResource(CONSTANTS.CSS_PATH.string).toExternalForm());
        searchWindow.initModality(Modality.APPLICATION_MODAL);
        searchWindow.show();

        return searchWindow;
    }

    private void findInDatabase(boolean iD, boolean name, boolean surname, boolean birthday, boolean address, boolean telephone, String text) {
        StringBuilder query = createQuery(iD, text, name, surname, birthday, address, telephone);
        try (Connection connection = Main.dataSource.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(query.toString())) {
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    employeesList.clear();
                    while (resultSet.next()) {
                        EmployeeData employee = new EmployeeData();
                        employee.setiD(resultSet.getInt(1));
                        employee.setName(resultSet.getString(2));
                        employee.setSurname(resultSet.getString(3));
                        employee.setBirthday(resultSet.getDate(4).toLocalDate());
                        employee.setAdress(resultSet.getString(5));
                        employee.setTelephone(resultSet.getString(6));
                        if (resultSet.getString(7) != null) {
                            employee.setAccount(EXISTS.YES);
                        }
                        employeesList.add(employee);
                    }
                }
            }
        } catch (Exception ex) {
            ExceptionDialog exceptionDialog = new ExceptionDialog("Couldn't search employees table", ex);
            exceptionDialog.showAndWait();
        }
    }

    private StringBuilder createQuery(boolean iD, String text, boolean name, boolean surname, boolean birthday, boolean address, boolean telephone) {
        final String phrase = "LIKE '%" + text + "%')";
        StringBuilder query = new StringBuilder("SELECT e.ID, e.name, e.surname, e.birthday, e.adres, e.telephone, u.login FROM employees AS e "
                + "LEFT JOIN users AS u on e.id = u.employee_id WHERE ");
        query.append(iD ? "(e.id " + phrase : "");
        if (name && iD) {
            query.append(" OR ");
        }
        query.append(name ? "(e.name " + phrase : "");
        if (surname && (name || iD)) {
            query.append(" OR ");
        }
        query.append((surname) ? "(e.surname " + phrase : "");
        if (birthday && (name || iD || surname)) {
            query.append(" OR ");
        }
        query.append(birthday ? "(e.birthday " + phrase : "");
        if (address && (name || iD || surname || birthday)) {
            query.append(" OR ");
        }
        query.append((address) ? "(e.adres " + phrase : "");
        if (telephone && (name || iD || surname || birthday || address)) {
            query.append(" OR ");
        }
        query.append(telephone ? "(e.telephone " + phrase : "");
        return query;
    }

    public void employeeEditUser(EmployeesTable employeesTable) {
        if (employeesTable.getSelectionModel().getSelectedIndex() >= 0) { // we check if something is selected
            EmployeeData employeeData = (EmployeeData) employeesTable.getSelectionModel().getSelectedItem();

            if (employeeData.getAccount() == EXISTS.YES) {
                //now we need to load this employee account informations
                String query = "SELECT e.id, e.name, e.surname, e.birthday, u.login, u.password FROM employees AS e "
                        + "RIGHT JOIN users AS u ON e.id = u.employee_id WHERE id = ? ORDER BY e.id;";
                try (Connection connection = Main.dataSource.getConnection()) {
                    try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                        preparedStatement.setInt(1, employeeData.getiD());
                        try (ResultSet resultSet = preparedStatement.executeQuery()) {
                            //--- working with result
                            resultSet.next();
                            UserData userData = new UserData();
                            userData.setiD(resultSet.getInt(1));
                            userData.setName(resultSet.getString(2));
                            userData.setSurname(resultSet.getString(3));
                            userData.setBirthday(resultSet.getDate(4).toLocalDate());
                            userData.setLogin(resultSet.getString(5));
                            userData.setPassword(resultSet.getString(6));

                            //now we're loading edition window
                            UsersTable.loadUserEdit(userData);
                        }
                    }
                } catch (Exception exception) {
                    ExceptionDialog exceptionDialog = new ExceptionDialog("Error while loading data from database", exception);
                    exceptionDialog.showAndWait();
                }
            } else { //employee has no account
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Information");
                alert.setHeaderText(null);
                alert.setContentText("Employee with ID: " + employeeData.getiD() + " has no account.");
                alert.showAndWait();
            }
        }
    }

}
