package model;

import classes.ExceptionDialog;
import classes.InfoTip;
import classes.ModalWindow;
import classes.SearchPane;
import controller.OrderInfoController;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TableView;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import model.EmployeeData.EXISTS;
import model.SaleData.ORDER_STATUS;

public class DeliveriesTable extends TableView<DeliveryData> {

    private ObservableList<DeliveryData> deliveriesList;

    public DeliveriesTable() {
        super();
        deliveriesList = FXCollections.observableArrayList();
        getColumns().addAll(DeliveryData.getColumns(this));
        setItems(deliveriesList);
    }

    public ObservableList<DeliveryData> getDeliveriesList() {
        return deliveriesList;
    }

    public boolean loadDeliveriesTable() {
        boolean isOK = false;
        //---Loading data
        String query = "SELECT d.*, s.order_date FROM deliveries AS d, sales AS s WHERE d.order_id = s.id;";
        try (Connection connection = Main.dataSource.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    //--- working with result
                    while (resultSet.next()) {
                        DeliveryData deliveryData = new DeliveryData();
                        deliveryData.setOrderID(resultSet.getInt(1));
                        deliveryData.setOrderDate(resultSet.getTimestamp(6));
                        deliveryData.setAdress(resultSet.getString(2));
                        deliveryData.setClientName(resultSet.getString(3));
                        deliveryData.setTelephone(resultSet.getString(4));
                        deliveryData.setCost(resultSet.getDouble(5));
                        deliveriesList.add(deliveryData);
                    }
                    isOK = true;
                }
            }
        } catch (Exception exception) {
            ExceptionDialog exceptionDialog = new ExceptionDialog("Error while loading data from database", exception);
            exceptionDialog.showAndWait();
        };
        return isOK;
    }

    public ModalWindow showSaleInfo() { //returns references in case we want to keep list of opened windows with info
        ModalWindow infoWindow = null;
        if (getSelectionModel().getSelectedIndex() >= 0) { // we check if something is selected
            FXMLLoader loader = new FXMLLoader(getClass().getResource(CONSTANTS.ROOT_ORDER_INFO.string));
            BorderPane editPane;
            try {
                DeliveryData deliveryData = (DeliveryData) getSelectionModel().getSelectedItem();
                SaleData saleData = findSale(deliveryData.getOrderID());
                if (saleData != null) {
                    editPane = new BorderPane(loader.load());
                    infoWindow = new ModalWindow(Main.mainStage, editPane, "Order info");
                    infoWindow.setResizable(false);
                    infoWindow.getScene().getStylesheets().add(getClass().getResource(CONSTANTS.CSS_PATH.string).toExternalForm()); //its new stage with new scene, so we need to load this file again	

                    OrderInfoController orderInfoController = loader.getController();
                    orderInfoController.fillWithData(saleData);

                    infoWindow.initModality(Modality.APPLICATION_MODAL);
                    infoWindow.show();
                } else { //couldnt load sale

                }
            } catch (IOException exception) {
                ExceptionDialog exceptionDialog = new ExceptionDialog("Couldn't load OrderInfo", exception);
                exceptionDialog.showAndWait();
            }
        }
        return infoWindow;
    }

    private SaleData findSale(Integer ID) {
        SaleData sale = null;

        final String query = "SELECT s.id, s.order_date, s.status, s.user_id, s.pizza_id, s.pizza_size, s.income, d.order_id FROM sales AS s "
                + "LEFT JOIN deliveries AS d ON s.id = d.order_id WHERE id = " + ID + " ORDER BY s.id ASC";
        try (Connection connection = Main.dataSource.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    if (resultSet.next()) {
                        sale = new SaleData();
                        sale.setiD(resultSet.getInt(1));
                        sale.setOrderDate(resultSet.getTimestamp(2));
                        sale.setOrderStatus(ORDER_STATUS.valueOf(resultSet.getString(3)));
                        sale.setUser(resultSet.getString(4));
                        sale.setPizzaNumber(resultSet.getInt(5));
                        sale.setPizzaSize(resultSet.getInt(6));
                        sale.setIncome(resultSet.getDouble(7));
                        if (resultSet.getInt(8) != 0) {
                            sale.setDelivery(EXISTS.YES);
                        }
                    } else {
                        ExceptionDialog exceptionDialog = new ExceptionDialog("there is no sale with id " + ID, new Exception());
                        exceptionDialog.showAndWait();
                    }
                }
            }
        } catch (Exception e) {
            ExceptionDialog exceptionDialog = new ExceptionDialog("Error while loading sale from database", e);
            exceptionDialog.showAndWait();
        }

        return sale;
    }

    public ModalWindow deliverySearch() {
        SearchPane searchPane = new SearchPane(5);
        searchPane.setNames("Order ID", "Address", "Client name", "Telephone", "Cost");
        ModalWindow searchWindow = new ModalWindow(Main.mainStage, searchPane, "Search deliveries table");

        searchPane.setOnSearch(event -> {
            ArrayList<CheckBox> list = searchPane.getCheckBoxList();
            boolean orderiD = list.get(0).isSelected();
            boolean address = list.get(1).isSelected();
            boolean clientName = list.get(2).isSelected();
            boolean telephone = list.get(3).isSelected();
            boolean cost = list.get(4).isSelected();

            InfoTip infoTip = new InfoTip();
            if (searchPane.isSomethingSelected()) {
                if (!searchPane.getSearchPhrase().isEmpty()) {
                    findInDatabase(orderiD, address, clientName, telephone, cost, searchPane.getSearchPhrase());
                    searchWindow.close();
                } else {
                    infoTip.showTip(searchPane.getTextField(), "You need to type phrase to search");
                }
            } else {
                infoTip.showTip(searchPane.getCheckBoxList().get(0), "You need to select columns first");
            }
        });

        searchWindow.setResizable(false);
        searchWindow.getScene().getStylesheets().add(getClass().getResource(CONSTANTS.CSS_PATH.string).toExternalForm());
        searchWindow.initModality(Modality.APPLICATION_MODAL);
        searchWindow.show();

        return searchWindow;
    }

    private void findInDatabase(boolean orderID, boolean address, boolean clientName, boolean telephone, boolean cost, String text) {
        final String phrase = "LIKE '%" + text + "%')";

        StringBuilder query = createQuery(orderID, phrase, address, clientName, telephone, cost);
        try (Connection connection = Main.dataSource.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(query.toString())) {
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    deliveriesList.clear();
                    while (resultSet.next()) {
                        DeliveryData deliveryData = new DeliveryData();
                        deliveryData.setOrderID(resultSet.getInt(1));
                        deliveryData.setOrderDate(resultSet.getTimestamp(6));
                        deliveryData.setAdress(resultSet.getString(2));
                        deliveryData.setClientName(resultSet.getString(3));
                        deliveryData.setTelephone(resultSet.getString(4));
                        deliveryData.setCost(resultSet.getDouble(5));
                        deliveriesList.add(deliveryData);
                    }
                }
            }
        } catch (Exception ex) {
            ExceptionDialog exceptionDialog = new ExceptionDialog("Couldn't search employees table", ex);
            exceptionDialog.showAndWait();
        }
    }

    private StringBuilder createQuery(boolean orderID, final String phrase, boolean address, boolean clientName, boolean telephone, boolean cost) {
        StringBuilder query = new StringBuilder("SELECT d.*, s.order_date FROM deliveries AS d, sales AS s WHERE (d.order_id = s.id) AND (");
        query.append((orderID) ? "(d.order_id " + phrase : "");
        if (address && orderID) {
            query.append(" OR ");
        }
        query.append(address ? "(d.adress " + phrase : "");
        if (clientName && (orderID || address)) {
            query.append(" OR ");
        }
        query.append((clientName) ? "(d.client_name " + phrase : "");
        if (telephone && (orderID || address || clientName)) {
            query.append(" OR ");
        }
        query.append((telephone) ? "(d.telephone " + phrase : "");
        if (cost && (orderID || address || clientName || telephone)) {
            query.append(" OR ");
        }
        query.append((cost) ? "(d.cost " + phrase : "");
        query.append(");");
        return query;
    }
}
