package model;

import java.util.ArrayList;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

public class PizzaData {

    private SimpleIntegerProperty iD;
    private SimpleStringProperty name;
    private SimpleStringProperty composition;
    private SimpleDoubleProperty price;
    private SimpleObjectProperty<AVAILABLE> visible;

    public enum AVAILABLE {
        Yes, No
    }

    public PizzaData() {
        iD = new SimpleIntegerProperty();
        name = new SimpleStringProperty();
        price = new SimpleDoubleProperty();
        composition = new SimpleStringProperty();
        visible = new SimpleObjectProperty<>();
    }

    public SimpleIntegerProperty getIDProperty() {
        return iD;
    }

    public void setIDProperty(SimpleIntegerProperty iD) {
        this.iD = iD;
    }

    public SimpleStringProperty getNameProperty() {
        return name;
    }

    public void setNameProperty(SimpleStringProperty name) {
        this.name = name;
    }

    public SimpleStringProperty getCompositionProperty() {
        return composition;
    }

    public void setCompositionProperty(SimpleStringProperty composition) {
        this.composition = composition;
    }

    public SimpleDoubleProperty getPriceProperty() {
        return price;
    }

    public void setPriceProperty(SimpleDoubleProperty price) {
        this.price = price;
    }

    public SimpleObjectProperty<AVAILABLE> getAvailableProperty() {
        return visible;
    }

    public void setAvailableProperty(SimpleObjectProperty<AVAILABLE> visible) {
        this.visible = visible;
    }

    public Integer getID() {
        return iD.get();
    }

    public void setID(Integer iD) {
        this.iD.set(iD);
    }

    public String getName() {
        return name.get();
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public String getComposition() {
        return composition.get();
    }

    public void setComposition(String composition) {
        this.composition.set(composition);
    }

    public Double getPrice() {
        return price.get();
    }

    public void setPrice(Double price) {
        this.price.set(price);
    }

    public AVAILABLE getAvailable() {
        return visible.get();
    }

    public void setAvailable(AVAILABLE visible) {
        this.visible.set(visible);
    }

    public static ArrayList<TableColumn<PizzaData, ?>> getColumns(TableView<PizzaData> table) {
        ArrayList<TableColumn<PizzaData, ?>> columns = new ArrayList<>();
        double[] columnWidth = {5, 24, 51, 10, 8}; //percentage values

        TableColumn<PizzaData, Number> iD = new TableColumn<>("ID");
        TableColumn<PizzaData, String> name = new TableColumn<>("Name");
        TableColumn<PizzaData, String> composition = new TableColumn<>("Composition");
        TableColumn<PizzaData, Number> price = new TableColumn<>("Price");
        TableColumn<PizzaData, AVAILABLE> available = new TableColumn<>("Available");

        int i = 0;
        iD.prefWidthProperty().bind(table.widthProperty().divide(100 / columnWidth[i++]));
        name.prefWidthProperty().bind(table.widthProperty().divide(100 / columnWidth[i++]));
        composition.prefWidthProperty().bind(table.widthProperty().divide(100 / columnWidth[i++]));
        price.prefWidthProperty().bind(table.widthProperty().divide(100 / columnWidth[i++]));
        available.prefWidthProperty().bind(table.widthProperty().divide(100 / columnWidth[i++]));

        iD.setCellValueFactory(cellData -> cellData.getValue().getIDProperty());
        name.setCellValueFactory(cellData -> cellData.getValue().getNameProperty());
        composition.setCellValueFactory(cellData -> cellData.getValue().getCompositionProperty());
        price.setCellValueFactory(cellData -> cellData.getValue().getPriceProperty());
        available.setCellValueFactory(cellData -> cellData.getValue().getAvailableProperty());

        columns.add(iD);
        columns.add(name);
        columns.add(composition);
        columns.add(price);
        columns.add(available);

        return columns;
    }
}
