package model;

import classes.ExceptionDialog;
import classes.InfoTip;
import classes.ModalWindow;
import classes.SearchPane;
import controller.OrderInfoController;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Optional;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableView;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import model.EmployeeData.EXISTS;
import model.SaleData.ORDER_STATUS;

public class SalesTable extends TableView<SaleData> {

    private ObservableList<SaleData> salesList;
    private String since; //when we refresh data we need 
    boolean dynamicPendingList = false;  //we need to know if we should delete from table rows with status 'Finalised' or 'Canceled'

    public SalesTable() {
        super();
        salesList = FXCollections.observableArrayList();

        getColumns().addAll(SaleData.getColumns(this));
        setItems(salesList);
    }

    public void addToTable(SaleData sale) {
        salesList.add(sale);
    }

    public boolean loadAllSalesData() {
        String query = "SELECT s.id, s.order_date, s.status, s.user_id, s.pizza_id, s.pizza_size, s.income, d.order_id FROM sales AS s "
                + "LEFT JOIN deliveries AS d ON s.id = d.order_id ORDER BY s.id ASC";
        return loadData(query, false);
    }

    private boolean loadData(String query, boolean add) { // add - wherever we want to add something to end of list
        if (add == false) {
            salesList.clear();
        }

        boolean isOK = false;
        try (Connection connection = Main.dataSource.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    while (resultSet.next()) {
                        SaleData saleData = new SaleData();
                        saleData.setiD(resultSet.getInt(1));
                        saleData.setOrderDate(resultSet.getTimestamp(2));
                        saleData.setOrderStatus(ORDER_STATUS.valueOf(resultSet.getString(3)));
                        saleData.setUser(resultSet.getString(4));
                        saleData.setPizzaNumber(resultSet.getInt(5));
                        saleData.setPizzaSize(resultSet.getInt(6));
                        saleData.setIncome(resultSet.getDouble(7));
                        if (resultSet.getInt(8) != 0) {
                            saleData.setDelivery(EXISTS.YES);
                        }
                        salesList.add(saleData);
                    }
                    isOK = true;
                }
            }
        } catch (Exception exception) {
            ExceptionDialog exceptionDialog = new ExceptionDialog("Error while loading sales data from database", exception);
            exceptionDialog.showAndWait();
        };

        return isOK;
    }

    public void refreshData() {//adds new orders and updates status of orders
        int lastOrderID = findHighestID();

        String query = "SELECT s.id, s.order_date, s.status, s.user_id, s.pizza_id, s.pizza_size, s.income, d.order_id FROM sales AS s "
                + "LEFT JOIN deliveries AS d ON s.id = d.order_id ";

        if (dynamicPendingList) {
            String dynamicCondition = "WHERE (s.status <> 'Finalised' AND s.status <> 'Canceled') AND (s.id > " + lastOrderID + ") ORDER BY s.id ASC";
            query += dynamicCondition;
        } else {
            String intervalCondition = "WHERE s.id > " + lastOrderID + " AND s.order_date > '" + since + "' ORDER BY s.id ASC";
            query += intervalCondition;
        }
        loadData(query, true);

        //update status for table from database
        refreshStatuses();
    }

    private void refreshStatuses() { //we refresh data for every single row in table
        String query = "SELECT s.id, s.order_date, s.status, s.user_id, s.pizza_id, s.pizza_size, s.income, d.order_id FROM sales AS s "
                + "LEFT JOIN deliveries AS d ON s.id = d.order_id WHERE s.id = ?";
        try (Connection connection = Main.dataSource.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                for (int i = 0; i < salesList.size(); i++) {
                    SaleData saleData = salesList.get(i);
                    preparedStatement.setInt(1, saleData.getID());
                    try (ResultSet resultSet = preparedStatement.executeQuery()) {
                        //--- working with result
                        if (resultSet.next()) {
                            ORDER_STATUS newStatus = ORDER_STATUS.valueOf(resultSet.getString(3));
                            saleData.setOrderStatus(newStatus);
                            if (dynamicPendingList) { //we need to delete finished and canceled orders from table
                                if (newStatus.equals(ORDER_STATUS.Canceled) || newStatus.equals(ORDER_STATUS.Finalised)) {
                                    salesList.remove(i);
                                }
                            }
                        } else {
                            System.out.println("Couldnt find sale with ID=" + saleData.getID() + " in database");
                        }
                    }
                }
            }
        } catch (Exception exception) {
            ExceptionDialog exceptionDialog = new ExceptionDialog("Error while loading sales data from database", exception);
            exceptionDialog.showAndWait();
        };
    }

    private int findHighestID() {
        int id = 0;
        for (SaleData sale : salesList) {
            if (sale.getID() > id) {
                id = sale.getID();
            }
        }
        return id;
    }

    public void changeOrderStatus(ORDER_STATUS status) {
        int indexOfEmployee = getSelectionModel().getSelectedIndex();
        if (indexOfEmployee >= 0) { // we check if something is selected
            SaleData saleData = (SaleData) getSelectionModel().getSelectedItem();

            String query = "UPDATE sales SET status = ? WHERE id = ?";
            try (Connection connection = Main.dataSource.getConnection()) {
                try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                    preparedStatement.setString(1, status.toString());
                    preparedStatement.setInt(2, saleData.getID());
                    preparedStatement.executeUpdate();
                    salesList.get(indexOfEmployee).setOrderStatus(status);
                }
            } catch (Exception exception) {
                ExceptionDialog exceptionDialog = new ExceptionDialog("Error while loading data from database", exception);
                exceptionDialog.showAndWait();
            }
        }
    }

    public ModalWindow showSaleInfo(boolean isModal) { //returns references in case we want to keep list of opened windows with info
        ModalWindow infoWindow = null;
        if (getSelectionModel().getSelectedIndex() >= 0) { // we check if something is selected
            FXMLLoader loader = new FXMLLoader(getClass().getResource(CONSTANTS.ROOT_ORDER_INFO.string));
            BorderPane editPane;
            try {
                editPane = new BorderPane(loader.load());
                infoWindow = new ModalWindow(Main.mainStage, editPane, "Order info");
                infoWindow.setResizable(false);
                infoWindow.getScene().getStylesheets().add(getClass().getResource(CONSTANTS.CSS_PATH.string).toExternalForm()); //its new stage with new scene, so we need to load this file again

                SaleData saleData = (SaleData) getSelectionModel().getSelectedItem();
                OrderInfoController orderInfoController = loader.getController();
                orderInfoController.fillWithData(saleData);

                if (isModal) {
                    infoWindow.initModality(Modality.APPLICATION_MODAL);
                }
                infoWindow.show();
            } catch (IOException exception) {
                ExceptionDialog exceptionDialog = new ExceptionDialog("Couldn't load OrderInfo", exception);
                exceptionDialog.showAndWait();
            }
        }
        return infoWindow;
    }

    public void delete() {
        int indexOfItem = getSelectionModel().getSelectedIndex();
        if (indexOfItem >= 0) { // we check if something is selected
            SaleData saleData = (SaleData) getSelectionModel().getSelectedItem();

            Alert alert = new Alert(AlertType.CONFIRMATION);
            alert.setTitle("Confirmation Dialog");
            String delivery = (saleData.getDelivery() != null) ? "YES" : "NO";
            alert.setHeaderText("ID: " + saleData.getID() + ", date: " + saleData.getOrderDate() + "\nDelivery: " + delivery);
            alert.setContentText("Delete this sale from table?");

            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                String query = "DELETE FROM sales WHERE id = ?";
                try (Connection connection = Main.dataSource.getConnection()) {
                    try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                        preparedStatement.setInt(1, saleData.getID());
                        preparedStatement.executeUpdate();
                        salesList.remove(indexOfItem);
                    }
                } catch (Exception exception) {
                    ExceptionDialog exceptionDialog = new ExceptionDialog("Error while deleting sale data", exception);
                    exceptionDialog.showAndWait();
                };
            }
        }
    }

    public ModalWindow salesSearch() {
        SearchPane searchPane = new SearchPane(7);
        searchPane.setNames("ID", "Order date", "Status", "User name", "Pizza ID", "Pizza size", "Income");
        ModalWindow searchWindow = new ModalWindow(Main.mainStage, searchPane, "Search sales table");

        searchPane.setOnSearch(event -> {
            ArrayList<CheckBox> list = searchPane.getCheckBoxList();
            boolean iD = list.get(0).isSelected();
            boolean date = list.get(1).isSelected();
            boolean status = list.get(2).isSelected();
            boolean user = list.get(3).isSelected();
            boolean pizzaID = list.get(4).isSelected();
            boolean pizzaSize = list.get(5).isSelected();
            boolean income = list.get(6).isSelected();

            InfoTip infoTip = new InfoTip();
            if (searchPane.isSomethingSelected() == true) {
                if (!searchPane.getSearchPhrase().isEmpty()) {
                    findInDatabase(iD, date, status, user, pizzaID, pizzaSize, income, searchPane.getSearchPhrase());
                    searchWindow.close();
                } else {
                    infoTip.showTip(searchPane.getTextField(), "You need to type phrase to search");
                }
            } else {
                infoTip.showTip(searchPane.getCheckBoxList().get(0), "You need to select columns first");
            }
        });

        searchWindow.setResizable(false);
        searchWindow.getScene().getStylesheets().add(getClass().getResource(CONSTANTS.CSS_PATH.string).toExternalForm());
        searchWindow.initModality(Modality.APPLICATION_MODAL);
        searchWindow.show();

        return searchWindow;
    }

    private void findInDatabase(boolean iD, boolean date, boolean status, boolean user, boolean pizzaID, boolean pizzaSize, boolean income, String text) {
        StringBuilder query = createQuery(text, iD, date, status, user, pizzaID, pizzaSize, income);

        try (Connection connection = Main.dataSource.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(query.toString())) {
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    salesList.clear();
                    while (resultSet.next()) {
                        SaleData saleData = new SaleData();
                        saleData.setiD(resultSet.getInt(1));
                        saleData.setOrderDate(resultSet.getTimestamp(2));
                        saleData.setOrderStatus(ORDER_STATUS.valueOf(resultSet.getString(3)));
                        saleData.setUser(resultSet.getString(4));
                        saleData.setPizzaNumber(resultSet.getInt(5));
                        saleData.setPizzaSize(resultSet.getInt(6));
                        saleData.setIncome(resultSet.getDouble(7));
                        if (resultSet.getInt(8) != 0) {
                            saleData.setDelivery(EXISTS.YES);
                        }
                        salesList.add(saleData);
                    }
                }
            }
        } catch (Exception ex) {
            ExceptionDialog exceptionDialog = new ExceptionDialog("Couldn't search sales table", ex);
            exceptionDialog.showAndWait();
        }
    }

    private StringBuilder createQuery(String text, boolean iD, boolean date, boolean status, boolean user, boolean pizzaID, boolean pizzaSize, boolean income) {
        final String phrase = "LIKE '%" + text + "%')";
        StringBuilder query = new StringBuilder("SELECT s.id, s.order_date, s.status, s.user_id, s.pizza_id, s.pizza_size, s.income, d.order_id FROM sales AS s "
                + "LEFT JOIN deliveries AS d ON s.id = d.order_id WHERE ");
        query.append(iD ? "(s.id " + phrase : "");
        if (date && iD) {
            query.append(" OR ");
        }
        query.append(date ? "(s.order_date " + phrase : "");
        if (status && (iD || date)) {
            query.append(" OR ");
        }
        query.append((status) ? "(s.status " + phrase : "");
        if (user && (iD || date || status)) {
            query.append(" OR ");
        }
        query.append(user ? "(s.user_id " + phrase : "");
        if (pizzaID && (iD || date || status || user)) {
            query.append(" OR ");
        }
        query.append(pizzaID ? "(s.pizza_id " + phrase : "");
        if (pizzaSize && (iD || date || status || user || pizzaID)) {
            query.append(" OR ");
        }
        query.append(pizzaSize ? "(s.pizza_size " + phrase : "");
        if (income && (iD || date || status || user || pizzaID || pizzaSize)) {
            query.append(" OR ");
        }
        query.append(income ? "(s.income " + phrase : "");
        query.append(" ORDER BY s.id ASC;");
        return query;
    }

    //================================= Data selecting methods
    public void loadLast24hData() {
        dynamicPendingList = false;
        Timestamp today = new Timestamp(new Date().getTime());

        Calendar yesterday = Calendar.getInstance();
        yesterday.setTime(today);
        yesterday.add(Calendar.HOUR_OF_DAY, -23);

        since = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(yesterday.getTime());
        String query = "SELECT s.id, s.order_date, s.status, s.user_id, s.pizza_id, s.pizza_size, s.income, d.order_id FROM sales AS s "
                + "LEFT JOIN deliveries AS d ON s.id = d.order_id WHERE s.order_date > '" + since + "' ORDER BY s.id ASC";
        loadData(query, false);
    }

    public void loadLastWeekData() {
        dynamicPendingList = false;
        Timestamp today = new Timestamp(new Date().getTime());

        Calendar lastWeek = Calendar.getInstance();
        lastWeek.setTime(today);
        lastWeek.add(Calendar.DATE, -7);

        since = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(lastWeek.getTime());
        String query = "SELECT s.id, s.order_date, s.status, s.user_id, s.pizza_id, s.pizza_size, s.income, d.order_id FROM sales AS s "
                + "LEFT JOIN deliveries AS d ON s.id = d.order_id WHERE s.order_date > '" + since + "' ORDER BY s.id ASC";
        loadData(query, false);
    }

    public void loadLastMonthData() {
        dynamicPendingList = false;
        Timestamp today = new Timestamp(new Date().getTime());

        Calendar lastMonth = Calendar.getInstance();
        lastMonth.setTime(today);
        lastMonth.add(Calendar.DATE, -30);

        since = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(lastMonth.getTime());
        String query = "SELECT s.id, s.order_date, s.status, s.user_id, s.pizza_id, s.pizza_size, s.income, d.order_id FROM sales AS s "
                + "LEFT JOIN deliveries AS d ON s.id = d.order_id WHERE s.order_date > '" + since + "' ORDER BY s.id ASC";
        loadData(query, false);
    }

    public void loadPendingSalesData() {
        dynamicPendingList = true;
        String query = "SELECT s.id, s.order_date, s.status, s.user_id, s.pizza_id, s.pizza_size, s.income, d.order_id FROM sales AS s "
                + "LEFT JOIN deliveries AS d ON s.id = d.order_id WHERE "
                + "s.status <> 'Finalised' AND s.status <> 'Canceled' ORDER BY s.id ASC";
        loadData(query, false);
    }

    //----------------------------------------------------------
    public class StatusTableCell extends TableCell<SaleData, ORDER_STATUS> {

        public StatusTableCell() {
            super();
        }

        @Override
        protected void updateItem(ORDER_STATUS item, boolean empty) {
            super.updateItem(item, empty);
        }

    }
}
