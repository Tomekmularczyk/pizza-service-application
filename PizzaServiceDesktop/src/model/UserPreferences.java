package model;

public final class UserPreferences {

    private static final Data data = new Data();

    public static void saveServerName(String serverName) {
        data.server = serverName;
    }

    public static void saveDatabaseName(String databaseName) {
        data.database = databaseName;
    }

    public static void saveUserName(String userName) {
        data.user = userName;
    }

    public static void saveUserPassword(String userPassword) {
        data.password = userPassword;
    }

    public static String getServerName() {
        return data.server;
    }

    public static String getDatabaseName() {
        return data.database;
    }

    public static String getUserName() {
        return data.user;
    }

    public static String getUserPassword() {
        return data.password;
    }

    public static String getDefaultServerName() {
        return CONSTANTS.DEFAULT_SERVER_NAME.string;
    }

    public static String getDefaultDatabaseName() {
        return CONSTANTS.DEFAULT_DATABASE_NAME.string;
    }

    public static String getDefaultUserName() {
        return CONSTANTS.DEFAULT_USER_NAME.string;
    }

    public static String getDefaultUserPassword() {
        return CONSTANTS.DEFAULT_USER_PASSWORD.string;
    }

    private static class Data {
        public String server = getDefaultServerName(),
                database = getDefaultDatabaseName(),
                user = getDefaultUserName(),
                password = getDefaultUserPassword();
    }
}
