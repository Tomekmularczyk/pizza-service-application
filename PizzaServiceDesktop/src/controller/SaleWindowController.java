package controller;

import classes.Controller;
import classes.ExceptionDialog;
import classes.InfoTip;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import model.EmployeeData.EXISTS;
import model.Main;
import model.PizzaData;
import model.PizzaData.AVAILABLE;
import model.PizzaSizeData;
import model.PizzaSizeTable;
import model.PizzasTable;
import model.SaleData;
import model.SaleData.ORDER_STATUS;
import model.SalesTable;
import model.UserData;

public class SaleWindowController extends Controller implements Initializable {

    private UserData currentUser;
    private ObservableList<PizzaData> pizzaList;
    private ObservableList<PizzaSizeData> pizzaSizeList;
    private SalesTable salesTable; //reference to table

    @FXML
    private BorderPane borderPane, borderPaneInside;
    @FXML
    private VBox vBoxDelivery;
    @FXML
    private ToggleGroup groupDelivery;
    @FXML
    private RadioButton radioNo, radioYes;
    @FXML
    private ComboBox<String> comboPizza;
    @FXML
    private ComboBox<Integer> comboPizzaSize;
    @FXML
    private TextField textFAdress, textFClientName, textFCost, textFTelephone;

    @FXML
    private void buttSellAction(ActionEvent event) {
        InfoTip infoTip = new InfoTip();
        if (validatePizzaSelected()) {
            PizzaData selectedPizza = getPizzaData(comboPizza.getValue());
            PizzaSizeData selectedPizzaSize = getPizzaSizeData(comboPizzaSize.getValue());
            Date date = new Date();
            Timestamp timestamp = new Timestamp(date.getTime());

            if (selectedPizza != null) {
                if (groupDelivery.getSelectedToggle().equals(radioNo)) { // sale without delivery
                    Alert alert = new Alert(AlertType.CONFIRMATION);
                    alert.setTitle("Confirm");
                    alert.setHeaderText("Total cost: " + String.format("%.2f", selectedPizza.getPrice() + selectedPizzaSize.getPrice()));
                    alert.setContentText("Proceed with sale?");

                    Optional<ButtonType> result = alert.showAndWait();
                    if (result.get() == ButtonType.OK) {
                        insertSale(timestamp, currentUser.getLogin(), selectedPizza.getID(),
                                selectedPizzaSize.getSize(), false, selectedPizza.getPrice() + selectedPizzaSize.getPrice());

                        infoTip.getInfoTip().setId("greenInfoTip");
                        infoTip.showTip((Button) event.getSource(), "   Saved   ");
                    }
                } else { //Sale with delivery
                    String adress = textFAdress.getText();
                    String clientName = textFClientName.getText();
                    String telephone = textFTelephone.getText();
                    String deliveryCost = textFCost.getText();

                    if (validateSaleDate(adress, clientName, telephone, deliveryCost)) {
                        double totalCost = selectedPizza.getPrice() + selectedPizzaSize.getPrice() + Double.valueOf(deliveryCost);
                        //everything is okey
                        Alert alert = new Alert(AlertType.CONFIRMATION);
                        alert.setTitle("Confirm");
                        alert.setHeaderText("Total cost: " + totalCost);
                        alert.setContentText("Proceed with sale?");

                        Optional<ButtonType> result = alert.showAndWait();
                        if (result.get() == ButtonType.OK) {
                            int id = insertSale(timestamp, currentUser.getLogin(), selectedPizza.getID(),
                                    selectedPizzaSize.getSize(), true, selectedPizza.getPrice() + selectedPizzaSize.getPrice());

                            insertDelivery(id, adress, clientName, telephone, Double.valueOf(deliveryCost));

                            infoTip.getInfoTip().setId("greenInfoTip");
                            infoTip.showTip((Button) event.getSource(), "   Saved   ");
                        } else {
                            // ... user chose CANCEL or closed the dialog
                        }
                    }

                }
            } else { //couldnt found selected pizza in pizzaList(which should not be possible)
                ExceptionDialog exceptionDialog = new ExceptionDialog("Error when searching for selected pizza", new Exception());
                exceptionDialog.showAndWait();
            }
        }
    }

    private boolean validatePizzaSelected() {
        InfoTip infoTip = new InfoTip();
        if (comboPizza.getValue() == null) {
            infoTip.showTip(comboPizza, "select pizza");
            return false;
        }

        if (comboPizzaSize.getValue() == null) {
            infoTip.showTip(comboPizzaSize, "select pizza size");
            return false;
        }

        return true;
    }

    private boolean validateSaleDate(String address, String clientName, String telephone, String deliveryCost) {
        boolean isAddressOK = address.length() < 51 && !address.isEmpty();
        boolean isClientNameOK = clientName.length() < 36;
        boolean isTelephoneOK = telephone.length() < 21 && !telephone.isEmpty();
        boolean isCostOK;
        try {
            Double.valueOf(deliveryCost);
            isCostOK = true;
        } catch (NumberFormatException exception) {
            isCostOK = false;
        }

        InfoTip infoTip = new InfoTip();
        if (!isAddressOK) {
            infoTip.showTip(textFAdress, "max 50 characters, not empty");
            return false;
        }

        if (!isClientNameOK) {
            infoTip.showTip(textFClientName, "max 35 characters");
            return false;
        }

        if (!isTelephoneOK) {
            infoTip.showTip(textFTelephone, "max 20 characters, not empty");
            return false;
        }

        if (!isCostOK) {
            infoTip.showTip(textFCost, "keep right format e.g. 4.35");
            return false;
        }

        return true;
    }

    @FXML
    private void buttClearAllAction(ActionEvent event) {
        textFAdress.clear();
        textFClientName.clear();
        textFTelephone.clear();
        textFCost.clear();
        comboPizza.setValue(null);
        comboPizzaSize.setValue(null);
    }

    @FXML
    private void radioNoAction(ActionEvent event) {
        vBoxDelivery.setDisable(true);
    }

    @FXML
    private void radioYesAction(ActionEvent event) {
        vBoxDelivery.setDisable(false);
    }

    //======================== END OF FXML DECLARATIONS ===========================================
    private PizzaData getPizzaData(String pizzaListItem) { //we get index of selected pizza from combo and that must match pizzaList index  
        PizzaData pizzaData = null;
        for (int i = 0; i < comboPizza.getItems().size(); i++) {
            if (comboPizza.getItems().get(i).equals(pizzaListItem)) {
                pizzaData = pizzaList.get(i);
                break;
            }
        }
        return pizzaData;
    }

    private PizzaSizeData getPizzaSizeData(Integer pizzaSizeListItem) { //we get index of selected pizza from combo and that must match pizzaList index  
        PizzaSizeData pizzaSizeData = null;
        for (int i = 0; i < comboPizzaSize.getItems().size(); i++) {
            if (comboPizzaSize.getItems().get(i).equals(pizzaSizeListItem)) {
                pizzaSizeData = pizzaSizeList.get(i);
                break;
            }
        }
        return pizzaSizeData;
    }

    private void insertSaleInTable(int ID, Timestamp timeStamp, String user, int pizza_id, int pizza_size, boolean delivery, double income) {
        SaleData sale = new SaleData();
        sale.setiD(ID);
        sale.setOrderDate(timeStamp);
        sale.setOrderStatus(ORDER_STATUS.Waiting);
        sale.setUser(user);
        sale.setPizzaNumber(pizza_id);
        sale.setPizzaSize(pizza_size);
        if (delivery == true) {
            sale.setDelivery(EXISTS.YES);
        }
        sale.setIncome(income);
        salesTable.addToTable(sale);
    }

    private int insertSale(Timestamp timeStamp, String user, int pizza_id, int pizza_size, boolean delivery, double income) {
        int thisSaleID = 0;
        String salesStatement = "INSERT INTO sales VALUES(Null, ?, 'Waiting', ?, ?, ?, ?);";
        try (Connection connection = Main.dataSource.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(salesStatement)) {
                preparedStatement.setTimestamp(1, timeStamp);
                preparedStatement.setString(2, user);
                preparedStatement.setInt(3, pizza_id);
                preparedStatement.setInt(4, pizza_size);
                preparedStatement.setDouble(5, income);
                preparedStatement.executeUpdate();

                try (PreparedStatement preparedStatement2 = connection.prepareStatement("SELECT LAST_INSERT_ID();")) {
                    try (ResultSet resultSet = preparedStatement2.executeQuery()) {
                        resultSet.next();
                        thisSaleID = resultSet.getInt(1);
                        insertSaleInTable(thisSaleID, timeStamp, user, pizza_id, pizza_size, delivery, income);
                    }
                }
            }
        } catch (SQLException e) {
            ExceptionDialog exceptionDialog = new ExceptionDialog("Couldn't connect to database or execute update", e);
            exceptionDialog.showAndWait();
        }

        return thisSaleID;
    }

    private void insertDelivery(int saleID, String adress, String clientName, String telephone, double cost) {
        String deliveryStatement = "INSERT INTO deliveries VALUES(?, ?, ?, ?, ?);";
        try (Connection connection = Main.dataSource.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(deliveryStatement)) {
                preparedStatement.setInt(1, saleID);
                preparedStatement.setString(2, adress);
                preparedStatement.setString(3, clientName);
                preparedStatement.setString(4, telephone);
                preparedStatement.setDouble(5, cost);
                preparedStatement.executeUpdate();
            }
        } catch (SQLException e) {
            ExceptionDialog exceptionDialog = new ExceptionDialog("Couldn't connect to database or execute update", e);
            exceptionDialog.showAndWait();
        }
    }

    private void loadPizzaList() {
        ObservableList<String> list = FXCollections.observableArrayList();

        PizzasTable pizzasTable = new PizzasTable();
        pizzasTable.loadPizzasTable();
        for (PizzaData pizzaData : pizzasTable.getPizzasList()) {
            if (pizzaData.getAvailable() == AVAILABLE.Yes) {
                list.add(pizzaData.getID() + ". " + pizzaData.getName());
                pizzaList.add(pizzaData);
            }
        }
        comboPizza.setItems(list);
    }

    private void loadPizzaSizeList() {
        ObservableList<Integer> list = FXCollections.observableArrayList();

        PizzaSizeTable pizzaSizeTable = new PizzaSizeTable();
        pizzaSizeTable.loadPizzaSizeTable();
        pizzaSizeList = pizzaSizeTable.getPizzaSizeList();
        for (PizzaSizeData pizzaSizeData : pizzaSizeTable.getPizzaSizeList()) {
            list.add(pizzaSizeData.getSize());
        }
        comboPizzaSize.setItems(list);
    }

    public void setUserAndTable(UserData userData, SalesTable table) {
        this.currentUser = userData;
        this.salesTable = table;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        currentUser = new UserData();

        pizzaList = FXCollections.observableArrayList();
        loadPizzaList();
        loadPizzaSizeList();
    }

    @Override
    public Pane getMainPane() {
        return borderPane;
    }
}
