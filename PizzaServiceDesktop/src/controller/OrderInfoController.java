package controller;

import classes.Controller;
import classes.ExceptionDialog;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import model.DeliveryData;
import model.Main;
import model.PizzaData;
import model.SaleData;

public class OrderInfoController extends Controller implements Initializable {

    private SaleData sale;
    private DeliveryData delivery;
    private PizzaData pizza;

    @FXML
    private BorderPane borderPane;
    @FXML
    private VBox vBoxDelivery;
    @FXML
    private Label labID, labOrderDate, labOrderStatus, labUser, labPizza, labPizzaSize, labPizzaCost, labDelivery,
            labClientName, labAddress, labTelephone, labDeliveryCost;

    private void startFilling() {
        labID.setText(sale.getID().toString());
        labOrderDate.setText(sale.getOrderDate().toString());
        labOrderStatus.setText(sale.getOrderStatus().toString());
        labUser.setText(sale.getUser());
        labPizza.setText(pizza.getID() + ". " + pizza.getName());
        labPizzaSize.setText(sale.getPizzaSize().toString());
        labPizzaCost.setText(sale.getIncome().toString());

        if (sale.getDelivery() != null) {
            labDelivery.setText(sale.getDelivery().toString());
            labClientName.setText(delivery.getClientName());
            labAddress.setText(delivery.getAdress());
            labTelephone.setText(delivery.getTelephone());
            labDeliveryCost.setText(delivery.getCost().toString());
        } else {
            vBoxDelivery.setDisable(true);
        }
    }

    private void loadDelivery(Integer id) {
        String query = "SELECT d.*, s.order_date FROM deliveries AS d, sales AS s WHERE (d.order_id = s.id)AND(d.order_id=?);";
        try (Connection connection = Main.dataSource.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                preparedStatement.setInt(1, id);
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    if (resultSet.next() == true) {
                        DeliveryData deliveryData = new DeliveryData();
                        deliveryData.setOrderID(resultSet.getInt(1));
                        deliveryData.setOrderDate(resultSet.getTimestamp(6));
                        deliveryData.setAdress(resultSet.getString(2));
                        deliveryData.setClientName(resultSet.getString(3));
                        deliveryData.setTelephone(resultSet.getString(4));
                        deliveryData.setCost(resultSet.getDouble(5));
                        this.delivery = deliveryData;
                    } else {
                        Alert alert = new Alert(AlertType.INFORMATION);
                        alert.setTitle("Information");
                        alert.setHeaderText(null);
                        alert.setContentText("Couldn't load delivery data");
                        alert.showAndWait();
                    }
                }
            }
        } catch (Exception exception) {
            ExceptionDialog exceptionDialog = new ExceptionDialog("Error while loading delivery data from database", exception);
            exceptionDialog.showAndWait();
        }
    }

    private void loadPizza(int id) {
        //---Loading data
        String query = "SELECT * FROM pizzas WHERE id = ?;";
        try (Connection connection = Main.dataSource.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                preparedStatement.setInt(1, id);
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    //--- working with result
                    if (resultSet.next()) {
                        PizzaData pizzaData = new PizzaData();
                        pizzaData.setID(resultSet.getInt(1));
                        pizzaData.setName(resultSet.getString(2));
                        pizzaData.setComposition(resultSet.getString(3));
                        pizzaData.setPrice(resultSet.getDouble(4));

                        pizza = pizzaData;
                    } else { //its not such a big problem so we just print it to console
                        System.err.println("Couldn't load pizza data");
                    }
                }
            }
        } catch (Exception exception) {
            System.err.println("Error while loading pizza data from database");
        };
    }

    public void fillWithData(SaleData saleData) {
        sale = saleData;
        loadPizza(sale.getPizzaNumber());
        if (saleData.getDelivery() != null) //if not null then its YES
        {
            loadDelivery(sale.getID());
        }

        startFilling();
    }

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        delivery = new DeliveryData();
        pizza = new PizzaData();
    }

    @Override
    public Pane getMainPane() {
        return borderPane;
    }
}
