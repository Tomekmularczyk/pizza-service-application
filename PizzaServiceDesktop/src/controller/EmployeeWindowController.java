package controller;

import classes.ChangeRoot;
import classes.Controller;
import classes.ExceptionDialog;
import classes.INFO;
import classes.ModalWindow;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.RadioButton;
import javafx.scene.control.RadioMenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.util.Duration;
import model.CONSTANTS;
import model.Main;
import model.SaleData.ORDER_STATUS;
import model.SalesTable;
import model.UserData;
import org.controlsfx.control.PopOver;

public class EmployeeWindowController extends Controller implements Initializable {

    private UserData currentUser;
    private SalesTable salesTable;
    private ScheduledExecutorService executorRefresh;
    private ModalWindow ordersWindow;
    private ObservableList<ModalWindow> infoWindowList = FXCollections.observableArrayList(); //we want list of opened windows
    private PopOver popOverInfo;

    @FXML
    private BorderPane borderPane, borderPaneInside;
    @FXML
    private MenuBar menuBar;
    @FXML
    private VBox vBoxDelivery;
    @FXML
    private ToggleGroup groupDelivery;
    @FXML
    private RadioButton radioNo, radioYes;
    @FXML
    private ComboBox<String> comboPizza;
    @FXML
    private ComboBox<Integer> comboPizzaSize;
    @FXML
    private TextField textFAdress, textFClientName, textFCost, textFTelephone;

    @FXML
    private void menuItemNewOrderAction(ActionEvent event) {
        ordersWindow.show();
    }

    @FXML
    private void radioMenuTodayAction(ActionEvent event) {
        stopRefreshingTask();
        salesTable.loadLast24hData();
        startRefreshing();
    }

    @FXML
    private void radioMenuLastWeekAction(ActionEvent event) {
        stopRefreshingTask();
        salesTable.loadLastWeekData();
        startRefreshing();
    }

    @FXML
    private void radioMenuLastMonthAction(ActionEvent event) {
        stopRefreshingTask();
        salesTable.loadLastMonthData();
        startRefreshing();
    }

    @FXML
    private void radioMenuPendingSalesAction(ActionEvent event) {
        stopRefreshingTask();
        salesTable.loadPendingSalesData();
        startRefreshing();
    }

    @FXML
    private void menuItemLogoutAction(ActionEvent e) {
        ordersWindow.close();
        popOverInfo.hide();
        stopRefreshingTask();
        closeInfoWindows();
        ChangeRoot changeRoot = new ChangeRoot(Main.mainStage, CONSTANTS.ROOT_MAIN_WINDOW.string, "Pizza Service", true);
        changeRoot.doFadingTransition(borderPane, CONSTANTS.FADE_OUT.value, CONSTANTS.FADE_IN.value);
    }

    @FXML
    private void menuItemCloseAction(ActionEvent e) {
        Platform.exit();
    }

    @FXML
    private void menuItemAboutAction(ActionEvent e) {
        popOverInfo.show(menuBar);
    }

    //======================== END OF FXML DECLARATIONS ===========================================
    public void setUser(UserData userData) {
        this.currentUser = userData;
        prepareOrdersWindow(); //because just now we know the user
    }

    private void startRefreshing() {
        Runnable task = new Runnable() {
            @Override
            public void run() {
                System.out.println("refreshing");
                salesTable.refreshData();
            }
        };
        executorRefresh = Executors.newSingleThreadScheduledExecutor();
        executorRefresh.scheduleAtFixedRate(task, 0, 5, TimeUnit.SECONDS);
    }

    private void stopRefreshingTask() {
        if (!executorRefresh.isShutdown()) {
            executorRefresh.shutdownNow();
        }
    }

    private void closeInfoWindows() {
        for (ModalWindow window : infoWindowList) {
            window.close();
        }
    }

    private void prepareSalesTable() {
        initSalesTable();

        MenuItem menuItemShow = new MenuItem("Show");
        MenuItem menuItemNewOrder = new MenuItem("New order");
        MenuItem menuChangeStatus = new MenuItem("Change status");
        menuChangeStatus.setId("menuIt");
        SeparatorMenuItem separator = new SeparatorMenuItem();
        RadioMenuItem menuAccepted = new RadioMenuItem("Accepted");
        RadioMenuItem menuProduction = new RadioMenuItem("Production");
        RadioMenuItem menuTransport = new RadioMenuItem("Transport");
        RadioMenuItem menuFinalised = new RadioMenuItem("Finalised");
        RadioMenuItem menuCanceled = new RadioMenuItem("Canceled");

        ToggleGroup statusToggle = new ToggleGroup();
        statusToggle.getToggles().addAll(menuAccepted, menuProduction,
                menuTransport, menuCanceled, menuFinalised);

        menuItemNewOrder.setOnAction(event -> {
            ordersWindow.show();
        });

        menuItemShow.setOnAction(event -> {
            ModalWindow window = salesTable.showSaleInfo(false);
            if (window != null) {
                infoWindowList.add(window);
            }
        });

        menuCanceled.setOnAction(event -> {
            salesTable.changeOrderStatus(ORDER_STATUS.Canceled);
        });

        menuTransport.setOnAction(event -> {
            salesTable.changeOrderStatus(ORDER_STATUS.Transport);
        });

        menuAccepted.setOnAction(event -> {
            salesTable.changeOrderStatus(ORDER_STATUS.Accepted);
        });

        menuProduction.setOnAction(event -> {
            salesTable.changeOrderStatus(ORDER_STATUS.Production);
        });

        menuFinalised.setOnAction(event -> {
            salesTable.changeOrderStatus(ORDER_STATUS.Finalised);
        });

        ContextMenu contextMenu = new ContextMenu(menuItemNewOrder, menuItemShow, separator, menuChangeStatus, menuAccepted,
                menuProduction, menuTransport, menuCanceled, menuFinalised);

        salesTable.setContextMenu(contextMenu);
        salesTable.setOnContextMenuRequested(event -> {
            int indexOfSale = salesTable.getSelectionModel().getSelectedIndex();
            if (indexOfSale >= 0) { // we check if something is selected
                ORDER_STATUS saleStatus = salesTable.getSelectionModel().getSelectedItem().getOrderStatus();
                switch (saleStatus) {
                    case Waiting:
                        // do nothing
                        break;
                    case Accepted:
                        menuAccepted.setSelected(true);
                        break;
                    case Production:
                        menuProduction.setSelected(true);
                        break;
                    case Transport:
                        menuTransport.setSelected(true);
                        break;
                    case Finalised:
                        menuFinalised.setSelected(true);
                        break;
                    case Canceled:
                        menuCanceled.setSelected(true);
                        break;
                    default:
                        break;
                }
            }
        });
        startRefreshing();
    }

    private void initSalesTable() {
        salesTable = new SalesTable();
        salesTable.loadPendingSalesData();
        salesTable.setOnMouseClicked(event -> {
            if (event.getClickCount() > 1) {
                ModalWindow window = salesTable.showSaleInfo(false);
                if (window != null) {
                    infoWindowList.add(window);
                }
            }
        });
    }

    private void prepareOrdersWindow() {
        String title = currentUser.getName() + " " + currentUser.getSurname() + "(" + currentUser.getLogin() + ")";
        ordersWindow = new ModalWindow(Main.mainStage, new Pane(), title);
        ordersWindow.setResizable(false);

        FXMLLoader loader = new FXMLLoader(getClass().getResource(CONSTANTS.ROOT_SALE_WINDOW.string));
        BorderPane pane;
        try {
            pane = loader.load();
            ordersWindow.getScene().setRoot(pane);
            SaleWindowController controller = loader.getController();
            controller.setUserAndTable(currentUser, salesTable);
        } catch (IOException exception) {
            ExceptionDialog exceptionDialog = new ExceptionDialog("Couldn't load sale fxml file", exception);
            exceptionDialog.showAndWait();
            exception.printStackTrace();
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        currentUser = new UserData();
        prepareSalesTable();
        borderPaneInside.setCenter(salesTable);

        TextArea textArea = new TextArea(INFO.EMPLOYEE_INFO.text);
        textArea.setPrefWidth(350);
        popOverInfo = new PopOver(textArea);
        popOverInfo.setDetachedTitle("Pizza service info");

        Main.mainStage.setOnCloseRequest(event -> {
            popOverInfo.hide(new Duration(0));
            stopRefreshingTask();
        });
    }

    @Override
    public Pane getMainPane() {
        return borderPane;
    }
}
