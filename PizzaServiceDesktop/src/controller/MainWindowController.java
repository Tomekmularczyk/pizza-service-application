package controller;

import classes.ChangeRoot;
import classes.Controller;
import classes.ExceptionDialog;
import classes.INFO;
import classes.LoginDialog;
import classes.ModalWindow;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ResourceBundle;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.util.Duration;
import model.CONSTANTS;
import model.Main;
import model.UserData;
import model.UserPreferences;
import org.controlsfx.control.PopOver;

public class MainWindowController extends Controller implements Initializable {

    private ModalWindow preferencesWindow;
    private PreferencesController preferencesWindowController;
    private PopOver popOverInfo;

    @FXML
    private BorderPane borderPane;
    @FXML
    private VBox vBox; //buttons are here
    @FXML
    private MenuItem menuItemTestConnection, menuItemPreferences;
    @FXML
    private Label labStatus;
    @FXML
    private MenuBar menuBar;

    @FXML
    private void menuItemTestConnectionAction(ActionEvent e) { //--- testing connection to database
        hideLoginButtons();
        initDataSource();

        try (Connection connection = Main.dataSource.getConnection()) {
            labStatus.setText("Connection to " + UserPreferences.getDatabaseName() + " succeed");
            labStatus.setStyle("-fx-text-fill: green");

            menuItemPreferences.setDisable(true); //we get connection so we disable preferences
            preferencesWindow.close();			  //
            showLoginButtons();
        } catch (Exception ex) {
            labStatus.setText("Connection to " + UserPreferences.getDatabaseName() + " failed");
            labStatus.setStyle("-fx-text-fill: red");

            ExceptionDialog exceptionDialog = new ExceptionDialog("Couldn't reach the connection", ex);
            exceptionDialog.show();
        }
    }

    private void initDataSource() {
        String databaseName = UserPreferences.getDatabaseName();
        Main.dataSource.setServerName(UserPreferences.getServerName());
        Main.dataSource.setDatabaseName(databaseName);
        Main.dataSource.setUser(UserPreferences.getUserName());
        Main.dataSource.setPassword(UserPreferences.getUserPassword());
    }

    @FXML
    private void menuItemPreferencesAction(ActionEvent e) {
        preferencesWindow.show();
    }

    @FXML
    private void menuItemAboutAction(ActionEvent e) {
        popOverInfo.show(menuBar);
    }

    @FXML
    private void menuItemCloseAction(ActionEvent e) {
        Platform.exit();
    }

    @FXML
    private void buttEmployeeAction(ActionEvent e) {
        LoginDialog loginDialog = new LoginDialog("Employee login");
        loginDialog.setTooltips("Case insensitive(use \"zenon\")", "Case sensitive(use \"zenon\")");
        loginDialog.setOnLogin(actionevent -> {
            String user = loginDialog.getUserName().toUpperCase();
            UserData userData = searchForEmployee(user);
            if (userData != null) {
                if (userData.getPassword().equals(loginDialog.getUserPassword())) {
                    popOverInfo.hide();
                    String title = userData.getName() + " " + userData.getSurname() + "(" + userData.getLogin() + ")";
                    ChangeRoot changeRoot = new ChangeRoot(Main.mainStage, CONSTANTS.ROOT_EMPLOYEE_WINDOW.string, title, true);
                    EmployeeWindowController controller = (EmployeeWindowController) changeRoot.getController();
                    controller.setUser(userData);
                    changeRoot.doFadingTransition(borderPane, CONSTANTS.FADE_OUT.value, CONSTANTS.FADE_IN.value);
                }
            }
        });
        loginDialog.showLoginDialog();
    }

    @FXML
    private void buttAdministratorAction(ActionEvent e) {
        LoginDialog loginDialog = new LoginDialog("Administrator login");
        loginDialog.setTooltips("Case insensitive(use \"admin\")", "Case sensitive(use \"admin\")");
        loginDialog.setOnLogin(actionevent -> {
            String user = loginDialog.getUserName().toUpperCase();
            String password = loginDialog.getUserPassword();
            if (user.equals(CONSTANTS.ADMINISTRATOR_NAME.string.toUpperCase()) && password.equals(CONSTANTS.ADMINISTRATOR_PASSWORD.string)) {
                popOverInfo.hide();
                ChangeRoot changeRoot = new ChangeRoot(Main.mainStage, CONSTANTS.ROOT_ADMINISTRATOR_WINDOW.string, "Administrator panel", true);
                changeRoot.doFadingTransition(borderPane, CONSTANTS.FADE_OUT.value, CONSTANTS.FADE_IN.value);
            }
        });
        loginDialog.showLoginDialog();
    }

    //=================== END OF FXML DECLARATIONS ================================================
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initializePreferencesWindow();

        TextArea textArea = new TextArea(INFO.GENERAL_INFO.text);
        textArea.setPrefWidth(350);
        popOverInfo = new PopOver(textArea);
        popOverInfo.setDetachedTitle("Pizza service info");
        Main.mainStage.setOnCloseRequest(e -> {
            popOverInfo.hide(new Duration(0));
        });
    }

    @Override
    public Pane getMainPane() {
        return borderPane;
    }

    private UserData searchForEmployee(String user) { //returns employee if exists
        UserData userData = null;
        String query = "SELECT * FROM users WHERE login = " + "'" + user + "';";
        try (Connection connection = Main.dataSource.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    //--- working with result
                    if (resultSet.next()) {
                        userData = new UserData();
                        userData.setiD(resultSet.getInt(3));
                        userData.setLogin(resultSet.getString(1));
                        userData.setPassword(resultSet.getString(2));
                        //we need rest of data for userData
                        String query2 = "SELECT name, surname, birthday FROM employees WHERE id = ?;";
                        try (PreparedStatement preparedStatement2 = connection.prepareStatement(query2)) {
                            preparedStatement2.setInt(1, userData.getiD());
                            try (ResultSet resultSet2 = preparedStatement2.executeQuery()) {
                                resultSet2.next();
                                userData.setName(resultSet2.getString(1));
                                userData.setSurname(resultSet2.getString(2));
                                userData.setBirthday(resultSet2.getDate(3).toLocalDate());
                            }
                        }
                    }
                }
            }
        } catch (Exception exception) {
            ExceptionDialog exceptionDialog = new ExceptionDialog("Couldn't get connection to database", exception);
            exceptionDialog.showAndWait();
        }

        return userData;
    }

    private void initializePreferencesWindow() {
        preferencesWindow = new ModalWindow(Main.mainStage, new Pane(), "Preferences");
        preferencesWindow.setResizable(false);

        FXMLLoader loader = new FXMLLoader(getClass().getResource(CONSTANTS.ROOT_PREFERENCES.string));
        BorderPane preferencesPane;
        try {
            preferencesPane = loader.load();
            preferencesWindow.getScene().setRoot(preferencesPane);
            preferencesWindowController = loader.getController();
            preferencesLoad();
            preferencesWindowController.setOnDefault(actionevent -> {
                preferencesLoadDefault();
            });
            preferencesWindowController.setOnSave(actionevent -> {
                preferencesSave();
            });
        } catch (IOException exception) {
            ExceptionDialog exceptionDialog = new ExceptionDialog("Couldn't load preferences fxml file", exception);
            exceptionDialog.showAndWait();
        }
    }

    private void preferencesLoad() {
        preferencesWindowController.setServerName(UserPreferences.getServerName());
        preferencesWindowController.setDatabaseName(UserPreferences.getDatabaseName());
        preferencesWindowController.setUserName(UserPreferences.getUserName());
        preferencesWindowController.setUserPassword(UserPreferences.getUserPassword());
    }

    private void preferencesSave() {
        UserPreferences.saveServerName(preferencesWindowController.getServerName());
        UserPreferences.saveDatabaseName(preferencesWindowController.getDatabaseName());
        UserPreferences.saveUserName(preferencesWindowController.getUserName());
        UserPreferences.saveUserPassword(preferencesWindowController.getUserPassword());

        //---signal to user that changes has been affected
        preferencesWindowController.getGridPane().setStyle("-fx-background-color: lightgreen");
        ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();
        Runnable task = () -> {
            Platform.runLater(() -> {
                preferencesWindowController.getGridPane().setStyle("-fx-background-color: lightgrey");
            });
        };
        service.schedule(task, 700, TimeUnit.MILLISECONDS);
        service.shutdown();
    }

    private void preferencesLoadDefault() {
        preferencesWindowController.setServerName(UserPreferences.getDefaultServerName());
        preferencesWindowController.setDatabaseName(UserPreferences.getDefaultDatabaseName());
        preferencesWindowController.setUserName(UserPreferences.getDefaultUserName());
        preferencesWindowController.setUserPassword(UserPreferences.getDefaultUserPassword());
        preferencesSave();
    }

    private void showLoginButtons() {
        vBox.setDisable(false);
        Timeline fadeIn = new Timeline(
                new KeyFrame(Duration.ZERO, new KeyValue(vBox.opacityProperty(), 0.0)),
                new KeyFrame(new Duration(1500), new KeyValue(vBox.opacityProperty(), 1.0)
                ));

        fadeIn.play();
    }

    private void hideLoginButtons() {
        vBox.setDisable(true);
        vBox.setOpacity(0.0);
    }
}
