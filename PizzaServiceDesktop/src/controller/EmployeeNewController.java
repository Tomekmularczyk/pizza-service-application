package controller;

import classes.Controller;
import classes.ExceptionDialog;
import classes.InfoTip;
import classes.ModalWindow;
import java.net.URL;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDate;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import model.EmployeeData;
import model.EmployeeData.EXISTS;
import model.EmployeesTable;
import model.Main;

public class EmployeeNewController extends Controller implements Initializable {

    private EmployeesTable employeesTable;
    private int lastEmployeeID = -1;

    @FXML
    private BorderPane borderPane;
    @FXML
    private Button buttCancel;
    @FXML
    private TextField textFName, textFSurname, textFAdress, textFTelephone, textFLogin, textFPassword;
    @FXML
    private DatePicker datePBirthday;
    @FXML
    private CheckBox checkAddAccount;
    @FXML
    private VBox vBoxAccountPanel;

    @FXML
    private void buttCancelAction(ActionEvent event) {
        ModalWindow owner = (ModalWindow) buttCancel.getScene().getWindow();
        owner.close();
    }

    @FXML
    private void buttSaveAction(ActionEvent event) {
        String name = textFName.getText();
        String surname = textFSurname.getText();
        String adress = textFAdress.getText();
        String telephone = textFTelephone.getText();

        if (validateData(name, surname, adress, telephone)) {
            InfoTip infoTip = new InfoTip();

            if (checkAddAccount.isSelected()) {
                String login = textFLogin.getText();
                String password = textFPassword.getText();
                if (validateLoginAndPassword(login, password)) {
                    addEmployee(name, surname, datePBirthday.getValue(), adress, telephone, EXISTS.YES);
                    addUser(login, password, lastEmployeeID);// after addEmployee `lastEmployeeID` is set
                    infoTip.getInfoTip().setId("greenInfoTip");
                    infoTip.showTip((Button) event.getSource(), "   Saved   ");
                }
            } else {
                addEmployee(name, surname, datePBirthday.getValue(), adress, telephone, EXISTS.NO);
                infoTip.getInfoTip().setId("greenInfoTip");
                infoTip.showTip((Button) event.getSource(), "   Saved   ");
            }
        }
    }

    private boolean validateData(String name, String surname, String adsress, String telephone) {
        boolean isNameOK = name.length() < 21 && !name.isEmpty();
        boolean isSurnameOK = surname.length() < 26 && !surname.isEmpty();
        boolean isBirthdayOK = datePBirthday.getValue() != null;
        boolean isAdressOK = adsress.length() < 51 && !adsress.isEmpty();
        boolean isTelephoneOK = telephone.length() < 26 && !telephone.isEmpty();

        InfoTip infoTip = new InfoTip();

        if (!isNameOK) {
            infoTip.showTip(textFName, "max 20 characters, not empty");
            return false;
        }

        if (!isSurnameOK) {
            infoTip.showTip(textFSurname, "max 25 characters, not empty");
            return false;
        }

        if (!isBirthdayOK) {
            infoTip.showTip(datePBirthday, "choose date");
            return false;
        }

        if (!isAdressOK) {
            infoTip.showTip(textFAdress, "max 50 characters, not empty");
            return false;
        }

        if (!isTelephoneOK) {
            infoTip.showTip(textFTelephone, "max 25 characters, not empty");
            return false;
        }

        return true;
    }

    private boolean validateLoginAndPassword(String login, String password) {
        Pattern pattern = Pattern.compile("\\s");
        Matcher matcherLogin = pattern.matcher(login);
        Matcher matcherPassword = pattern.matcher(password);
        boolean isLoginOK = login.length() < 17 && !login.isEmpty() && !matcherLogin.find();
        boolean isPasswordOK = password.length() < 17 && !password.isEmpty() && !matcherPassword.find();

        InfoTip infoTip = new InfoTip();
        if (!isLoginOK) {
            infoTip.showTip(textFLogin, "max 16 characters, no spaces, not empty");
            return false;
        }

        if (isLoginReserved(login)) {
            infoTip.showTip(textFLogin, "This login already exists!");
            return false;
        }

        if (!isPasswordOK) {
            infoTip.showTip(textFPassword, "max 16 characters, no spaces, not empty");
            return false;
        }

        return true;
    }

    //=============================== END OF FXML DECLARATIONS =================================================================
    private void addEmployee(String name, String surname, LocalDate birthday, String adress, String telephone, EXISTS account) {
        String query = "INSERT INTO employees VALUES(null, ?, ?, ?, ?, ?);";
        try (Connection connection = Main.dataSource.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {;
                preparedStatement.setString(1, name);
                preparedStatement.setString(2, surname);
                preparedStatement.setDate(3, Date.valueOf(birthday));
                preparedStatement.setString(4, adress);
                preparedStatement.setString(5, telephone);
                preparedStatement.executeUpdate();

                try (ResultSet result = preparedStatement.executeQuery("SELECT LAST_INSERT_ID();")) {
                    result.next();
                    EmployeeData newEmployee = new EmployeeData();
                    lastEmployeeID = result.getInt(1);
                    newEmployee.setiD(lastEmployeeID);
                    newEmployee.setName(name);
                    newEmployee.setSurname(surname);
                    newEmployee.setBirthday(datePBirthday.getValue());
                    newEmployee.setAdress(adress);
                    newEmployee.setTelephone(telephone);
                    if (account == EXISTS.YES) {
                        newEmployee.setAccount(account);
                    }
                    employeesTable.getEmployeesList().add(newEmployee);
                }
            }
        } catch (Exception exception) {
            ExceptionDialog exceptionDialog = new ExceptionDialog("Error while saving data to table employees", exception);
            exceptionDialog.showAndWait();
        };
    }

    private void addUser(String login, String password, int employee_id) {
        String query = "INSERT INTO users VALUES(?, ?, ?);";
        try (Connection connection = Main.dataSource.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {;
                preparedStatement.setString(1, login);
                preparedStatement.setString(2, password);
                preparedStatement.setInt(3, employee_id);
                preparedStatement.executeUpdate();
            }
        } catch (Exception exception) {
            ExceptionDialog exceptionDialog = new ExceptionDialog("Error while saving data to table users", exception);
            exceptionDialog.showAndWait();
        };
    }

    private boolean isLoginReserved(String login) {
        boolean exists = false;
        String query = "SELECT * FROM users WHERE login = ?;";
        try (Connection connection = Main.dataSource.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                preparedStatement.setString(1, login);
                try (ResultSet result = preparedStatement.executeQuery()) {
                    if (result.next()) {
                        exists = true;
                    }
                }
            }
        } catch (Exception exception) {
            ExceptionDialog exceptionDialog = new ExceptionDialog("Couldn't check if login exists in table Users", exception);
            exceptionDialog.showAndWait();
        };

        return exists;
    }

    public void setEmployeesTable(EmployeesTable table) {
        employeesTable = table;
    }

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        vBoxAccountPanel.disableProperty().bind(checkAddAccount.selectedProperty().not());
    }

    @Override
    public Pane getMainPane() {
        return borderPane;
    }

}
