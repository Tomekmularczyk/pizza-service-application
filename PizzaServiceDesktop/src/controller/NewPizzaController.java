package controller;

import classes.Controller;
import classes.ExceptionDialog;
import classes.InfoTip;
import classes.ModalWindow;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import model.Main;
import model.PizzaData;
import model.PizzaData.AVAILABLE;
import model.PizzasTable;

public class NewPizzaController extends Controller implements Initializable {

    private PizzasTable currentPizzasTable;

    @FXML
    private BorderPane borderPane;
    @FXML
    private Button buttCancel;
    @FXML
    private TextField textFName, textFComposition, textFPrice;

    @FXML
    private void buttSaveAction(ActionEvent event) {
        String name = textFName.getText().trim();
        String composition = textFComposition.getText().trim();

        InfoTip infoTip = new InfoTip();
        if (validatePizza(name, composition)) {
            try {
                Double price = Double.valueOf(textFPrice.getText().trim());
                insertValues(name, composition, price);
                infoTip.getInfoTip().setId("greenInfoTip");
                infoTip.showTip((Button) event.getSource(), "   Saved   ");
            } catch (NumberFormatException ex) { //price not ok
                infoTip.showTip(textFPrice, "empty or wrong format(e.x 10.55)");
            }
        }
    }

    private boolean validatePizza(String name, String composition) {
        boolean isNameOK = name.length() < 41 && !name.isEmpty();
        boolean isCompositionOK = composition.length() < 151 && !composition.isEmpty();
        for (PizzaData pizza : currentPizzasTable.getPizzasList()) {
            if (name.equalsIgnoreCase(pizza.getName())) {
                isNameOK = false;
            }
        }

        InfoTip infoTip = new InfoTip();
        if (!isNameOK) {
            infoTip.showTip(textFName, "max 40 characters, not empty, unique");
            return false;
        }

        if (!isCompositionOK) {
            infoTip.showTip(textFComposition, "max 150 characters, not empty");
            return false;
        }

        return true;
    }

    private void insertValues(String name, String composition, Double price) {
        String query = "INSERT INTO pizzas VALUES(null, ?, ?, ?, 'No');";
        try (Connection connection = Main.dataSource.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {;
                preparedStatement.setString(1, name);
                preparedStatement.setString(2, composition);
                preparedStatement.setDouble(3, price);
                preparedStatement.executeUpdate();

                try (ResultSet result = preparedStatement.executeQuery("SELECT LAST_INSERT_ID();")) {
                    result.next();
                    PizzaData newPizza = new PizzaData();
                    newPizza.setID(result.getInt(1));
                    newPizza.setName(name);
                    newPizza.setComposition(composition);
                    newPizza.setPrice(price);
                    newPizza.setAvailable(AVAILABLE.No);
                    if (currentPizzasTable != null) {
                        currentPizzasTable.getPizzasList().add(newPizza);
                    }
                }
            }
        } catch (Exception exception) {
            ExceptionDialog exceptionDialog = new ExceptionDialog("Error while saving data to table employees", exception);
            exceptionDialog.showAndWait();
        };
    }

    @FXML
    private void buttCancelAction(ActionEvent event) {
        ModalWindow owner = (ModalWindow) buttCancel.getScene().getWindow();
        owner.close();
    }

    public void setPizzasTable(PizzasTable table) {
        currentPizzasTable = table;
    }

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        // TODO Auto-generated method stub
    }

    @Override
    public Pane getMainPane() {
        return borderPane;
    }
}
