package controller;

import classes.Controller;
import classes.ExceptionDialog;
import classes.InfoTip;
import classes.ModalWindow;
import java.net.URL;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import model.EmployeeData;
import model.Main;

public class EmployeeEditController extends Controller implements Initializable {

    private EmployeeData currentEmployee;

    @FXML
    private BorderPane borderPane;
    @FXML
    private Label labID, labAccount;
    @FXML
    private TextField textFName, textFSurname, textFAdress, textFTelephone;
    @FXML
    private DatePicker datePBirthday;

    @FXML
    private void buttCancelAction(ActionEvent event) {
        ModalWindow owner = (ModalWindow) ((Button) event.getSource()).getScene().getWindow();
        owner.close();
    }

    @FXML
    private void buttSaveAction(ActionEvent event) {
        String name = textFName.getText();
        String surname = textFSurname.getText();
        String address = textFAdress.getText();
        String telephone = textFTelephone.getText();

        if (validateData(name, surname, address, telephone)) {
            InfoTip infoTip = new InfoTip();
            String query = "UPDATE employees SET name = ?, surname = ?, birthday = ?, adres = ?, telephone = ? WHERE id = ?;";
            try (Connection connection = Main.dataSource.getConnection()) {
                try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                    preparedStatement.setString(1, name);
                    preparedStatement.setString(2, surname);
                    preparedStatement.setDate(3, Date.valueOf(datePBirthday.getValue()));
                    preparedStatement.setString(4, address);
                    preparedStatement.setString(5, telephone);
                    preparedStatement.setInt(6, currentEmployee.getiD());
                    preparedStatement.executeUpdate();

                    currentEmployee.setName(name);
                    currentEmployee.setSurname(surname);
                    currentEmployee.setBirthday(datePBirthday.getValue());
                    currentEmployee.setAdress(address);
                    currentEmployee.setTelephone(telephone);

                    infoTip.getInfoTip().setId("greenInfoTip");
                    infoTip.showTip((Button) event.getSource(), "   Saved   ");
                }
            } catch (Exception exception) {
                ExceptionDialog exceptionDialog = new ExceptionDialog("Error while loading data from database", exception);
                exceptionDialog.showAndWait();
            };
        }
    }

    private boolean validateData(String name, String surname, String address, String telephone) {
        boolean isNameOK = name.length() < 21 && !name.isEmpty();
        boolean isSurnameOK = surname.length() < 26 && !surname.isEmpty();
        boolean isBirthdayOK = datePBirthday.getValue() != null;
        boolean isAddressOK = address.length() < 51 && !address.isEmpty();
        boolean isTelephoneOK = telephone.length() < 26 && !telephone.isEmpty();

        InfoTip infoTip = new InfoTip();
        if (!isNameOK) {
            infoTip.showTip(textFName, "max 20 characters, not empty");
            return false;
        }

        if (!isSurnameOK) {
            infoTip.showTip(textFSurname, "max 25 characters, not empty");
            return false;
        }

        if (!isBirthdayOK) {
            infoTip.showTip(datePBirthday, "choose date");
            return false;
        }

        if (!isAddressOK) {
            infoTip.showTip(textFAdress, "max 50 characters, not empty");
            return false;
        }

        if (!isTelephoneOK) {
            infoTip.showTip(textFTelephone, "max 25 characters, not empty");
            return false;
        }

        return true;
    }

    //========================= END OF FXML DECLARATIONS ==========================================================
    public void fillWithData(EmployeeData employeeData) {
        currentEmployee = employeeData;

        labID.setText(employeeData.getiD().toString());
        if (employeeData.getAccount() != null) {
            labAccount.setText(employeeData.getAccount().toString());
        } else {
            labAccount.setText("NO");
        }
        textFName.setText(employeeData.getName());
        textFSurname.setText(employeeData.getSurname());
        datePBirthday.setValue(employeeData.getBirthday());
        textFAdress.setText(employeeData.getAdress());
        textFTelephone.setText(employeeData.getTelephone());
    }

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        
    }

    @Override
    public Pane getMainPane() {
        return borderPane;
    }
}
