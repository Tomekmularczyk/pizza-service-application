package controller;

import classes.Controller;
import classes.ExceptionDialog;
import classes.InfoTip;
import classes.ModalWindow;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import model.Main;
import model.PizzaData;

public class PizzaEditController extends Controller implements Initializable {

    private PizzaData currentPizza;

    @FXML
    private BorderPane borderPane;
    @FXML
    private TextField textFName, textFComposition, textFPrice;
    @FXML
    private Label labID;

    @FXML
    private void buttCancelAction(ActionEvent event) {
        ModalWindow owner = (ModalWindow) ((Button) event.getSource()).getScene().getWindow();
        owner.close();
    }

    @FXML
    private void buttSaveAction(ActionEvent event) {
        String name = textFName.getText().trim();
        String composition = textFComposition.getText().trim();

        InfoTip infoTip = new InfoTip();
        if (validatePizza(name, composition)) {
            try {
                Double price = Double.valueOf(textFPrice.getText().trim());
                saveValues(name, composition, price);
                infoTip.getInfoTip().setId("greenInfoTip");
                infoTip.showTip((Button) event.getSource(), "   Saved   ");
            } catch (NumberFormatException ex) { //price not ok
                infoTip.showTip(textFPrice, "empty or wrong format(e.x 10.55)");
            }
        }
    }

    private boolean validatePizza(String name, String composition) {
        boolean isNameOK = name.length() < 41 && !name.isEmpty();
        boolean isCompositionOK = composition.length() < 151 && !composition.isEmpty();

        if (!name.equalsIgnoreCase(currentPizza.getName())) { //new name we need to check if doesnt exist
            if (doesNameExists(name)) {
                isNameOK = false;
            }
        }

        InfoTip infoTip = new InfoTip();
        if (!isNameOK) {
            infoTip.showTip(textFName, "max 40 characters, not empty, unique");
            return false;
        }

        if (!isCompositionOK) {
            infoTip.showTip(textFComposition, "max 150 characters, not empty");
            return false;
        }

        return true;
    }

    private void saveValues(String name, String composition, Double price) {
        currentPizza.setName(name);
        currentPizza.setComposition(composition);
        currentPizza.setPrice(price);
        String query = "UPDATE pizzas SET name=?, composition=?, price=? WHERE id=?";
        try (Connection connection = Main.dataSource.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {;
                preparedStatement.setString(1, name);
                preparedStatement.setString(2, composition);
                preparedStatement.setDouble(3, price);
                preparedStatement.setInt(4, currentPizza.getID());
                preparedStatement.executeUpdate();
            }
        } catch (Exception exception) {
            ExceptionDialog exceptionDialog = new ExceptionDialog("Error while saving data to table employees", exception);
            exceptionDialog.showAndWait();
        };
    }

    private boolean doesNameExists(String name) {
        boolean doesExists = true;

        final String query = "SELECT * FROM pizzas WHERE name = ?";
        try (Connection connection = Main.dataSource.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                preparedStatement.setString(1, name);
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    if (!resultSet.next()) {
                        doesExists = false;
                    }
                }
            }
        } catch (Exception ex) {
            ExceptionDialog exceptionDialog = new ExceptionDialog("Error while connecting to database", ex);
            exceptionDialog.showAndWait();
        }

        return doesExists;
    }

    public void fillWithData(PizzaData pizza) {
        currentPizza = pizza;
        labID.setText(currentPizza.getID().toString());
        textFName.setText(currentPizza.getName());
        textFComposition.setText(currentPizza.getComposition());
        textFPrice.setText(currentPizza.getPrice().toString());
    }

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
    }

    @Override
    public Pane getMainPane() {
        return borderPane;
    }
}
