package controller;

import classes.Controller;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;

public class PreferencesController extends Controller implements Initializable {

    @FXML
    private BorderPane borderPane;
    @FXML
    private GridPane gridPane;
    @FXML
    private TextField textFServer, textFDatabase, textFUser;
    @FXML
    private PasswordField passFPassword;
    @FXML
    private Button buttDefault, buttSave;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    @Override
    public Pane getMainPane() {
        return borderPane;
    }

    public GridPane getGridPane() {
        return gridPane;
    }

    public void setOnSave(EventHandler<ActionEvent> action) {
        buttSave.setOnAction(action);
    }

    public void setOnDefault(EventHandler<ActionEvent> action) {
        buttDefault.setOnAction(action);
    }

    public String getServerName() {
        return textFServer.getText();
    }

    public String getDatabaseName() {
        return textFDatabase.getText();
    }

    public String getUserName() {
        return textFUser.getText();
    }

    public String getUserPassword() {
        return passFPassword.getText();
    }

    public void setServerName(String serverName) {
        textFServer.setText(serverName);
    }

    public void setDatabaseName(String databaseName) {
        textFDatabase.setText(databaseName);
    }

    public void setUserName(String userName) {
        textFUser.setText(userName);
    }

    public void setUserPassword(String userPassword) {
        passFPassword.setText(userPassword);
    }
}
