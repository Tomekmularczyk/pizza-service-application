package controller;

import classes.Controller;
import classes.ExceptionDialog;
import classes.InfoTip;
import classes.ModalWindow;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import model.Main;
import model.PizzaSizeData;
import model.PizzaSizeTable;

public class NewPizzaSizeController extends Controller implements Initializable {

    private PizzaSizeTable currentPizzaSizeTable;
    private Spinner<Integer> spinner;

    @FXML
    private HBox hBoxSize;
    @FXML
    private TextField textFPrice;
    @FXML
    private BorderPane borderPane;

    @FXML
    private void buttSaveAction(ActionEvent event) {
        double price;
        int size;
        InfoTip infoTip = new InfoTip();
        try {
            price = Double.valueOf(textFPrice.getText());
            size = spinner.getValue();
            if (!checkIfSizeExists(size)) {
                PizzaSizeData pizzaSize = new PizzaSizeData();
                pizzaSize.setSize(size);
                pizzaSize.setPrice(price);
                insertPizzaSize(pizzaSize);
                currentPizzaSizeTable.getPizzaSizeList().add(pizzaSize);
                infoTip.getInfoTip().setId("greenInfoTip");
                infoTip.showTip((Button) event.getSource(), "   Saved   ");
            } else {
                infoTip.showTip(spinner, "size value must be unique");
            }
        } catch (NumberFormatException ex) {
            infoTip.showTip(textFPrice, "empty or wrong format(e.x 10.55)");
        }
    }

    @FXML
    private void buttCancelAction(ActionEvent event) {
        ModalWindow owner = (ModalWindow) hBoxSize.getScene().getWindow();
        owner.close();
    }

    private boolean checkIfSizeExists(int size) {
        boolean exists = true;

        String query = "SELECT * FROM pizza_size WHERE size = ?";
        try (Connection connection = Main.dataSource.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                preparedStatement.setInt(1, size);
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    if (!resultSet.next()) {
                        exists = false;
                    }
                }
            }
        } catch (Exception ex) {
            ExceptionDialog exceptionDialog = new ExceptionDialog("Couldn't connect to pizzaSize", ex);
            exceptionDialog.showAndWait();
        }

        return exists;
    }

    private void insertPizzaSize(PizzaSizeData pizzaSize) {
        String query = "INSERT INTO pizza_size VALUES(?,?);";
        try (Connection connection = Main.dataSource.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                preparedStatement.setInt(1, pizzaSize.getSize());
                preparedStatement.setDouble(2, pizzaSize.getPrice());
                preparedStatement.executeUpdate();
            }
        } catch (Exception ex) {
            ExceptionDialog exceptionDialog = new ExceptionDialog("Couldn't insert data to pizzaSize", ex);
            exceptionDialog.showAndWait();
        }
    }

    public void setPizzaSizeTable(PizzaSizeTable table) {
        currentPizzaSizeTable = table;
    }

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        spinner = new Spinner<>(new SpinnerValueFactory.IntegerSpinnerValueFactory(10, 250));
        spinner.setPrefWidth(70);
        hBoxSize.getChildren().add(spinner);
    }

    @Override
    public Pane getMainPane() {
        return borderPane;
    }
}
