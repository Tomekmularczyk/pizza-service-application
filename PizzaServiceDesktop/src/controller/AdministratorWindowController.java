package controller;

import classes.ChangeRoot;
import classes.Controller;
import classes.INFO;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.RadioMenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.util.Duration;
import model.CONSTANTS;
import model.DeliveriesTable;
import model.EmployeeData;
import model.EmployeesTable;
import model.Main;
import model.PizzaData;
import model.PizzaData.AVAILABLE;
import model.PizzaSizeTable;
import model.PizzasTable;
import model.SaleData.ORDER_STATUS;
import model.SalesTable;
import model.TableFunctions;
import model.UserData;
import model.UsersTable;
import org.controlsfx.control.PopOver;

public class AdministratorWindowController extends Controller implements Initializable {

    private PopOver popOverInfo;

    @FXML
    private MenuBar menuBar;
    @FXML
    private BorderPane borderPane;
    @FXML
    private ToggleGroup tableGroup;

    @FXML
    private void menuItemLogoutAction(ActionEvent e) {
        popOverInfo.hide();
        ChangeRoot changeRoot = new ChangeRoot(Main.mainStage, CONSTANTS.ROOT_MAIN_WINDOW.string, "Pizza Service", true);
        changeRoot.doFadingTransition(borderPane, CONSTANTS.FADE_OUT.value, CONSTANTS.FADE_IN.value);
    }

    @FXML
    private void menuItemAboutAction(ActionEvent e) {
        popOverInfo.show(menuBar);
    }

    @FXML
    private void menuItemLoadDefaultAction(ActionEvent event) {
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Confirmation Dialog");
        alert.setHeaderText("Are you sure you want to load default sample data?");
        alert.setContentText("All current data will be deleted");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            TableFunctions.thrashAndLoadDefaultData();
            //we dont know which table is open and which view should be refreshed thats why we will delete whats open
            borderPane.setCenter(null);
            tableGroup.selectToggle(null);
        }
    }

    @FXML
    private void menuItemCloseAction(ActionEvent e) {
        Platform.exit();
    }

    @FXML
    private void menuItemSalesAction(ActionEvent event) {
        SalesTable salesTable = createSalesTable();
        MenuItem menuItemShow = new MenuItem("Show");
        MenuItem menuItemDelete = new MenuItem("Delete");
        MenuItem menuItemSearch = new MenuItem("Search");
        MenuItem menuChangeStatus = new MenuItem("Change status");
        menuChangeStatus.setId("menuIt");
        SeparatorMenuItem separator = new SeparatorMenuItem();
        RadioMenuItem menuAccepted = new RadioMenuItem("Accepted");
        RadioMenuItem menuProduction = new RadioMenuItem("Production");
        RadioMenuItem menuTransport = new RadioMenuItem("Transport");
        RadioMenuItem menuFinalised = new RadioMenuItem("Finalised");
        RadioMenuItem menuCanceled = new RadioMenuItem("Canceled");

        ToggleGroup statusToggle = new ToggleGroup();
        statusToggle.getToggles().addAll(menuAccepted, menuProduction,
                menuTransport, menuCanceled, menuFinalised);

        menuItemShow.setOnAction(evnt -> {
            salesTable.showSaleInfo(true);
        });
        menuItemDelete.setOnAction(evnt -> {
            salesTable.delete();
        });
        menuItemSearch.setOnAction(evnt -> {
            salesTable.salesSearch();
        });
        menuCanceled.setOnAction(evnt -> {
            salesTable.changeOrderStatus(ORDER_STATUS.Canceled);
        });
        menuTransport.setOnAction(evnt -> {
            salesTable.changeOrderStatus(ORDER_STATUS.Transport);
        });
        menuAccepted.setOnAction(evnt -> {
            salesTable.changeOrderStatus(ORDER_STATUS.Accepted);
        });
        menuProduction.setOnAction(evnt -> {
            salesTable.changeOrderStatus(ORDER_STATUS.Production);
        });
        menuFinalised.setOnAction(evnt -> {
            salesTable.changeOrderStatus(ORDER_STATUS.Finalised);
        });
        ContextMenu contextMenu = new ContextMenu(menuItemShow, menuItemDelete, menuItemSearch, separator, menuChangeStatus, menuAccepted,
                menuProduction, menuTransport, menuCanceled, menuFinalised);
        
        salesTable.setContextMenu(contextMenu);
        salesTable.setOnContextMenuRequested(evnt -> {
            int indexOfSale = salesTable.getSelectionModel().getSelectedIndex();
            if (indexOfSale >= 0) { // we check if something is selected
                ORDER_STATUS saleStatus = salesTable.getSelectionModel().getSelectedItem().getOrderStatus();
                switch (saleStatus) {
                    case Waiting:
                        // do nothing
                        break;
                    case Accepted:
                        menuAccepted.setSelected(true);
                        break;
                    case Production:
                        menuProduction.setSelected(true);
                        break;
                    case Transport:
                        menuTransport.setSelected(true);
                        break;
                    case Finalised:
                        menuFinalised.setSelected(true);
                        break;
                    case Canceled:
                        menuCanceled.setSelected(true);
                        break;
                    default:
                        break;
                }
            }
        });
        borderPane.setCenter(salesTable);
    }

    private SalesTable createSalesTable() {
        SalesTable salesTable = new SalesTable();
        salesTable.loadAllSalesData();
        salesTable.setOnMouseClicked(evnt -> {
            if (evnt.getClickCount() > 1) {
                salesTable.showSaleInfo(true);
            }
        });
        return salesTable;
    }

    @FXML
    private void menuItemDeliveriesAction(ActionEvent event) {
        DeliveriesTable deliveriesTable = new DeliveriesTable();
        deliveriesTable.loadDeliveriesTable();
        borderPane.setCenter(deliveriesTable);
        deliveriesTable.setOnMouseClicked(evnt -> {
            if (evnt.getClickCount() > 1) {
                deliveriesTable.showSaleInfo();
            }
        });

        ContextMenu contextMenu = new ContextMenu();
        deliveriesTable.setContextMenu(contextMenu);

        MenuItem menuItemShow = new MenuItem("Show sale");
        MenuItem menuItemSearch = new MenuItem("Search");

        contextMenu.getItems().addAll(menuItemShow, menuItemSearch);
        menuItemShow.setOnAction(evnt -> {
            deliveriesTable.showSaleInfo();
        });
        menuItemSearch.setOnAction(evnt -> {
            deliveriesTable.deliverySearch();
        });
    }

    @FXML
    private void menuItemEmployeesAction(ActionEvent e) {
        EmployeesTable employeesTable = new EmployeesTable();

        if (employeesTable.loadEmployeesTable()) {
            Alert alert = new Alert(AlertType.INFORMATION); //we don't want to edit modal android account
            alert.setTitle("Information");
            alert.setHeaderText(null);
            alert.setContentText("You can't edit or delete \"android\" employee");

            employeesTable.setOnMouseClicked(value -> {
                if (value.getClickCount() > 1) {
                    if (!isAndroidEmployee(employeesTable)) {
                        employeesTable.employeeEdit();
                    } else {
                        alert.showAndWait();
                    }
                }
            });

            MenuItem menuItemAddNew = new MenuItem("Add new");
            MenuItem menuItemSearch = new MenuItem("Search");
            SeparatorMenuItem separator = new SeparatorMenuItem();
            MenuItem menuItemEdit = new MenuItem("Edit");
            MenuItem menuItemEditAccount = new MenuItem("Edit account");
            MenuItem menuItemAddAccount = new MenuItem("Add account");
            MenuItem menuItemDelete = new MenuItem("Delete");

            menuItemDelete.setOnAction(event -> {
                if (isAndroidEmployee(employeesTable)) {
                    alert.showAndWait();
                } else {
                    employeesTable.employeeDelete();
                }
            });

            menuItemEdit.setOnAction(event -> {
                if (isAndroidEmployee(employeesTable)) {
                    alert.showAndWait();
                } else {
                    employeesTable.employeeEdit();
                }
            });

            menuItemAddNew.setOnAction(event -> {
                employeesTable.employeeNew(employeesTable);
            });

            menuItemSearch.setOnAction(event -> {
                employeesTable.employeeSearch();
            });

            menuItemEditAccount.setOnAction(event -> {
                if (isAndroidEmployee(employeesTable)) {
                    alert.showAndWait();
                } else {
                    employeesTable.employeeEditUser(employeesTable);
                }
            });

            menuItemAddAccount.setOnAction(event -> {
                employeesTable.addAccount();
            });

            ContextMenu contextMenu = new ContextMenu(menuItemAddNew, menuItemSearch, separator,
                    menuItemEdit, menuItemDelete, menuItemEditAccount, menuItemAddAccount);

            employeesTable.setContextMenu(contextMenu);
            employeesTable.setOnContextMenuRequested(evnt -> {
                int indexOfItem = employeesTable.getSelectionModel().getSelectedIndex();
                if (indexOfItem >= 0) {
                    EmployeeData employee = (EmployeeData) employeesTable.getSelectionModel().getSelectedItem();
                    if (employee.getAccount() != null) {
                        menuItemAddAccount.setVisible(false);
                        menuItemEditAccount.setVisible(true);
                    } else {
                        menuItemAddAccount.setVisible(true);
                        menuItemEditAccount.setVisible(false);
                    }
                }
            });

            borderPane.setCenter(employeesTable);
        }
    }

    @FXML
    private void menuItemUsersAction(ActionEvent e) {
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle("Information");
        alert.setHeaderText(null);
        alert.setContentText("You can't edit or delete \"android\" user");

        UsersTable usersTable = new UsersTable();
        if (usersTable.loadUserTable()) {
            usersTable.setOnMouseClicked(value -> {
                if (value.getClickCount() > 1) {
                    if (isAndroidUser(usersTable)) {
                        alert.showAndWait();
                    } else {
                        usersTable.userEdit();
                    }
                }
            });

            MenuItem menuItemEdit = new MenuItem("Edit");
            MenuItem menuItemDelete = new MenuItem("Delete");
            menuItemDelete.setDisable(true); // coś problem z foreign key constraint jest
            MenuItem menuItemSearch = new MenuItem("Search");
            ContextMenu contextMenu = new ContextMenu(menuItemEdit, menuItemDelete, menuItemSearch);
            usersTable.setContextMenu(contextMenu);

            menuItemEdit.setOnAction(event -> {
                if (isAndroidUser(usersTable)) {
                    alert.showAndWait();
                } else {
                    usersTable.userEdit();
                }
            });

            menuItemDelete.setOnAction(event -> {
                if (isAndroidUser(usersTable)) {
                    alert.showAndWait();
                } else {
                    usersTable.userDelete();
                }
            });

            menuItemSearch.setOnAction(evnt -> {
                usersTable.userSearch();
            });
        }
        borderPane.setCenter(usersTable);
    }

    @FXML
    private void menuItemPizzaAction(ActionEvent event) {
        PizzasTable pizzasTable = new PizzasTable();
        pizzasTable.loadPizzasTable();
        ContextMenu contextMenu = new ContextMenu();
        pizzasTable.setContextMenu(contextMenu);

        MenuItem menuItemAddNew = new MenuItem("Add new");
        MenuItem menuItemEdit = new MenuItem("Edit");
        MenuItem menuItemSearch = new MenuItem("Search");
        SeparatorMenuItem separator = new SeparatorMenuItem();
        RadioMenuItem menuItemHideInMenu = new RadioMenuItem("Not available in menu");
        RadioMenuItem menuItemShowInMenu = new RadioMenuItem("Available in menu");
        
        menuItemAddNew.setOnAction(evnt -> {
            pizzasTable.addNew();
        });
        
        menuItemEdit.setOnAction(evnt -> {
            pizzasTable.edit();
        });
        
        menuItemShowInMenu.setOnAction(evnt -> {
            pizzasTable.changeStatus(true);
        });
        
        menuItemHideInMenu.setOnAction(evnt -> {
            pizzasTable.changeStatus(false);
        });
        
        menuItemSearch.setOnAction(evnt -> {
            pizzasTable.pizzasSearch();
        });
        
        ToggleGroup availableGroup = new ToggleGroup();
        availableGroup.getToggles().addAll(menuItemHideInMenu, menuItemShowInMenu);

        contextMenu.getItems().addAll(menuItemAddNew, menuItemEdit, menuItemSearch, separator, menuItemShowInMenu, menuItemHideInMenu);
        pizzasTable.setOnContextMenuRequested(evnt -> {
            int indexOfItem = pizzasTable.getSelectionModel().getSelectedIndex();
            if (indexOfItem >= 0) { // we check if something is selected
                PizzaData pizzaData = (PizzaData) pizzasTable.getSelectionModel().getSelectedItem();
                if (pizzaData.getAvailable() == AVAILABLE.Yes) {
                    availableGroup.selectToggle(menuItemShowInMenu);
                } else {
                    availableGroup.selectToggle(menuItemHideInMenu);
                }
            }
        });
        borderPane.setCenter(pizzasTable);
    }

    @FXML
    private void menuItemPizzaSizeAction(ActionEvent event) {
        PizzaSizeTable pizzaSizeTable = new PizzaSizeTable();
        pizzaSizeTable.loadPizzaSizeTable();

        ContextMenu contextMenu = new ContextMenu();
        MenuItem menuItemAddNew = new MenuItem("Add new");
        MenuItem menuItemDelete = new MenuItem("Delete");
        
        menuItemAddNew.setOnAction(eve -> {
            pizzaSizeTable.newSize();
        });
        
        menuItemDelete.setOnAction(eve -> {
            pizzaSizeTable.delete();
        });

        contextMenu.getItems().addAll(menuItemAddNew, menuItemDelete);
        pizzaSizeTable.setContextMenu(contextMenu);
        borderPane.setCenter(pizzaSizeTable);
    }

    //======================== END OF FXML DECLARATIONS =================================================================
    private boolean isAndroidEmployee(EmployeesTable employeesTable) {
        boolean isAndroidEmployee = true;

        EmployeeData employeeData = (EmployeeData) employeesTable.getSelectionModel().getSelectedItem();
        if (employeeData.getiD() > 1) {
            isAndroidEmployee = false;
        }

        return isAndroidEmployee;
    }

    private boolean isAndroidUser(UsersTable usersTable) {
        boolean isAndroidEmployee = true;

        UserData userData = (UserData) usersTable.getSelectionModel().getSelectedItem();
        if (userData.getiD() > 1) {
            isAndroidEmployee = false;
        }

        return isAndroidEmployee;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        TextArea textArea = new TextArea(INFO.ADMIN_INFO.text);
        textArea.setPrefWidth(350);
        popOverInfo = new PopOver(textArea);
        popOverInfo.setDetachedTitle("Pizza service info");
        Main.mainStage.setOnCloseRequest(e -> {
            popOverInfo.hide(new Duration(0));
        });
    }

    @Override
    public Pane getMainPane() {
        return borderPane;
    }
}
