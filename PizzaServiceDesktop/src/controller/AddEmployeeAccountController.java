package controller;

import classes.Controller;
import classes.ExceptionDialog;
import classes.InfoTip;
import classes.ModalWindow;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import model.EmployeeData;
import model.EmployeeData.EXISTS;
import model.Main;

public class AddEmployeeAccountController extends Controller implements Initializable {

    private EmployeeData currentEmployee;

    @FXML
    private BorderPane borderPane;
    @FXML
    private Label labEmployeeID, labEmployeeName;
    @FXML
    private TextField textFLogin, textFPassword;
    @FXML
    private Button buttCancel;

    @FXML
    private void buttCancelAction(ActionEvent event) {
        ModalWindow owner = (ModalWindow) buttCancel.getScene().getWindow();
        owner.close();
    }

    @FXML
    private void buttSaveAction(ActionEvent event) {
        String login = textFLogin.getText().trim();
        String password = textFPassword.getText().trim();

        if (validateLoginAndPassword(login, password)) {
            addUser(login, password, currentEmployee.getiD());// after addEmployee `lastEmployeeID` is set

            InfoTip infoTip = new InfoTip();
            infoTip.getInfoTip().setId("greenInfoTip");
            infoTip.showTip((Button) event.getSource(), "   Saved   ");
        }
    }

    private boolean validateLoginAndPassword(String login, String password) {
        Pattern pattern = Pattern.compile("\\s");
        Matcher matcherLogin = pattern.matcher(login);
        Matcher matcherPassword = pattern.matcher(password);
        InfoTip infoTip = new InfoTip();

        if (!isLoginOk(login, matcherLogin)) {
            infoTip.showTip(textFLogin, "max 16 characters, no spaces, not empty");
            return false;
        }

        if (isLoginReserved(login)) {
            infoTip.showTip(textFLogin, "This login already exists!");
            return false;
        }

        if (isPasswordOk(password, matcherPassword)) {
            infoTip.showTip(textFPassword, "max 16 characters, no spaces, not empty");
            return false;
        }

        return true;
    }

    private boolean isPasswordOk(String password, Matcher matcherPassword) {
        return password.length() < 17 && !password.isEmpty() && !matcherPassword.find();
    }

    private boolean isLoginOk(String login, Matcher matcherLogin) {
        return login.length() < 17 && !login.isEmpty() && !matcherLogin.find();
    }

    private boolean isLoginReserved(String login) {
        boolean exists = false;
        String query = "SELECT * FROM users WHERE login = ?;";
        try (Connection connection = Main.dataSource.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                preparedStatement.setString(1, login);
                try (ResultSet result = preparedStatement.executeQuery()) {
                    if (result.next()) {
                        exists = true;
                    }
                }
            }
        } catch (Exception exception) {
            ExceptionDialog exceptionDialog = new ExceptionDialog("Couldn't check if login exists in table Users", exception);
            exceptionDialog.showAndWait();
        };

        return exists;
    }

    private void addUser(String login, String password, int employee_id) {
        String query = "INSERT INTO users VALUES(?, ?, ?);";
        try (Connection connection = Main.dataSource.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {;
                preparedStatement.setString(1, login);
                preparedStatement.setString(2, password);
                preparedStatement.setInt(3, employee_id);
                preparedStatement.executeUpdate();
                currentEmployee.setAccount(EXISTS.YES);
            }
        } catch (Exception exception) {
            ExceptionDialog exceptionDialog = new ExceptionDialog("Error while saving data to table users", exception);
            exceptionDialog.showAndWait();
        };
    }

    public void fillWithData(EmployeeData employeeData) {
        currentEmployee = employeeData;
        labEmployeeID.setText(currentEmployee.getiD().toString());
        labEmployeeName.setText(currentEmployee.getName());
    }

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        // TODO Auto-generated method stub
    }

    @Override
    public Pane getMainPane() {
        return borderPane;
    }
}
