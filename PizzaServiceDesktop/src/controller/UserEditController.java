package controller;

import classes.Controller;
import classes.ExceptionDialog;
import classes.InfoTip;
import classes.ModalWindow;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import model.Main;
import model.UserData;

public class UserEditController extends Controller implements Initializable {

    private UserData currentUser;

    @FXML
    private BorderPane borderPane;
    @FXML
    private Label labID, labName, labSurname, labBirthday;
    @FXML
    private TextField textFLogin, textFPassword;

    @FXML
    private void buttCancelAction(ActionEvent event) {
        ModalWindow owner = (ModalWindow) ((Button) event.getSource()).getScene().getWindow();
        owner.close();
    }

    @FXML
    private void buttSaveAction(ActionEvent event) {
        String login = textFLogin.getText();
        String password = textFPassword.getText();

        InfoTip infoTip = new InfoTip();
        if (validateLoginAndPassword(login, password)) {
            String query = "UPDATE users SET login = ?, password = ? WHERE employee_id = ?;";
            try (Connection connection = Main.dataSource.getConnection()) {
                try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                    preparedStatement.setString(1, login);
                    preparedStatement.setString(2, password);
                    preparedStatement.setInt(3, currentUser.getiD());
                    preparedStatement.executeUpdate();

                    currentUser.setLogin(login);
                    currentUser.setPassword(password);

                    infoTip.getInfoTip().setId("greenInfoTip");
                    infoTip.showTip((Button) event.getSource(), "   Saved   ");
                }
            } catch (Exception exception) {
                ExceptionDialog exceptionDialog = new ExceptionDialog("Error while saving data to database", exception);
                exceptionDialog.showAndWait();
            }
        }
    }

    private boolean validateLoginAndPassword(String login, String password) {
        boolean hasLoginWhiteSpace = isContainingWhiteSpace(login);
        boolean hasPasswordWhiteSpace = isContainingWhiteSpace(password);
        boolean isLoginMoreThan3 = login.length() > 3;
        boolean isLoginLessThan17 = login.length() < 17;
        boolean isPasswordMoreThan3 = password.length() > 3;
        boolean isPasswordLessThan17 = password.length() < 17;

        InfoTip infoTip = new InfoTip();
        if (hasLoginWhiteSpace) {
            infoTip.showTip(textFLogin, "no spaces!");
            return false;
        }

        if (hasPasswordWhiteSpace) {
            infoTip.showTip(textFPassword, "no spaces!");
            return false;
        }

        if (!isLoginMoreThan3 || !isLoginLessThan17) {
            infoTip.showTip(textFLogin, "no more than 16 and no less than 4 characters!");
            return false;
        }

        if (!currentUser.getLogin().equals(login) && doesLoginExists(login)) {
            infoTip.showTip(textFLogin, "this login already exists!");
            return false;
        }

        if (!isPasswordMoreThan3 || !isPasswordLessThan17) {
            infoTip.showTip(textFPassword, "no more than 16 and no less than 4 characters!");
            return false;
        }

        return true;
    }

    //======================================== END OF FXML DECLARATIONS ================================================
    public void fillWithData(UserData data) {
        currentUser = data;
        labID.setText(data.getiD().toString());
        labName.setText(data.getName());
        labSurname.setText(data.getSurname());
        labBirthday.setText(data.getBirthday().toString());
        textFLogin.setText(data.getLogin());
        textFPassword.setText(data.getPassword());
    }

    private boolean doesLoginExists(String login) {
        boolean exists = true;

        String query = "SELECT * FROM users WHERE login = ?;";
        try (Connection connection = Main.dataSource.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                preparedStatement.setString(1, login);
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    if (!resultSet.next()) {
                        exists = false;
                    }
                }
            }
        } catch (SQLException e) {
            ExceptionDialog exceptionDialog = new ExceptionDialog("Error while connecting to database", e);
            exceptionDialog.showAndWait();
        }

        return exists;
    }

    private boolean isContainingWhiteSpace(String string) {
        Pattern pattern = Pattern.compile("\\s");
        Matcher matcher = pattern.matcher(string);
        return matcher.find();
    }

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {

    }

    @Override
    public Pane getMainPane() {
        return borderPane;
    }
}
