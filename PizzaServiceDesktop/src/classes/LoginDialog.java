package classes;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.GridPane;
import javafx.util.Pair;


public class LoginDialog extends Dialog<Pair<String,String>>{
	private TextField username;
	private TextField password;
	private Button loginButton;
	
	public LoginDialog(String title){
		super();
		setTitle(title);
		setHeaderText(null);
		create();
	}
	
	public void setOnLogin(EventHandler<ActionEvent> value){
		loginButton.setOnAction(value);
	}
	
	public void showLoginDialog(){
		// Request focus on the username field by default.
		Platform.runLater(() -> username.requestFocus());
		showAndWait();
	}
	
	public String getUserName(){
		return username.getText();
	}
	public String getUserPassword(){
		return password.getText();
	}
	
	public void setTooltips(String nameField, String passField){
		username.setTooltip(new Tooltip(nameField));
		password.setTooltip(new Tooltip(passField));
	}
	
	//----------------------------------------------------------------------------------------------
	private void create() {
		// Set the button types.
		ButtonType loginButtonType = new ButtonType("Login", ButtonData.OK_DONE);
		getDialogPane().getButtonTypes().addAll(loginButtonType, ButtonType.CANCEL);
				
		// Create the username and password labels and fields.
		GridPane grid = new GridPane();
		grid.setHgap(10);
		grid.setVgap(10);
		grid.setPadding(new Insets(20, 150, 10, 10));

		username = new TextField();
		username.setPromptText("Username");
		password = new PasswordField();
		password.setPromptText("Password");

		grid.add(new Label("Username:"), 0, 0);
		grid.add(username, 1, 0);
		grid.add(new Label("Password:"), 0, 1);
		grid.add(password, 1, 1);

		// Enable/Disable login button depending on whether a username was entered.
		loginButton = (Button) getDialogPane().lookupButton(loginButtonType);
		loginButton.setDisable(true);

		// Do some validation (using the Java 8 lambda syntax).
		username.textProperty().addListener((observable, oldValue, newValue) -> {
		    loginButton.setDisable(newValue.trim().isEmpty());
		});
		
		getDialogPane().setContent(grid);
	}
	
}
