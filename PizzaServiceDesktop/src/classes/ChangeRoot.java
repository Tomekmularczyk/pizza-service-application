package classes;

/**
 * You need: 1. root to FXML file 2. FXML file with set path to controller class 3. FXML file controller class has to extend Controller
 * class and override getMainPane() method 4. If he wants to fade the stage and not change the window size, he still need to put controller
 * class
 *
 * example: ChangeTheRoot change = new ChangeTheRoot(Main.primaryStage, Main.NUMBER_GENERATOR_ROOT, "Number Generator", true);
 * change.doFadingTransition(stackPane, Main.fadeOutMillis, Main.fadeInMillis, true);
 *
 * 1. First user have declare object and construct it with initialize() method Then if initialize() didnt return false(error) he may use two
 * methods to change the root - changeRoot(); simply changes the root in window - doFadingTransition(); changes the root and also do the
 * fading effect
 *
 * 2. if he wants to change the Stage while changing a root then he has to use constructor with two additional parameters: - boolean
 * bChangeStageSize - needs to be true - Controller controller - the mother class of fxml file-controllers(each controller overrides
 * getMainPane() method. We need it to adjust Stage to new root size.
 *
 * 3. If he wants allso to fade the Stage and not only Scene then he has to use constructor with additional parameters
 */
import java.io.IOException;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.property.DoubleProperty;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.util.Duration;
import model.Main;

public class ChangeRoot {

    private boolean bChangeStageSize = true;
    private boolean bFadeStage = true;

    private String sNewTitle;
    private boolean bResizable;
    private Controller controller;
    private Stage primaryStage;
    private FXMLLoader loader;
    private Parent root;

    public ChangeRoot(Stage primaryStage, String sRoot, String sNewTitle, boolean bResizable) {
        this.primaryStage = primaryStage;
        this.bResizable = bResizable;
        this.sNewTitle = sNewTitle;

        loader = new FXMLLoader(getClass().getResource(sRoot));

        try {
            root = loader.load();
            controller = loader.getController();
        } catch (IOException exception) {
            ExceptionDialog exceptionDialog = new ExceptionDialog("Error while loading fxml file", exception);
            exceptionDialog.showAndWait();
        }
    }

    /**
     * ************************** PUBLIC METHODS ******************************
     */
    public Controller getController() {
        return controller;
    }

    public void doFadingTransition(Pane actualPane, double fadeOutMillis, double fadeInMillis) {
        actualPane.setDisable(true);
        //fading out
        final DoubleProperty stageOpacity = Main.mainStage.opacityProperty();
        final DoubleProperty oldPaneOpacity = actualPane.opacityProperty();
        if (bFadeStage) {
            oldPaneOpacity.bindBidirectional(stageOpacity);
        }

        Timeline fadeOut = new Timeline(
                new KeyFrame(Duration.ZERO, new KeyValue(oldPaneOpacity, 1.0)),
                new KeyFrame(new Duration(fadeOutMillis), e -> {
                    if (bResizable) {
                        Main.mainStage.setResizable(true);
                    } else {
                        Main.mainStage.setResizable(false);
                    }
                    if (bFadeStage) {
                        oldPaneOpacity.unbindBidirectional(stageOpacity);
                    }
                    changeRootAndWindow();
                    fadeIn(fadeInMillis);
                }, new KeyValue(oldPaneOpacity, 0.0)));

        fadeOut.play();
    }

    public final void changeRoot() {
        changeRootAndWindow();
    }

    /**
     * ************************** PRIVATE METHODS ******************************
     */
    private final void changeRootAndWindow() { //it will do depending what was put to the constructor
        if (primaryStage.isMaximized()) {
            primaryStage.setMaximized(false);
        }
        primaryStage.getScene().setRoot(root);
        primaryStage.setTitle(sNewTitle);

        if (bChangeStageSize) {
            Main.mainStage.setHeight(controller.getMainPane().getPrefHeight() + 38);
            Main.mainStage.setWidth(controller.getMainPane().getPrefWidth() + 16);
        }
    }

    private void fadeIn(double fadeInMillis) {
        if (controller == null) {
            ExceptionDialog exceptionDialog = new ExceptionDialog("insert controller class", new Exception());
            exceptionDialog.showAndWait();
        }

        final DoubleProperty newPaneOpacity = controller.getMainPane().opacityProperty();
        final DoubleProperty stageOpacity = Main.mainStage.opacityProperty();
        if (bFadeStage) {
            newPaneOpacity.bindBidirectional(stageOpacity);
        }

        Timeline fadeIn = new Timeline(
                new KeyFrame(Duration.ZERO, new KeyValue(newPaneOpacity, 0.0)),
                new KeyFrame(new Duration(fadeInMillis), actionEvent -> {
                    if (bFadeStage) {
                        newPaneOpacity.unbindBidirectional(stageOpacity);
                    }
                }, new KeyValue(newPaneOpacity, 1.0)));

        fadeIn.play();
    }
}
