package classes;

public enum INFO {
    GENERAL_INFO("Hi!\n"
            + "This sample desktop application is designed to help manage\n"
            + "pizza service. After specifying directory to database in\n"
            + "preferences(data will be stored in register for another\n"
            + "launch of application) and testing connection you will get\n"
            + "two buttons to log in administrator or employee panel.\n"
            + "Each panel serves different things.\n\n"
            + "Administrator panel helps to manage hired employees,\n"
            + "setting their accounts, adding/deleting new pizzas or\n"
            + "sizes to menu and so on. It is also possible to bring back\n"
            + "all default data to tables.\n"
            + "Employee panel on another hand can be accessed only by\n"
            + "employees which has account set. After log in to panel you\n"
            + "get sales table which is refreshed after every 5 seconds\n"
            + "so you can respond to new orders coming in online or place\n"
            + "new order.\n\n"
            + "To log in as administrator use:\n"
            + "login: admin\n"
            + "password: admin\n"
            + "To log in as employee use account set by admin e.g:\n"
            + "login: zenon\n"
            + "password: zenon\n\n"
            + "My name is tomek and if you want more informations go to\n"
            + "this site: tomekmularczyk.weebly.com\n\n"
            + "Cheers!"),
    EMPLOYEE_INFO("Welcome to employee panel! ;)\n"
            + "The title of this windows tells you who is currently logged\n"
            + "in, this is important because every sale made from that point\n"
            + "will be attached to this account.\n\n"
            + "Table is refreshing it's content every 5 seconds so if\n"
            + "someone orders it's pizza online his order will be added to\n"
            + "the table list. By double-clicking on specific table row you\n"
            + "will see window with all details attached to this order.\n"
            + "By going to `options->new order` you can place new order\n"
            + "for client. Choose available in service pizzas, size, and\n"
            + "fill in required informations.\n"
            + "Press right mouse button on specific row to highlight it\n"
            + "and choose additional status change from context menu. If \n"
            + "you change status of order it might help online user to \n"
            + "determine time left to recieve his pizza.\n"
            + "From `Table` menu choose `Pending` to see list of only active\n"
            + "orders. That means orders with status changed to 'Canceled'\n"
            + "or 'Finished' will be removed from this table list.\n"
            + "Click on column header or headers(with shift button) to \n"
            + "perform ascending or descending data sorting."),
    ADMIN_INFO("Welcome to administrator panel! ;)\n"
            + "In this section you will perform many different tasks on\n"
            + "database tables which should be permitted only by\n"
            + "confidential person.\n\n"
            + "To display all data within table go to menu `Tables` and\n"
            + "choose the one you need.\n"
            + "- delete/search/describe/change status of a sale\n"
            + "- describe/search delivery\n"
            + "- add/delete/edit/search service employees\n"
            + "- add/modify/search employees accounts\n"
            + "- add/modify/search pizza menu\n"
            + "- add or delete available pizza sizes\n"
            + "Most of the task are accessible by double mouse click or\n"
            + "by requesting context menu on table.\n\n"
            + "You can load all default table data by choosing\n"
            + "`Options->Load deafult data`.\n\n"
            + "`Android` eployee and account is designed to enable\n"
            + "sending request from android devices.");

    public String text;

    private INFO(String text) {
        this.text = text;
    }
}
