package classes;

import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Tooltip;
import javafx.scene.text.Font;

public class InfoTip {

    private final Tooltip infoTip;

    public InfoTip() {
        infoTip = new Tooltip();
    }

    private static Point2D getNodePos(Node node) {  //getting node coordination on screen
        Scene nodeScene = node.getScene();
        final Point2D windowPos = new Point2D(nodeScene.getWindow().getX(), nodeScene.getWindow().getY());
        final Point2D scenePos = new Point2D(nodeScene.getX(), nodeScene.getY());
        final Point2D nodePos = node.localToScene(0.0, 0.0);
        final Point2D nodePosOnScreen = new Point2D(windowPos.getX() + scenePos.getX() + nodePos.getX(),
                windowPos.getY() + scenePos.getY() + nodePos.getY());
        return nodePosOnScreen;
    }

    public void showTip(Node node, String text) {
        Point2D nodePosOnScreen = getNodePos(node);

        infoTip.setText(text);
        infoTip.setFont(new Font(15));
        infoTip.setOpacity(0.9);
        infoTip.setAutoFix(true);
        infoTip.setAutoHide(true);
        infoTip.show(node, nodePosOnScreen.getX() - 30, nodePosOnScreen.getY() - 40);
    }

    public Tooltip getInfoTip() {
        return infoTip;
    }
}
