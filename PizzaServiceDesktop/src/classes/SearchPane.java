package classes;

import java.util.ArrayList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class SearchPane extends VBox {

    private final ArrayList<CheckBox> checkBoxList = new ArrayList<>();
    private final TextField textFSearchPhrase = new TextField();
    private final Button buttSearch = new Button("Search");

    public SearchPane(Integer count) {
        setSpacing(3);
        setPadding(new Insets(2, 2, 0, 2));

        for (int i = 0; i < count; ++i) {
            CheckBox newCheckBox = new CheckBox();
            checkBoxList.add(newCheckBox);
            getChildren().add(newCheckBox);
        }

        HBox hBoxSearchField = new HBox();
        hBoxSearchField.setSpacing(2);
        hBoxSearchField.setPadding(new Insets(5, 0, 0, 0));
        Label label = new Label("Search phrase:");
        label.setStyle("-fx-font-weight: bold; -fx-font-size: 14;");
        textFSearchPhrase.setPrefWidth(200);
        textFSearchPhrase.setPromptText("type text you want to search");
        hBoxSearchField.getChildren().addAll(label, textFSearchPhrase);
        hBoxSearchField.setAlignment(Pos.CENTER_LEFT);

        HBox hBoxAction = new HBox();
        hBoxAction.setAlignment(Pos.CENTER_RIGHT);
        hBoxAction.setPadding(new Insets(0, 1, 2, 0));
        hBoxAction.setSpacing(2);
        Button buttCancel = new Button("Cancel");
        buttCancel.setCancelButton(true);
        buttCancel.setPrefWidth(80);
        buttCancel.setOnAction(event -> {
            Stage owner = (Stage) buttCancel.getScene().getWindow();
            owner.close();
        });
        buttSearch.setDefaultButton(true);
        buttSearch.setPrefWidth(80);
        hBoxAction.getChildren().addAll(buttCancel, buttSearch);

        getChildren().addAll(hBoxSearchField, new Separator(), hBoxAction);
    }

    public void setNames(String... strings) {
        for (int i = 0; i < strings.length; i++) {
            checkBoxList.get(i).setText(strings[i]);
        }
    }

    public boolean isSomethingSelected() {
        boolean is = false;

        for (CheckBox checkBox : checkBoxList) {
            if (checkBox.isSelected()) {
                is = true;
            }
        }

        return is;
    }

    public void setOnSearch(EventHandler<ActionEvent> handler) {
        buttSearch.setOnAction(handler);
    }

    public ArrayList<CheckBox> getCheckBoxList() {
        return checkBoxList;
    }

    public String getSearchPhrase() {
        return textFSearchPhrase.getText().trim();
    }

    public TextField getTextField() {
        return textFSearchPhrase;
    }
}
