package classes;

import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class ModalWindow extends Stage {

    public ModalWindow(Stage owner, Pane content, String title) {
        Scene scene = new Scene(content);
        setScene(scene);

        initOwner(owner);
        initModality(Modality.NONE);
        initStyle(StageStyle.UTILITY);
        setTitle(title);
    }
}
