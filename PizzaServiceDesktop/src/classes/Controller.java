package classes;

import javafx.scene.layout.Pane;

public abstract class Controller {

    public abstract Pane getMainPane();

}
