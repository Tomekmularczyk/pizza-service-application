

CREATE TABLE `employees` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(20) NOT NULL,
  `Surname` varchar(25) NOT NULL,
  `Birthday` date NOT NULL,
  `Adres` varchar(50) NOT NULL,
  `Telephone` varchar(25) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

CREATE TABLE `users` (
  `login` varchar(16) NOT NULL,
  `password` varchar(16) NOT NULL,
  `employee_id` int(11) NOT NULL,
  PRIMARY KEY (`login`),
  UNIQUE KEY `login` (`login`),
  UNIQUE KEY `employee_id` (`employee_id`),
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8

CREATE TABLE `pizzas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL DEFAULT '???',
  `composition` varchar(150) DEFAULT NULL,
  `price` decimal(10,2) NOT NULL DEFAULT '10.00',
  `visible` enum('Yes','No'),
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

CREATE TABLE `pizza_size` (
  `size` tinyint(4) unsigned NOT NULL,    // must be less than 255
  `price` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`size`),
  UNIQUE KEY `size` (`size`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

CREATE TABLE `sales` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_date` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('Waiting', 'Accepted', 'Production','Transport', 'Finalised', 'Canceled') NOT NULL DEFAULT 'Waiting',
  `user_id` varchar(16) NOT NULL,
  `pizza_id` int(11) NOT NULL,
  `pizza_size` tinyint(4) unsigned NOT NULL,
  `income` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `pizza_id` (`pizza_id`),
  CONSTRAINT `sales_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`login`) ON UPDATE CASCADE,
  CONSTRAINT `sales_ibfk_2` FOREIGN KEY (`pizza_id`) REFERENCES `pizzas` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8

CREATE TABLE `deliveries` (
  `order_id` int(11) NOT NULL,
  `adress` varchar(50) NOT NULL,
  `client_name` varchar(35) DEFAULT NULL,
  `telephone` varchar(20) DEFAULT NULL,
  `cost` decimal(10,2) NOT NULL DEFAULT '0.00',
  UNIQUE KEY `order_id` (`order_id`),
  CONSTRAINT `delivery_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `sales` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8

LOAD DATA LOCAL INFILE ? INTO TABLE employees LINES TERMINATED BY '\r\n' STARTING BY 'xxx';

SELECT e.ID, e.name, e.surname, e.birthday, e.adres, e.telephone, u.login FROM employees AS e 
LEFT JOIN users AS u on e.id = u.employee_id;

SELECT e.id, e.name, e.surname, e.birthday, u.login, u.password FROM employees AS e
RIGHT JOIN users AS u ON e.id = u.employee_id ORDER BY e.id;

SELECT s.id, s.order_date, s.user_id, s.pizza_id, s.pizza_size_id, s.income, d.order_id FROM sales AS s 
LEFT JOIN delivery AS d ON s.id = d.order_id