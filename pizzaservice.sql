-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1:3306
-- Czas generowania: 14 Lis 2015, 20:48
-- Wersja serwera: 5.7.9
-- Wersja PHP: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Baza danych: `pizzaservice3`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `sales`
--

CREATE TABLE `sales` (
  `id` int(11) NOT NULL,
  `order_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('Waiting','Accepted','Production','Transport','Finalised','Canceled') NOT NULL DEFAULT 'Waiting',
  `user_id` varchar(16) NOT NULL,
  `pizza_id` int(11) NOT NULL,
  `pizza_size` tinyint(4) UNSIGNED NOT NULL,
  `income` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `sales`
--

INSERT INTO `sales` (`id`, `order_date`, `status`, `user_id`, `pizza_id`, `pizza_size`, `income`) VALUES
(1, '2014-11-01 11:20:01', 'Finalised', 'ericB', 12, 20, 17.89),
(2, '2014-11-01 13:28:11', 'Finalised', 'ericB', 11, 30, 23.49),
(3, '2014-11-01 21:23:31', 'Finalised', 'ericB', 2, 40, 20.05),
(4, '2014-12-15 13:50:18', 'Canceled', 'jacquelyn', 5, 20, 9.99),
(5, '2014-12-15 15:30:33', 'Finalised', 'jacquelyn', 6, 30, 18.95),
(6, '2014-12-15 16:10:41', 'Finalised', 'jacquelyn', 3, 40, 17.94),
(7, '2015-01-10 07:04:11', 'Canceled', 'Johnny', 9, 20, 11.20),
(8, '2015-01-10 09:12:24', 'Finalised', 'Johnny', 9, 40, 18.15),
(9, '2015-01-31 22:54:02', 'Finalised', 'Android', 3, 60, 20.49),
(10, '2015-01-10 10:49:53', 'Finalised', 'Johnny', 4, 30, 20.00),
(11, '2015-01-15 21:24:12', 'Finalised', 'aliceFinley', 7, 20, 16.60),
(12, '2015-01-15 22:52:15', 'Finalised', 'aliceFinley', 5, 60, 19.49),
(13, '2015-02-22 10:15:26', 'Finalised', 'harrylove', 8, 40, 24.74),
(14, '2015-02-22 13:36:52', 'Production', 'harrylove', 8, 20, 17.79),
(15, '2015-02-28 16:34:50', 'Finalised', 'Android', 9, 40, 18.15),
(16, '2015-02-22 15:52:55', 'Production', 'harrylove', 13, 30, 21.09),
(17, '2015-03-29 01:23:12', 'Production', 'Carroline', 8, 60, 27.29),
(18, '2015-03-29 17:43:32', 'Transport', 'Carroline', 3, 20, 10.99),
(19, '2015-03-29 20:55:53', 'Waiting', 'Carroline', 7, 30, 21.10),
(20, '2015-04-18 08:10:12', 'Accepted', 'williamPayne', 10, 20, 13.33),
(21, '2015-04-19 10:22:20', 'Canceled', 'Android', 6, 30, 18.95),
(22, '2015-11-14 19:45:29', 'Transport', 'Android', 10, 40, 20.28),
(23, '2015-11-14 19:45:43', 'Production', 'Android', 13, 40, 23.54),
(24, '2015-11-14 19:46:45', 'Accepted', 'Android', 1, 60, 22.00),
(25, '2015-11-14 19:46:57', 'Waiting', 'Android', 2, 30, 17.60);

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `sales`
--
ALTER TABLE `sales`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `pizza_id` (`pizza_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `sales`
--
ALTER TABLE `sales`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `sales`
--
ALTER TABLE `sales`
  ADD CONSTRAINT `sales_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`login`) ON UPDATE CASCADE,
  ADD CONSTRAINT `sales_ibfk_2` FOREIGN KEY (`pizza_id`) REFERENCES `pizzas` (`id`) ON UPDATE CASCADE;
