package com.example.tomek.pizzaservice;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by tomek on 04.11.2015.
 */
public class PizzaSizeAdapter extends BaseAdapter {
    private Context context;
    private LayoutInflater layoutInflater;
    private ArrayList<PizzaSizeData> pizzaSizesList;

    public PizzaSizeAdapter(Context ctx, ArrayList<PizzaSizeData> pizzaSizesList){
        context = ctx;
        layoutInflater = LayoutInflater.from(context);
        this.pizzaSizesList = pizzaSizesList;
    }

    @Override
    public int getCount() {
        return pizzaSizesList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if(convertView == null) {
            convertView = layoutInflater.inflate(R.layout.pizza_size_adapter_layout, parent, false);

            viewHolder = new ViewHolder();
            viewHolder.pizzaSize = (TextView) convertView.findViewById(R.id.pizzaSizeTextView);

            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }

        PizzaSizeData pizzaSize = pizzaSizesList.get(position);

        String formattedPrice = String.format("%.2f", pizzaSize.getPrice());
        viewHolder.pizzaSize.setText(pizzaSize.getSize() + "cm - " + formattedPrice + " pln");

        return convertView;
    }

    private static class ViewHolder{
        protected TextView pizzaSize;
    }
}