package com.example.tomek.pizzaservice;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class PizzaListActivity extends ListActivity {
    private ArrayList<PizzaData> pizzaList;
    @Bind(R.id.progressBar) ProgressBar progressBar;
    @Bind(R.id.loadingText) TextView loadingText;
    @Bind(R.id.pizzaListLinearlayout) LinearLayout rootLayout;
    @Bind(R.id.imageButtonTryAgain) ImageButton imageTryAgain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pizza_list);

        ButterKnife.bind(this);

        setProgressBar();
        loadPizzaList();
    }

    private void setProgressBar() {
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        float density = dm.density;

        Drawable drawable = ContextCompat.getDrawable(this, R.drawable.progressbar_pizzalist);
        drawable.setBounds(0, 0, (int) (150 * density), (int) (150 * density));
        progressBar.setIndeterminateDrawable(drawable);
    }

    private void loadPizzaList() {
        ConnectivityManager manager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if(ApiConnector.isNetworkAvailable(manager)){
            imageTryAgain.setVisibility(View.GONE);
            new GetPizzaListTask().execute();
        }else{
            getSnackbar("Connect to the Internet first.").show();
            imageTryAgain.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.imageButtonTryAgain)
    public void toggleClick() {
        loadPizzaList();
    }
    private void setContent(){
        PizzaListAdapter adapter = new PizzaListAdapter(this, pizzaList);
        setListAdapter(adapter);

    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        Intent intent = new Intent(PizzaListActivity.this, MainActivity.class);
        intent.putExtra(PizzaData.PIZZADATA_KEY, pizzaList.get(position));
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    private Snackbar getSnackbar(String text) {
        Snackbar snackbar = Snackbar.make(rootLayout, text, Snackbar.LENGTH_SHORT);
        snackbar.setDuration(Snackbar.LENGTH_LONG);
        snackbar.setAction("Close", new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        return snackbar;
    }

    private class GetPizzaListTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
            loadingText.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            pizzaList = ApiConnector.getPizzaDataFromDatabase();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressBar.setVisibility(View.GONE);
            loadingText.setVisibility(View.GONE);

            if(pizzaList == null) {
                getSnackbar("Couldn't load pizza sizes data!").show();
                imageTryAgain.setVisibility(View.VISIBLE);
            }else{
                imageTryAgain.setVisibility(View.GONE);

                ArrayAdapter<String> adapter = (ArrayAdapter<String>) getListAdapter();

                setContent();
            }
        }
    }
}
