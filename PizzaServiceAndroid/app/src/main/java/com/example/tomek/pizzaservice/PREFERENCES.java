package com.example.tomek.pizzaservice;

/**
 * Created by Tomek on 2015-04-27.
 */
public enum PREFERENCES {
    PREFS_NAME("PizzaServicePreferences"),
    NAME_SURNAME_KEY("Name_Surname_Preferences"),
    ADDRESS_KEY("Address_Preferences"),
    TELEPHONE_KEY("Telephone_Preferences");

    public String string;
    private PREFERENCES(String value){
        string = value;
    }
}
