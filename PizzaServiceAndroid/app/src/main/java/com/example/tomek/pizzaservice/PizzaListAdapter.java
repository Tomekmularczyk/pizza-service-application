package com.example.tomek.pizzaservice;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by tomek on 11.11.2015.
 */
public class PizzaListAdapter extends BaseAdapter {
    private Context context;
    private LayoutInflater layoutInflater;
    private ArrayList<PizzaData> pizzaList;

    public PizzaListAdapter(Context ctx, ArrayList<PizzaData> pizzaList) {
        context = ctx;
        layoutInflater = LayoutInflater.from(context);
        this.pizzaList = pizzaList;
    }

    @Override
    public int getCount() {
        return pizzaList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.list_acitivty_item, parent, false);

            viewHolder = new ViewHolder(convertView);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        PizzaData pizza = pizzaList.get(position);

        viewHolder.pizza.setText(pizza.getID() + ". " + pizza.getName());
        String formattedPrice = String.format("%.2f", pizza.getPrice());
        viewHolder.pizzaPrice.setText(formattedPrice + " pln");
        viewHolder.pizzaDescription.setText(pizza.getComposition());

        return convertView;
    }

    protected static class ViewHolder {
        @Bind(R.id.textPizzaName)
        TextView pizza;
        @Bind(R.id.pizzaPriceText)
        TextView pizzaPrice;
        @Bind(R.id.textDescription)
        TextView pizzaDescription;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
