package com.example.tomek.pizzaservice;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TabOrderFragment extends Fragment {
    private PizzaData selectedPizza;
    private ArrayList<PizzaSizeData> pizzaSizesList;

     @Bind(R.id.tabOrderLayout) ScrollView rootLayout;
     @Bind(R.id.textNotSelected) TextView textNotSelected;
     @Bind(R.id.imageButtonTryAgain) ImageButton imageTryAgain;
     @Bind(R.id.textVPizzaName) TextView pizzaName;
     @Bind(R.id.progressBar) ProgressBar progressBar;
     @Bind(R.id.textVPizzaComposition) TextView pizzaComposition;
     @Bind(R.id.textVCost) TextView textVCost;
     @Bind(R.id.spinnerPizzaSize) Spinner spinnerPizzaSize;
     @Bind(R.id.editTextName) EditText editTextName;
     @Bind(R.id.editTextAdress) EditText editTextAdress;
     @Bind(R.id.editTextTelephone) EditText editTextTelephone;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tab_order, container, false);
        ButterKnife.bind(this, view);

        loadPreferences();
        setProgressBar();

        return view;
    }

    private void setProgressBar() {
        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        float density = dm.density;

        Drawable drawable = ContextCompat.getDrawable(getActivity(), R.drawable.progressbar);
        drawable.setBounds(0, 0, (int) (150 * density), (int) (150 * density));
        progressBar.setIndeterminateDrawable(drawable);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        loadPizzaSizeList();
    }

    private void loadPizzaSizeList() {
        ConnectivityManager manager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        if(ApiConnector.isNetworkAvailable(manager)) {
            imageTryAgain.setVisibility(View.GONE);
            new GetPizzaSizesListTask().execute();
        }else{
            getSnackbar("Connect to the Internet first.").show();
            imageTryAgain.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.buttSelectPizza)
    public void buttSelectPizza() {
        ConnectivityManager manager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        if(ApiConnector.isNetworkAvailable(manager))
            getActivity().startActivityForResult(new Intent(getActivity(), PizzaListActivity.class), 1);
        else{
            getSnackbar("Connect to the Internet first.").show();
        }
    }

    @OnClick(R.id.imageButtonTryAgain)
    public void toggleClick() {
        loadPizzaSizeList();
    }
    public void setPizza(PizzaData pizza){
        selectedPizza = pizza;
        pizzaName.setVisibility(View.VISIBLE);
        pizzaComposition.setVisibility(View.VISIBLE);
        textNotSelected.setVisibility(View.GONE);
        pizzaName.setText(selectedPizza.getName());
        pizzaComposition.setText(selectedPizza.getComposition());
        refreshCost();
    }

    private void refreshCost(){
        double pizzaCost = 0.0;
        double pizzaSizeCost = 0.0;

        if(selectedPizza != null)
            pizzaCost = selectedPizza.getPrice();
        if(pizzaSizesList != null){
            if(spinnerPizzaSize.getSelectedItemPosition() != Spinner.INVALID_POSITION)
                pizzaSizeCost = pizzaSizesList.get(spinnerPizzaSize.getSelectedItemPosition()).getPrice();
        }

        textVCost.setVisibility(View.VISIBLE);
        textVCost.setText("Cost: " + String.format("%.2f", (pizzaCost + pizzaSizeCost)) + " pln");
    }

    private void setSpinner(){
        PizzaSizeAdapter adapter = new PizzaSizeAdapter(getContext(), pizzaSizesList);

        spinnerPizzaSize.setAdapter(adapter);
        spinnerPizzaSize.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                refreshCost();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    private class GetPizzaSizesListTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            spinnerPizzaSize.setVisibility(View.INVISIBLE);
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            pizzaSizesList = ApiConnector.getPizzaSizesDataFromDatabase();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if(pizzaSizesList == null) {
                getSnackbar("Couldn't load pizza sizes data!").show();
                progressBar.setVisibility(View.GONE);
                imageTryAgain.setVisibility(View.VISIBLE);
            }else {
                spinnerPizzaSize.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
                imageTryAgain.setVisibility(View.GONE);
                setSpinner(); //after we load data we need to set it in list
                refreshCost();
            }
        }
    }

    @OnClick(R.id.buttOrder)
    public void buttOrderAction(View v){
        final String clientName = editTextName.getText().toString().trim();
        final String address = editTextAdress.getText().toString().trim();
        final String telephone = editTextTelephone.getText().toString().trim();
        boolean isPizzaSelected = (!pizzaName.getText().toString().isEmpty());
        boolean isClientNameOK = ((clientName.length() < 36) && (!clientName.isEmpty()));
        boolean isAddressOK = ((address.length() < 51) && (!address.isEmpty()));
        boolean isTelephoneOK = ((telephone.length() < 21) && (!telephone.isEmpty()));

        if(isPizzaSelected) {
            if (isClientNameOK) {
                if (isAddressOK) {
                    if (isTelephoneOK){ //if true then we're finally sending order to database

                        if(spinnerPizzaSize.getSelectedItemPosition() != AdapterView.INVALID_POSITION) {
                            PizzaSizeData pizzaSize = pizzaSizesList.get(spinnerPizzaSize.getSelectedItemPosition());
                            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder.setTitle("Are you sure?")
                                    .setMessage("Cost: " + String.format("%.2f", (selectedPizza.getPrice() + pizzaSize.getPrice())) + " pln")
                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            //Yes button clicked
                                            ConnectivityManager manager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
                                            if(ApiConnector.isNetworkAvailable(manager)) //last check
                                                new SendOrder().execute(); //here finally we are executing the order
                                            else{
                                                getSnackbar("Connect to the Internet first.").show();
                                            }
                                            savePreferences(clientName, address, telephone);
                                        }
                                    })
                                    .setNegativeButton("No", null)                        //Do nothing on no
                                    .show();
                        }else{
                            Toast.makeText(getActivity(), "You need to select pizza size.", Toast.LENGTH_SHORT).show();
                        }
                    } else { //telephone not ok
                        Toast.makeText(getActivity(), "Telephone: not empty, no more than 20 digits", Toast.LENGTH_SHORT).show();
                    }
                } else { //adress not ok
                    Toast.makeText(getActivity(), "Address: not empty, no more than 50 characters", Toast.LENGTH_SHORT).show();
                }
            } else { //client name not ok
                Toast.makeText(getActivity(), "Name: not empty, no more than 35 characters", Toast.LENGTH_SHORT).show();
            }
        }else{//textView pizzaName is empty, so no pizza is selected
            Toast.makeText(getActivity(), "Select pizza and size first", Toast.LENGTH_SHORT).show();
        }
    }
    private void savePreferences(String clientName, String address, String telephone){
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(PREFERENCES.PREFS_NAME.string, 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString(PREFERENCES.NAME_SURNAME_KEY.string, clientName);
        editor.putString(PREFERENCES.ADDRESS_KEY.string, address);
        editor.putString(PREFERENCES.TELEPHONE_KEY.string, telephone);
        editor.apply();
    }
    private void loadPreferences(){
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(PREFERENCES.PREFS_NAME.string, 0);
        editTextName.setText(sharedPreferences.getString(PREFERENCES.NAME_SURNAME_KEY.string, ""));
        editTextAdress.setText(sharedPreferences.getString(PREFERENCES.ADDRESS_KEY.string, ""));
        editTextTelephone.setText(sharedPreferences.getString(PREFERENCES.TELEPHONE_KEY.string, ""));
    }

    private Snackbar getSnackbar(String text) {
        Snackbar snackbar = Snackbar.make(rootLayout, text, Snackbar.LENGTH_SHORT);
        snackbar.setDuration(Snackbar.LENGTH_LONG);
        snackbar.setAction("Close", new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        return snackbar;
    }

    private class SendOrder extends AsyncTask<Void, Void, Void> {
        private String clientName, address, telephone;
        private int pizzaSize;
        private boolean isSent = false;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Toast.makeText(getActivity(), "Sending order... please wait.", Toast.LENGTH_LONG).show();

            clientName = editTextName.getText().toString().trim();
            address = editTextAdress.getText().toString().trim();
            telephone = editTextTelephone.getText().toString().trim();
            pizzaSize = spinnerPizzaSize.getSelectedItemPosition();
        }

        @Override
        protected Void doInBackground(Void... params) {
            PizzaSizeData pizzaSizeData = pizzaSizesList.get(pizzaSize);
            isSent = ApiConnector.sendData("Android", selectedPizza.getID(), pizzaSizeData.getSize(), (selectedPizza.getPrice()+pizzaSizeData.getPrice()),
                            address, clientName, telephone);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if(isSent)
                Toast.makeText(getActivity(), "Order sent. Check your history.", Toast.LENGTH_LONG).show();
            else{
                Toast.makeText(getActivity(), "Couldn't reach connection - try again.", Toast.LENGTH_LONG).show();
            }
        }
    }

}
