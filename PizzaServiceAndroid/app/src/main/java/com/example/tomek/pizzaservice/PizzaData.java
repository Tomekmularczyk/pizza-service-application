package com.example.tomek.pizzaservice;

import android.os.Parcel;
import android.os.Parcelable;

public class PizzaData implements Parcelable{
    public final static String PIZZADATA_KEY = "pizzadata_parcel";

	private int iD;
	private String name;
	private String composition;
	private Double price;

	public PizzaData() {
		iD = 0;
		name = "";
		price = 0.0;
		composition = "";
	}
    public PizzaData(Parcel object){
        iD = object.readInt();
        name = object.readString();
        composition = object.readString();
        price = object.readDouble();
    }
	
	public Integer getID() {
		return iD;
	}
	public void setID(Integer iD) {
		this.iD = iD;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getComposition() {
		return composition;
	}
	public void setComposition(String composition) {
		this.composition = composition;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}


    //============== REQUIRED METHODS TO MAKE THIS CLASS PARCELABLE ============================
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(iD);
        parcel.writeString(name);
        parcel.writeString(composition);
        parcel.writeDouble(price);
    }

    public static final Parcelable.Creator<PizzaData> CREATOR = new Parcelable.Creator<PizzaData>() {
        public PizzaData createFromParcel(Parcel in) {
            return new PizzaData(in);
        }

        public PizzaData[] newArray(int size) {
            return new PizzaData[size];
        }
    };
}
