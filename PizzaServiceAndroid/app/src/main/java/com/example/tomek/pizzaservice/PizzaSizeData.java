package com.example.tomek.pizzaservice;

public class PizzaSizeData {
	
	private int size;
	private double price;
	
	public PizzaSizeData(){
		size = 0;
		price = 0.0;
	}
	
	public Integer getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
}
