package com.example.tomek.pizzaservice;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by tomek on 12.11.2015.
 */
public class HistoryAdapter extends BaseAdapter {
    private Context context;
    private LayoutInflater layoutInflater;
    private ArrayList<SaleData> salesList;

    public HistoryAdapter(Context ctx, ArrayList<SaleData> pizzaList) {
        context = ctx;
        layoutInflater = LayoutInflater.from(context);
        this.salesList = pizzaList;
    }

    @Override
    public int getCount() {
        return salesList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.history_list_item, parent, false);

            viewHolder = new ViewHolder(convertView);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        SaleData order = salesList.get(salesList.size() - (position + 1));
        viewHolder.pizza.setText(order.getPizzaNumber() + ". " + order.getPizzaName());
        viewHolder.orderDate.setText(order.getOrderDate() + "");
        viewHolder.pizzaPrice.setText(String.format("%.2f", order.getIncome()) + " pln");
        viewHolder.pizzaSize.setText(order.getPizzaSize() + " cm");
        viewHolder.orderStatus.setText(order.getOrderStatus() + "");

        viewHolder.orderStatus.setTextColor(order.getOrderStatus().color);

        return convertView;
    }

    protected static class ViewHolder {
        @Bind(R.id.textPizzaName)
        TextView pizza;
        @Bind(R.id.pizzaPriceText)
        TextView pizzaPrice;
        @Bind(R.id.pizzaSize)
        TextView pizzaSize;
        @Bind(R.id.textDate)
        TextView orderDate;
        @Bind(R.id.textStatus)
        TextView orderStatus;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
