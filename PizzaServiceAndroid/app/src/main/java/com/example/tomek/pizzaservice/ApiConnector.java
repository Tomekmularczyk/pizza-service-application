package com.example.tomek.pizzaservice;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.squareup.okhttp.Call;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;

/**
 * Created by Tomek on 2015-04-23.
 */
public class ApiConnector {

    public static boolean sendData(String user, Integer pizzaID, Integer pizzaSize,  Double income,
                                                          String address, String clientName, String telephone){
        boolean result = false;

        OkHttpClient client = new OkHttpClient();

        RequestBody formBody = new FormEncodingBuilder()
                .add("user", user)
                .add("pizzaID", pizzaID.toString())
                .add("pizzaSize", pizzaSize.toString())
                .add("income", income.toString())
                .add("address", address)
                .add("clientName", clientName)
                .add("telephone", telephone)
                .build();

        //final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

        Request request = new Request.Builder()
                .url("http://tomekstronka.cba.pl/PizzaService/insertOrder.php")
                .post(formBody)
                .build();

        Response response = null;
        try {
            response = client.newCall(request).execute();

            if (response.isSuccessful()) {
                Log.e("LOGG", response.body().string());
                result = true;
            } else {
                result = false;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }

    public static ArrayList<SaleData> getAndroidOrdersHistory(){
        final String phpFileAddress = "http://tomekstronka.cba.pl/PizzaService/getAndroidOrders.php";
        JSONArray jsonArray = loadFromDatabase(phpFileAddress);
        ArrayList<SaleData> saleList = null;

        if(jsonArray != null) {
            saleList = new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject json;

                try {
                    json = jsonArray.getJSONObject(i);
                    SaleData sale = new SaleData();
                    sale.setiD(json.getInt("id"));
                    sale.setOrderDate(Timestamp.valueOf(json.getString("order_date")));
                    sale.setOrderStatus(SaleData.ORDER_STATUS.valueOf(json.getString("status")));
                    sale.setUser(json.getString("user_id"));
                    sale.setPizzaNumber(json.getInt("pizza_id"));
                    sale.setPizzaName(json.getString("name"));
                    sale.setPizzaSize(json.getInt("pizza_size"));
                    sale.setIncome(json.getDouble("income"));

                    saleList.add(sale);
                } catch (JSONException exception) {
                    exception.printStackTrace();
                }
            }
        }


        return saleList;
    }


    public static ArrayList<PizzaSizeData> getPizzaSizesDataFromDatabase(){
        final String phpFileAddress = "http://tomekstronka.cba.pl/PizzaService/getPizzaSizes.php";
        JSONArray jsonArray = loadFromDatabase(phpFileAddress);
        ArrayList<PizzaSizeData> pizzaSizeList = null;

        if(jsonArray != null) {
            pizzaSizeList = new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject json;

                try {
                    json = jsonArray.getJSONObject(i);
                    PizzaSizeData pizzaSize = new PizzaSizeData();
                    pizzaSize.setSize(json.getInt("size"));
                    pizzaSize.setPrice(json.getDouble("price"));

                    pizzaSizeList.add(pizzaSize);
                } catch (JSONException exception) {
                    exception.printStackTrace();
                }
            }
        }

        return pizzaSizeList;
    }

    public static ArrayList<PizzaData> getPizzaDataFromDatabase(){
        final String phpFileAddress = "http://tomekstronka.cba.pl/PizzaService/getPizzas.php";
        JSONArray jsonArray = loadFromDatabase(phpFileAddress);
        ArrayList<PizzaData> pizzaList = null;

        if(jsonArray != null) {
            pizzaList = new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject json;

                try {
                    json = jsonArray.getJSONObject(i);
                    if(json.getString("visible").equals("Yes")) { //we load only available pizzas
                        PizzaData pizza = new PizzaData();
                        pizza.setID(json.getInt("id"));
                        pizza.setName(json.getString("name"));
                        pizza.setComposition(json.getString("composition"));
                        pizza.setPrice(json.getDouble("price"));
                        pizzaList.add(pizza);
                    }
                } catch (JSONException exception) {
                    exception.printStackTrace();
                }
            }
        }

        return pizzaList;
    }

    //==============================================================================================
    private static JSONArray loadFromDatabase(String phpFileAddress){
        JSONArray jsonArray = null;


        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().url(phpFileAddress).build();
        Call call = client.newCall(request);
        try {
            Response response = call.execute();
            if (response.isSuccessful()) {
                Document doc = Jsoup.parse(response.body().string()); //removing html

                String jsonA = doc.text();

                jsonArray = new JSONArray(jsonA);//nie można dwa razy używać .string();
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }

        return jsonArray;
    }

    public static boolean isNetworkAvailable(ConnectivityManager manager) {
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        boolean isAvailable = false;
        if (networkInfo != null && networkInfo.isConnected()) {
            isAvailable = true;
        }

        return isAvailable;
    }
}
