package com.example.tomek.pizzaservice;

import android.graphics.Color;

import java.sql.Timestamp;
import java.util.Date;

public class SaleData {
	private Integer iD;
	private Timestamp orderDate;
	private ORDER_STATUS orderStatus;
	private String user;
	private Integer pizzaNumber;
    private String pizzaName;
	private Integer pizzaSize;
	private EXISTS delivery;
	private double income;
    
    public enum EXISTS{
        YES, NO
    }

    public enum ORDER_STATUS{
		Waiting(Color.YELLOW), Accepted(Color.GREEN), Production(Color.GREEN), Transport(Color.BLUE), Finalised(Color.BLACK), Canceled(Color.RED);

		public int color;

		ORDER_STATUS(int color) {
			this.color = color;
		}
	}

	public SaleData() {
		iD = 0;
		orderDate = new Timestamp(new Date().getTime());
		user = "";
		pizzaNumber = 0;
        pizzaName = "";
		pizzaSize = 0;
		income = 0.0;
	}

	//----------------------------------------------------------------------------------------------
	public Integer getID() {
		return iD;
	}
	public void setiD(Integer iD) {
		this.iD = iD;
	}
	public Timestamp getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(Timestamp orderDate) {
		this.orderDate = orderDate;
	}
	public ORDER_STATUS getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(ORDER_STATUS orderStatus) {
		this.orderStatus = orderStatus;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public Integer getPizzaNumber() {
        return pizzaNumber;
    }
    public void setPizzaNumber(Integer pizzaNumber) {
        this.pizzaNumber = pizzaNumber;
    }
    public String getPizzaName() {
        return pizzaName;
    }
    public void setPizzaName(String pizzaName) {
        this.pizzaName = pizzaName;
    }
	public Integer getPizzaSize() {
		return pizzaSize;
	}
	public void setPizzaSize(Integer pizzaSize) {
		this.pizzaSize = pizzaSize;
	}
	public EXISTS getDelivery() {
		return delivery;
	}
	public void setDelivery(EXISTS delivery) {
		this.delivery = delivery;
	}
	public Double getIncome() {
		return income;
	}
	public void setIncome(Double income){
		this.income = income;
	}
}
