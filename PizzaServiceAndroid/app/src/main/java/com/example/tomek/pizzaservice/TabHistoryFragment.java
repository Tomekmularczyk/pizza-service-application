package com.example.tomek.pizzaservice;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class TabHistoryFragment extends Fragment {

    @Bind(R.id.listView) ListView listView;
    @Bind(R.id.progressBar) ProgressBar progressBar;
    @Bind(R.id.tabHistoryLayout) LinearLayout rootLayout;
    private ArrayList<SaleData> salesList;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private void setProgressBar() {
        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        float density = dm.density;

        Drawable drawable = ContextCompat.getDrawable(getActivity(), R.drawable.progressbar);
        drawable.setBounds(0, 0, (int) (150 * density), (int) (150 * density));
        progressBar.setIndeterminateDrawable(drawable);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tab_history, container, false);
        ButterKnife.bind(this, view);
        setProgressBar();

        return view;
    }

    @OnClick(R.id.buttRefresh)
    public void buttRefreshAction(View view){
        listView.setVisibility(View.GONE);
        ConnectivityManager manager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        if(ApiConnector.isNetworkAvailable(manager))
            new GetSalesListTask().execute();
        else{
            getSnackbar("Connect to the Internet first.").show();
        }
    }
    private void fillTheList(){
        listView.setVisibility(View.VISIBLE);

        HistoryAdapter adapter = new HistoryAdapter(getContext(), salesList);
        listView.setAdapter(adapter);
    }

    private Snackbar getSnackbar(String text) {
        Snackbar snackbar = Snackbar.make(rootLayout, text, Snackbar.LENGTH_SHORT);
        snackbar.setDuration(Snackbar.LENGTH_LONG);
        snackbar.setAction("Close", new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        return snackbar;
    }

    private class GetSalesListTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
            listView.setAdapter(null);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            salesList = ApiConnector.getAndroidOrdersHistory();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressBar.setVisibility(View.GONE);
            if(salesList == null)
                getSnackbar("Couldn't load history.").show();
            else
                fillTheList();
        }
    }

}
